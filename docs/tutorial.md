Step-by-step HOW TO for System Packaging Library
================================================

Let's assume Alice has a new operating system, Bob wants to develop an application for it which will be used by Cecil.

[[_TOC_]]

Setting up
----------

Alice: creates a repository certificate.
```sh
$ syspkg cert --repo "GB" "Alice's Awesome OS"
```

Bob: creates a package maintainer certificate, and sends it to Alice.
```sh
$ syspkg cert "US" "Bob Doe <doe@notgmail.com>" >maintaner.pem
```

Alice: receives Bob's certficate, signs it and sends the signed certificate back.
```sh
$ syspkg cert --sign maintainer.pem >signed.pem
```

Bob: imports the signed certificate, and everything is set up.
```
$ syspkg cert signed.pem
```

If they wish to check their certificates, both Alice and Bob can run openssl to format it into human readable text:
```sh
$ syspkg cert | openssl x509 -text
```

Creating the package
--------------------

Bob: now it is Bob's job to actually develop the application. Let's say he finished it and uploaded it to gitlimb.com,
and placed the compiled executables in a branch called "distribution". Now Bob should create a metajson file and
screenshots describing the application in the main branch (or in a separate one, depending on Bob's preference). Of
course Bob could have used separate gitlimb projects or even separate servers for the binaries and the package description,
or he could have also used a static webserver for the zip payloads and the metajson.
```sh
$ cat bobsapp.json
{
  "id": "bobsapp",
  "description": [
    { "en", "Bob's Application", "A very cool stuff for Cecil" }
  ],
  "version": "1.0.0",
  "release": "1.0-beta-rc",
  "license": "MIT",
  "category": "tools",
  "url": "https://gitlimb.com/bob/bobsapp/archive/distribution/$ARCH.zip",
  "screenshots": [
    "https://gitlimb.com/bob/bobsapp/raw/master/docs/logo.png",
    "https://gitlimb.com/bob/bobsapp/raw/master/docs/screen1.png",
    "https://gitlimb.com/bob/bobsapp/raw/master/docs/screen2.png"
  ]
}
```
Now Bob should build a package by adding checksums to the metajson and signing it with his maintainer certificate.
```sh
$ syspkg build bobsapp.json
$ git commit -a -m "New bobsapp.json file"
$ git push
```
When he is finished and uploaded the signed metajson to gitlimb.com, he should send this url to Alice. (Note: if Bob
would have choosen to use a static webserver, then he could have used syspkg to generate the zip payloads for him by
specifing the architecture and a directory, like `syspkg build bobsapp.json x86_64=buildir aarch64=build2dir`.)

Alice: receives Bob's package's metajson url. She also has a gitlimb.com account just like Bob, where she creates
a plain text file with Bob's url (along with her own urls or other's) and appends her certificate after the url list.
```sh
$ cat aliceos.txt
https://gitlimb.com/alice/aliceos/raw/master/base.json
https://gitlimb.com/bob/bobsapp/raw/master/bobsapp.json
$ syspkg build aliceos.txt
$ git commit -a -m "Updated AliceOS' package list"
$ git push
```
Of course she could have used a different code hosting, or even a static webserver too.

She ports `bin/syspkg` to her OS, and into her OS' installer image, she adds a syspkg config file, so that everyone
installing AliceOS will know about her gitlimb repository list by default.
```sh
$ du -h /mnt/aliceos_root/bin/syspkg
576K  /mnt/aliceos_root/bin/syspkg
$ cat /mnt/aliceos_root/etc/syspkg.json
{
    "repos": [ "https://gitlimb.com/alice/aliceos/raw/master/aliceos.txt" ]
}
```

Using the package
-----------------

Okay, it is Cecil's turn. He installs AliceOS, and after he successfully logged in as the root user, refreshes
the package database. Because Alice has added a default repo configuration, this will work out-of-the-box.
```sh
# syspkg update
```
Then Cecil can search for packages, looking for Bob's application. Or if he knows the package name, he can install
it right away.
```sh
# syspkg search "Bob's"

tools/bobsapp 1.0-rc-beta
    Bob's Application - A very cool stuff for Cecil

# syspkg install bobsapp
```
That's it!

Now Cecil isn't nice, wants to betray Bob and cheat on Alice. So he adds someone else's repository too.
```sh
# syspkg repo https://gitlimb.com/notalice/aliceos3rdparty/raw/master/aliceos3rdparty.txt
# syspkg update
```
After that Cecil can search, list and install packages from notalice's repository as well.

New release of the package
--------------------------

Cecil: finds a bug in Bob's application, and asks Bob to fix it.

Bob: fixes it. After that Bob has to bump the version in the metajson and regenerate the checksums and signature.
```sh
$ cat bobsapp.json | grep version
  "version": "1.0.1",
$ syspkg build bobsapp.json
$ git commit -a -m "Updated bobsapp.json file"
$ git push
```

Cecil: updates the package list and upgrades to the latest version.
```sh
# syspkg update
# syspkg upgrade
```

Please note that providing a new version of Bob's package did not involve Alice in any way.

Revoking and regranting rights
------------------------------

Bob: finds out that Cecil is using someone else's application too, so out of jeliousy he puts a virus in Bob's app to teach
Cecil a lession (so he thinks).

Alice: learns that Bob cannot be trusted, but forgot which packages were Bob's. That would be the easiest and simpler way,
to remove Bob's metajson urls, but if there are lots and lots of url, Alice could forget which one was Bob's. So she wants
to stop Bob distributing packages through her repository alltogether. In order to do that, Alice has to revoke Bob's
certificate, and refresh the signature on the repository list.
```sh
$ syspkg cert --revoke signed.pem
$ syspkg build aliceos.txt
$ git commit -a -m "Updated AliceOS' package list"
$ git push
```
The aliceos.txt file should contain an url list, Alice's certificate, and now a CRL block too. Even though Alice forget
to remove bobsapp's url from the list, this will stop Cecil and the other users from installing or upgrading bobsapp.

Bob: makes peace with Cecil, removes the virus, but he cannot convince Alice. Because Bob's certificate is still signed
by Alice, he'll have to re-create his cert, and sign the new, virus-free version with his non-signed certificate.
```sh
$ syspkg cert "US" "Bob Doe <doe@notgmail.com>"
$ cat bobsapp.json | grep version
  "version": "1.0.2",
$ syspkg build bobsapp.json
$ git commit -a -m "Updated bobsapp.json file"
$ git push
```

Cecil: makes peace with Bob too, and wants to use Bob's app even though Bob doesn't have a signed certificate. So he
has to add an exception:
```sh
# syspkg trust https://gitlimb.com/bob/bobsapp/raw/master/bobsapp.json
# syspkg upgrade
```
If in the meantime Alice has found Bob's metajson url and removed that from the list, then upgrade won't pick up
bobsapp. But Cecil still can re-install Bob's application by using the metajson url directly instead of the package
name:
```sh
# syspkg install https://gitlimb.com/bob/bobsapp/raw/master/bobsapp.json
```
This only works if Cecil apriori has trusted Bob's self-signed certificate.

Alice: makes peace with Bob too. All she has to do is re-signing Bob's certificate and refresh aliceos.txt on gitlimb.
```sh
$ syspkg cert --sign maintainer.pem >signed.pem
$ echo "https://gitlimb.com/bob/bobsapp/raw/master/bobsapp.json" >>aliceos.txt
$ syspkg build aliceos.txt
$ git commit -a -m "Updated AliceOS' package list"
$ git push
```

Bob: gets his new signed certificate, but this won't allow bobsapp back into AliceOS. Bob also has to import this resigned
certificate and sign the metajson with it:
```sh
$ syspkg cert signed.pem
$ syspkg build bobsapp.json
$ git commit -a -m "Updated bobsapp.json file"
$ git push
```
Now Cecil and all the other users will see bobsapp in the packages list after an update.
