Porting The System Packaging Library
====================================

When porting to new operating systems, edit `src/lib/config.h` and tailor it to your needs. Take a look at one of the
config.OS.h files to get ideas.

To integrate libsyspkg with your OS, all you need is a `libc` (truth to be told, a fairly complete one with Berkeley
sockets, but that's where the list of dependencies end). If you have a special, non-socket based networking API, then
you'll have to edit `src/lib/mbedtls/library/net_sockets.c`. It already has the required ifdefs for the WIN32 API. That
file isn't provided by libsyspkg rather by mbedtls. Alternatively you can turn networking support entirely off by editing
`src/lib/mbedtls/include/mbedtls/config.h` and commenting out the `#define MBEDTLS_NET_C` line. You'll still be able
to use local files and "file://" URLs in repositories, package meta info and payloads. This is enough if your OS is
distributed on a CDROM for example.

If your target OS is a POSIX compliant one, nothing else left to do. If it isn't, then take a look at `src/lib/files.c`,
all file system related functions are implemented there. That's one of the two files in the source tree which have ifdef OS
guards. Besides of POSIX, it is also written for the WIN32 API. There's one more OS specific call in `src/lib/postinst.c`,
which should execute a command with specific environment variables (also written for both POSIX and WIN32 API). All the other
files contain only OS independent, portable C code.
