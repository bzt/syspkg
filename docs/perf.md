Performance
-----------

Generated 32768 packages, 8421376 files, 2.2 Gb payload (zips). Update is the slowest operation.

Update without certificate checks and without attachments
0m10.594s

Update with certificate and signature checks but without attachments
0m58.363s

Update with all features
1m43.781s

Search and which and every other operations less than a sec.
