RendszerCsomag Kezelő Függvénykönyvtár
======================================

Indoklás
--------

Minek? Mert legyünk őszinték, minden jelenlegi csomagkezelő sz*pás, nem kicsit, nagyon. Miért van egy rahedli parancs a debnél?
Telepítés "apt-get", csomagok listázása "dpkg", keresés "apt-cache", fájlok keresése "dpkg-query", beállítás "dpkg-reconfigure"?
És mégis ki gondolta akár egy röpke pillanatig is, hogy a pacman kapcsolói épeszűek? "-Ss" a keresés, "-S" a telepítés, "-Syu"
a frissítés? Miért nem jó a "-s", "-i", "-u"? Mert akkor nem tudnád véletlenül összekeverni a keresést a telepítéssel? És minek
van szükség "yaourt"-ra egyáltalán? Az "emerge" kapcsolói igazából egész jók, de miért van szükség külön parancsokra, mint
"e-file", "qpkg" vagy "ebuild"?

Többnyelvűsítés? Lefordítható csomag leírások, előnézeti- és képernyőképek? Hát azok meg minek, igaz?

Miért nem képes egyetlen meglévő csomagkezelő sem kezelni a körkörös hivatkozásokat és eltávolítani a felesleges csomagokat
maguktól? Futottál már bele függőségi pokolba (dependency hell)? Miért foglalják a rettentő értékes tárhelyet a már felesleges
csomagok és a letöltött tömörített fájlok (hogy kézzel kelljen az elárvult csomagokat levadászni és a /var/cache-t törölgetni)?

És mi ez a felfordulás a repók kapcsán? Még álmodni se merek róla, hogy a certeket adjam hozzá, miért kell GPG kulcsokkal
zsonglőrködni (még egy újabb program és függőség a láncban)? És ha megvan a kulcs, miért olyan bonyolult hozzáadni azt is
meg a felhasználó által definiált repókat is? Minek kezeli az emerge és az apt is egy külön csomagban a kulcsokat, ahelyett,
hogy csomagonként tárolná? És persze el is hasalnak ha *kézzel* nem frissítem a trusted.gpg.d és archlinux-keyring csomagokat
elsőnek? Miért nem tudom csak úgy hozzáadni a .deb és AUR fájlaimat és a frissítéseit a többivel együtt kezelni? Stb. stb. stb.
stb. stb. megannyi komoly kezelésbeli hiányosság.

Végezetül, próbálta már valaki portolni bármelyik meglévő csomagkezelőt egy új rendszerre? Rémálom! Freddy egy altató hozzájuk
és a függőségeikhez képest! Miért is fontos, hogy egy csomagkezelő egyáltalán ne függjön más csomagoktól? Hát mert az A
csomagkezelő! Nem állhat le a működése csak azért, mert valaki eltávolította az unzip csomagot például, igaz? Máskülönben
hogy tudná újrarakni? Egyszer egy komplett Gentoo rendszert újra kellett húznom, mert az emerge függőségi poklot okozott a
saját függőségei frissítésekor...

A lényegre térve, itt egy nagyon egyszerű, könnyen használható, teljes értékű, folyamatos-kiadás (rolling-release) alapú
rendszercsomag kezelő megoldás, egy önálló, függőségmentes ANSI C függvénykönyvtárként implementálva. Becsomagolhatod egy
parancssoros vagy akár ha szeretnéd, egy ablakos alkalmazásba is, hogy az operációs rendszered natív interfészét használja.

Használat
---------

Felhasználóbarátnak kell lennie, rövid betanulási idővel és könnyedén megjegyezhető kapcsolókkal. A függvénykönyvtárral
kapcsolatban lásd az [API dokumentáció](https://gitlab.com/bztsrc/syspkg/-/blob/master/docs/API.md)ját.

Hogy gyorsan nekikezdhess, a függvénykönyvtár be lett csomagolva egy [demó alkalmazás](https://gitlab.com/bztsrc/syspkg/-/tree/master/src/cli)ba.
```sh
$ syspkg (parancs) [paraméterek]
```
Habár teljesen működőképes, az elsődleges célja az API használatának a bemutatása. Az elképzelés az, hogy egy natív
csomagkezelőt írsz az operációs rendszeredhez a függvénykönyvtár felhasználásával, semmint hogy ezt a parancssoros demó
alkalmazást portold (de persze ez utóbbi is működhet).

### Parancsok végfelhasználóknak

| Kapcsoló / Parancs   | Leírás                                                  |
|----------------------|---------------------------------------------------------|
| `-u` / `update`      | A legfrissebb csomaginformációk letöltése a repókból    |
| `-U` / `upgrade`     | Minden csomag frissítése, amiből van újabb verzió       |
| `-l` / `list`        | Telepített csomagok listázása                           |
| `-s` / `search`      | Keresés a csomag nevében és leírásában                  |
| `-w` / `which`       | Melyik csomag tartalmazza a keresett fájlt              |
| `-d` / `depends`     | Mely csomagok függenek a megadott csomagtól             |
| `-i` / `info`        | Részletes csomag információ                             |
| `-I` / `install`     | Csomag (újra)telepítése vagy frissítése                 |
| `-r` / `reconf`      | Telepített csomag újrakonfigurálása                     |
| `-R` / `remove`      | Csomag, repó eltávolítása vagy tanúsítvány visszavonása |
| `-A` / `repo`        | Repó hozzáadása vagy konfigurált repók listázása        |
| `-T` / `trust`       | Tanúsítvány hozzáadása a megbízhatók listához           |

### Parancsok feljesztőknek és csomag- valamint repókarbantartóknak

| Kapcsoló / Parancs   | Leírás                                                  |
|----------------------|---------------------------------------------------------|
| `-c` / `cert`        | Tanúsítvány generálása                                  |
| `-S` / `cert --sign` | Feljesztői tanúsítvány aláírása repó tanúsítvánnyal     |
| `-C` / `check`       | Csomag metaadat és tömörített adat ellenőrzése          |
| `-B` / `build`       | Metaadat (és/vagy tömörített fájl), url lista aláírása  |

Bővebb leírást a [karbantartók kézikönyvé](https://gitlab.com/bztsrc/syspkg/-/blob/master/docs/maintainers.md)ben találsz
a csomagok és repók léterhozásáról. Ez a függvénykönyvtár nemcsak a csomagok kezeléséről gondoskodik, hanem a léterhozásukról
is. Egy részletes, lépésről lépésre HOGYAN leírást is találsz az [oktatóanyag](https://gitlab.com/bztsrc/syspkg/-/blob/master/docs/tutorial.md)ban.

Csomag repók
------------

A syspkg mögötti koncepció pofonegyszerű, de több rétegű. Az alapfeltevés az, hogy a szoftverek az utóbbi időben átkerültek
olyan kódszolgáltatásokra, mint a github vagy a gitlab (de persze a régimódi statikus tömörített fájlok egy webszerveren
metódust is támogatja).

| Szint              | Leírás                                                                                |
|--------------------|---------------------------------------------------------------------------------------|
| repó lista         | (repo list) A felhasználó gépén egy repó URL-eket tartalmazó lista                    |
| repó               | (repository) Egy sima szöveges fájl metaadat URL lista                                |
| metaadat           | (metainfo) Egy JSON fájl, ami leírja a csomagot, verzióit és tömörített fájl URL-jeit |
| tömörített fájl    | (payloads) Tömörített archív fájlok (tarball-ok), a csomag fájlait tartalmazzák       |

Fentről lefelé: a repó lista egy konfigurációs fájl, amit a felhasználó a kedvére szerkeszthet a gépén. Bármikor
kénye kedve szerint új repót adhat hozzá vagy távolíthat el, akárcsak a tanúsítványaikat. Az operációs rendszerek
készítői saját alapértelmezett listát biztosíthatnak (egy URL az alapcsomagoknak, egy a közösség által biztosított
csomagoknak, egy a nem-szabad csomagoknak stb.) valamint előtelepíthetik a hivatalos tanúsítványaikat is.

Az `update` parancsra a libsyspkg letölti ezeket a repó fájlokat és a metaadatok URL-jeit egy nagy listába fűzi.
Ezután végigmegy a listán és letölti a metaadatokat is. Ezzel az elérhető csomagok listája helyben letárolódik egy
adatbázisba.

A `search`, `which`, `depends` és `list` parancsok ezen a helyi adatbázison dolgoznak.

Ezek a metaadatok leírják, hogy honnan tölthető le a program. További infókat is tartalmaznak, mint például a
lefordított csomagleírás vagy képernyőkép URL-ek. Teljesen elképzelhető, hogy egy harmadik fél kreál metaadatot
egy teljesen más szerveren lévő csomagnak.

Az `install` során a metaadatokban található függőségek is automatikusan telepítésre kerülnek. Van egy hivatkozás
számláló minden csomagon, és a felhasználó telepítése egy hivatkozásnak számít. A körkörös hivatkozásokat észleli,
és mindkét csomagot egyszerre felrakja. Ha sikerül elrontani egy csomag konfigurációs fájlját, újrafuttatható a
telepítéskor lefutattott csomag felkonfigurálás a `reconf` kapcsolóval.

A `remove` során a csomag és minden tőle függő törlésre kerül. Azon csomagok számlálói, amiktől az eltávolított csomag
függött, eggyel csökkennek, és ha elérték a nullát, szintén eltávolításra kerülnek.

Ennyi.

<img src="https://gitlab.com/bztsrc/syspkg/-/raw/master/docs/syspkg_overview.png" alt="Áttekintés">

### A fő konfigurációs fájl

Mindössze egyetlen egy konfigurációs fájl van, ami a repó URL-eket listázza. Ez egy
[JSON](https://gitlab.com/bztsrc/syspkg/-/blob/master/docs/conf_schema.json) fájl a következő mezőkkel: `lang`, `osver`,
`arch`, `license` (opcionálisak) és `repos` (kötelező). A szűrők befolyásolják, hogy mely csomagok kerülnek listázásra és
telepítésre, az utolsó pedig egy sima URL sztringlista. Ha nincsenek megadva a szűrők alapértelmezetten rendre a "LANG"
környazeti változó értéke, bármilyen OS verzió, a fordításkor használt architektúra és bármilyen licensz. Ha több architektúra
is meg van adva, akkor ez eldönti a preferenciasorrendet is: ha egy csomagnak több tömörített változata is van, akkor az
első, listában felsorolttal egyező kerül telepítésre.

Példa:
```
{
    "lang": "en_GB",
    "osver": "bugsir",
    "arch": [ "x86_64", "x86_32", "any" ],
    "license": [ "MIT", "BSD", "GPL", "LGPL", "PD" ],
    "repos": [
        "https://gitlab.com/bztsrc/syspkg/-/raw/examplerepo/base.txt",
        "https://gitlab.com/bztsrc/syspkg/-/raw/examplerepo/community.txt",
        "https://gitlab.com/bztsrc/syspkg/-/raw/examplerepo/extra.txt",
        "https://gitlab.com/bztsrc/syspkg/-/raw/examplerepo/nonfree.txt",
        "https://github.com/someone/coolstuff/raw/master/repository.txt",
        "https://somewebserver.com/3rdpartytools.txt"
    ]
}
```
