The "syspkg" Command Line Utility
=================================

This simple CLI tool is just a wrapper around [libsyspkg](https://gitlab.com/bztsrc/syspkg/-/tree/master/src/lib). It really
does nothing else than parsing command line arguments and calling functions from that library.

It is a good example on how to use the [syspkg API](https://gitlab.com/bztsrc/syspkg/-/blob/master/docs/API.md), what returned
error codes mean and how and where package information is returned.
