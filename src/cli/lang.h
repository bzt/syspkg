/*
 * syspkg/src/cli/lang.h
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Multilanguage support
 *
 */

enum {
    MSG_GENKEYS = 0,
    MSG_GENCERT,
    MSG_GENPAYLOAD,
    MSG_CHKPAYLOAD,
    MSG_DLDPAYLOAD,
    MSG_GETREPO,
    MSG_GETMETA,
    MSG_CLNMETA,
    MSG_GENHTML,
    MSG_DELPKG,
    MSG_UNPPKG,
    MSG_CFGPKG,
    MSG_ERROR,
    MSG_OK,
    CONFPKG,
    ACCEPTTERMS,
    DEFAULT,
    USAGE,
    COMMAND,
    PARAMS,
    HELP_YES,
    HELP_NODEPS,
    HELP_ENDUSERS,
    HELP_UPDATE,
    HELP_UPGRADE,
    HELP_LIST,
    HELP_SEARCH,
    HELP_WHICH,
    HELP_DEPENDS,
    HELP_INFO,
    HELP_INSTALL,
    HELP_RECONF,
    HELP_REMOVE,
    HELP_REPO,
    HELP_TRUST,
    HELP_DEVS,
    HELP_PRTCERT,
    HELP_GENMNTR,
    HELP_IMPORT,
    HELP_BUILD,
    HELP_REPOS,
    HELP_GENREPO,
    HELP_SIGN,
    HELP_REVOKE,
    HELP_CHECK,
    HELP_HTML,
    ERR_NOCERT,
    ERR_NOREPOCERT,
    ERR_NOTMNTRCERT,
    ERR_NOMEM,
    ERR_SIGN,
    ERR_NOTSIGNEDBY,
    ERR_REVOKE,
    ERR_NOCERTFND,
    ERR_WRTRUST,
    ERR_NOTADMIN,
    ERR_ADDCERT,
    ERR_NOMETA,
    ERR_RDMETA,
    ERR_PRMETA,
    ERR_ARCH,
    ERR_PAYLOAD,
    ERR_WRMETA,
    ERR_SGMETA,
    ERR_GENPAYLD,
    ERR_NOMETAPL,
    ERR_RDFILE,
    ERR_BADFMT,
    ERR_UNKFMT,
    ERR_CHECK,
    ERR_NOREPOS,
    ERR_REPOADDED,
    ERR_WRCONF,
    ERR_ADDREPO,
    ERR_REMPAR,
    ERR_INVREMPAR,
    ERR_LOCKED,
    ERR_REM,
    ERR_UPDATE,
    ERR_NOPATTERN,
    ERR_NOPKGSPEC,
    ERR_UPGRADE,
    ERR_INSTPAR,
    ERR_NOSPACE,
    ERR_OPER,
    ERR_DOWNLOAD,
    ERR_CHKSUM,
    ERR_PERM,
    ERR_NOPKGNAME,
    ERR_NOTINST,
    ERR_CONF,
    PACKAGES,
    UNREACH,
    PKGSUMMARY,
    RELEASE,
    INSTALLED,
    VERSION,
    CATEGORY,
    HOMEPAGE,
    BUGTRACKER,
    MAINTAINER,
    LICENSE,
    PKGDEPENDS,
    PKGSUGGESTS,
    PKGCONFLICTS,
    UPTODATE,
    CONFLICTING,
    CONFLICTS,
    MISSING,
    TOBEREMOVED,
    SUGGESTED,
    TOBEINSTALLED,
    TOBEUPGRADED,
    DOWNLOADSIZE,
    NEEDEDSPACE,
    PROCEED,
    /* must be the last */
    NUMTEXTS
};

#define NUMLANGS         2

extern char *dict[NUMLANGS][NUMTEXTS + 1], **lang;

