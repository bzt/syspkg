/*
 * syspkg/src/cli/main.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Example command line interface demo around libsyspkg
 *
 */

#include <stdio.h>
#include <string.h>
#include <syspkg.h>

#ifdef __WIN32__
#define FLAG "/"
#else
#define FLAG "--"
#endif

/* multilanguage support */
#include "lang.h"

/* from internal.h, to write certs to file */
int syspkg_writefileall(char *fn, unsigned char *buf, size_t len);

/**
 * Progressbar callback
 */
void progressbar(int step, int numstep, int64_t curr, int64_t total, int msg)
{
    int64_t perc = curr * 100;
    printf("\r");
    if(numstep) printf("%3d / %3d ", step, numstep);
    printf("%s... ", lang[msg]);
    if(curr < total && total != 0) printf("%d%%\033[K", (int)(perc / total));
    else if(total < 0) printf("\033[31;1m%s\033[0m\033[K\n", lang[MSG_ERROR]);
    else printf("\033[32m%s\033[0m\033[K\n", lang[MSG_OK]);
    fflush(stdout);
}

/**
 * Package configuration callback
 */
int conf(char *id, char *name, char *terms, int len, syspkg_input_t *input)
{
    int i, j;
    char tmp[32];
    if(terms || input)
        printf( "\n%s %s (%s)\n"
                "-------------------------------------------------\n", lang[CONFPKG], name, id);
    if(terms) {
        printf( "%s\n%s? ", terms, lang[ACCEPTTERMS]);
        i = getchar();
        if(i != '\r' && i != '\n' && i != 'y') return 0;
        printf("\n");
    }
    if(input) {
        for(i = 0; i < len; i++) {
            if(input[i].desc)
                printf("%s\n", input[i].desc);
            switch(input[i].type) {
                case SYSPKG_FORM_STR:
                    printf("%s", input[i].name);
                    if(input[i].spec.str) printf(" (%s: %s)", lang[DEFAULT], input[i].spec.str);
                    printf("? ");
                    input[i].value = (char*)malloc(BUFSIZ);
                    if(input[i].value && !fgets(input[i].value, BUFSIZ, stdin)) {
                        free(input[i].value);
                        input[i].value = NULL;
                    }
                    printf("\n");
                    break;
                case SYSPKG_FORM_NUM:
                    printf("%s (min: %d, max: %d, %s: %d)? ", input[i].name, input[i].spec.num.min, input[i].spec.num.max,
                        lang[DEFAULT], input[i].spec.num.def);
                    input[i].value = (char*)malloc(32);
                    if(input[i].value) {
                        if(!fgets(input[i].value, 32, stdin)) {
                            free(input[i].value);
                            input[i].value = NULL;
                        } else
                        if(atoi(input[i].value) < input[i].spec.num.min) {
                            sprintf(input[i].value, "%d", input[i].spec.num.min);
                        } else
                        if(atoi(input[i].value) > input[i].spec.num.max) {
                            sprintf(input[i].value, "%d", input[i].spec.num.max);
                        }
                    }
                    printf("\n");
                    break;
                case SYSPKG_FORM_CHK:
                    printf("%s (0: %s, 1: %s)? ", input[i].name, input[i].spec.chk.disabled, input[i].spec.chk.enabled);
                    input[i].value = input[i].spec.chk.disabled;
                    if(fgets(tmp, sizeof(tmp), stdin) && tmp[0] == '1')
                        input[i].value = input[i].spec.chk.enabled;
                    printf("\n");
                    break;
                case SYSPKG_FORM_SEL:
                    for(j = 0; j < input[i].spec.sel.len && j < 26; j++)
                        printf(" %c. %s\n", 'a' + j, input[i].spec.sel.opts[j]);
                    printf("%s (a - %c)? ", input[i].name, 'a' + j - 1);
                    if(fgets(tmp, sizeof(tmp), stdin))
                        input[i].value = input[i].spec.sel.opts[(tmp[0] == '\r' || tmp[0] == '\n' || tmp[0] < 'a' || tmp[0] > 'z'
                            || tmp[0] - 'a' > input[i].spec.sel.len) ? 0 : tmp[0] - 'a'];
                    printf("\n");
                    break;
            }
        }
    }
    return 1;
}

/**
 * Print large number in human readabale format
 */
void print_num(long long int num)
{
    int sign = 1;
    char *unit = "b";
    if(num < 0) { sign = -1; num = -num; }
    if(num > 1023) { unit = "Kb"; num += 1023; num >>= 10;
        if(num > 1023) { unit = "Mb"; num += 1023; num >>= 10;
            if(num > 1023) { unit = "Gb"; num += 1023; num >>= 10; } } }
    printf("%ld %s", (long int)(sign*num), unit);
}

/* for testing purposes */
/*
void savetga(unsigned int aidx)
{
    unsigned int *img = syspkg_loadattachment(aidx, 0);
    if(img) {
        FILE *f = fopen("img.tga", "wb");
        unsigned char tmp[18] = {0};
        tmp[2] = 2;
        memcpy(tmp + 10, &img[0], 2);
        memcpy(tmp + 12, &img[0], 2);
        memcpy(tmp + 14, &img[1], 2);
        tmp[16] = 32;
        tmp[17] = 40;
        fwrite(tmp, 18, 1, f);
        fwrite(&img[2], img[0]*img[1]*4, 1, f);
        fclose(f);
        free(img);
    } else printf("attachment %d not found\n", aidx);
}
*/

/**
 * Main function, parses arguments and calls the library
 */
int main(int argc, char **argv)
{
    int i, isrepo = 0, ret, yes = 0, nodeps = 0, in = 0, up = 0;
    char *c, **arg = &argv[1], *jsonflds[] = { "id", "description", "version", "url", "category", "payloads", "signature", NULL };
    syspkg_ctx_t *ctx = syspkg_new(progressbar, conf);
    for(i = 0; i < NUMLANGS; i++)
        if(!strncmp(ctx->lang, dict[i][0], strlen(dict[i][0]))) break;
    if(i >= NUMLANGS) i = 0;
    lang = &dict[i][1];
    if(argc < 2) {
usage:  printf("%s:\n  syspkg [" FLAG "yes] [" FLAG "nodeps] [%s] [%s]\n\n"
            "  " FLAG "yes\t\t\t\t\t\t%s\n"
            "  " FLAG "nodeps\t\t\t\t\t%s\n\n",
            lang[USAGE], lang[COMMAND], lang[PARAMS], lang[HELP_YES], lang[HELP_NODEPS]);
        printf("%s\n"
            "  syspkg update\t\t\t\t\t%s\n"
            "  syspkg upgrade\t\t\t\t%s\n"
            "  syspkg list <pattern>\t\t\t\t%s\n"
            "  syspkg search <pattern>\t\t\t%s\n"
            "  syspkg which <pattern>\t\t\t%s\n"
            "  syspkg depends <package>\t\t\t%s\n"
            "  syspkg info <package>\t\t\t\t%s\n"
            "  syspkg install <package|json>\t\t\t%s\n"
            "  syspkg reconf <package>\t\t\t%s\n"
            "  syspkg remove <package|json|certcn|repourl>\t%s\n"
            "  syspkg repo [url]\t\t\t\t%s\n"
            "  syspkg trust <certfile|jsonfile|url>\t\t%s\n"
            "\n", lang[HELP_ENDUSERS], lang[HELP_UPDATE], lang[HELP_UPGRADE], lang[HELP_LIST], lang[HELP_SEARCH], lang[HELP_WHICH],
            lang[HELP_DEPENDS], lang[HELP_INFO], lang[HELP_INSTALL], lang[HELP_RECONF], lang[HELP_REMOVE], lang[HELP_REPO],
            lang[HELP_TRUST]);
        printf("%s\n"
            "  syspkg cert\t\t\t\t\t%s\n"
            "  syspkg cert <countrycode> <your name>\t\t%s\n"
            "  syspkg cert <certfile>\t\t\t%s\n"
            "  syspkg build <json> [<arch>=<dir|zip> [...]]\t%s\n"
            "\n", lang[HELP_DEVS], lang[HELP_PRTCERT], lang[HELP_GENMNTR], lang[HELP_IMPORT], lang[HELP_BUILD]);
        printf("%s\n"
            "  syspkg cert " FLAG "repo <countrycode> <your name>\t%s\n"
            "  syspkg cert " FLAG "sign <certfile> [outcertfile]\t%s\n"
            "  syspkg cert " FLAG "revoke <certfile>\t\t%s\n"
            "  syspkg check <json|payload>\t\t\t%s\n"
            "  syspkg build <urllisttxtfile> [htmlout]\t%s\n"
            "\n", lang[HELP_REPOS], lang[HELP_GENREPO], lang[HELP_SIGN], lang[HELP_REVOKE], lang[HELP_CHECK], lang[HELP_HTML]);
        syspkg_free(ctx);
        return 0;
    }

    if(arg[0] && (!strcmp(arg[0], FLAG "yes") || (arg[0][0] == FLAG[0] && arg[0][1] == 'y'))) {
        yes = 1;
        arg++;
    }
    if(arg[0] && (!strcmp(arg[0], FLAG "nodeps") || (arg[0][0] == FLAG[0] && arg[0][1] == 'n'))) {
        nodeps = 1;
        arg++;
    }

    /*** generate, sign or revoke certificates */
    if(arg[0] && arg[0][0] == FLAG[0] && arg[0][1] == 'S') goto sign;
    if(arg[0] && (!strcmp(arg[0], "cert") || (arg[0][0] == FLAG[0] && arg[0][1] == 'c'))) {
        arg++;
        if(arg[0] && !strcmp(arg[0], FLAG "sign")) {
sign:       arg++;
            if(!arg[0]) {
                fprintf(stderr, "%s\n", lang[ERR_NOCERT]);
                syspkg_free(ctx);
                return 1;
            }
            if((ret = syspkg_sign(ctx, arg[0])) || !ctx->cert) {
                switch(ret) {
                    case EINVAL: fprintf(stderr, "%s\n", lang[ERR_NOREPOCERT]); break;
                    case EBADF: fprintf(stderr, "%s\n", lang[ERR_NOTMNTRCERT]); break;
                    case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                    default: fprintf(stderr, "%s\n", lang[ERR_SIGN]); break;
                }
                syspkg_free(ctx);
                return 1;
            }
            if(arg[1])
                syspkg_writefileall(arg[1], (unsigned char*)ctx->cert, strlen(ctx->cert));
            else
                printf("%s", ctx->cert);
        } else
        if(arg[0] && !strcmp(arg[0], FLAG "revoke")) {
            arg++;
            if(!arg[0]) {
                fprintf(stderr, "%s\n", lang[ERR_NOCERT]);
                syspkg_free(ctx);
                return 1;
            }
            if((ret = syspkg_revoke(ctx, arg[0]))) {
                switch(ret) {
                    case EINVAL: fprintf(stderr, "%s\n", lang[ERR_NOREPOCERT]); break;
                    case EBADF: fprintf(stderr, "%s\n", lang[ERR_NOTSIGNEDBY]); break;
                    case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                    default: fprintf(stderr, "%s\n", lang[ERR_REVOKE]); break;
                }
                syspkg_free(ctx);
                return 1;
            }
            printf("%s", ctx->cert);
        } else {
            if(arg[0] && !strcmp(arg[0], FLAG "repo")) { isrepo = 1; arg++; }
            if(arg[0]) syspkg_cert(ctx, arg[1], arg[0], isrepo); else {
                if(syspkg_cert(ctx, NULL, NULL, 0) || !ctx->cert) {
                    fprintf(stderr, "%s\n", lang[ERR_NOCERTFND]);
                    syspkg_free(ctx);
                    return 1;
                }
                printf("%s", ctx->cert);
            }
        }
    } else

    /*** trust a maintainer certificate ***/
    if(arg[0] && (!strcmp(arg[0], "trust") || (arg[0][0] == FLAG[0] && arg[0][1] == 'T'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_NOCERT]);
            syspkg_free(ctx);
            return 1;
        }
        if((ret = syspkg_trust(ctx, arg[0]))) {
            switch(ret) {
                case EPERM: fprintf(stderr, "%s\n", lang[ERR_WRTRUST]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_ADDCERT]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
    } else

    /*** build packages or repository lists ***/
    if(arg[0] && (!strcmp(arg[0], "build") || (arg[0][0] == FLAG[0] && arg[0][1] == 'B'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_NOMETA]);
            syspkg_free(ctx);
            return 1;
        }
        arg++;
        if((ret = syspkg_build(ctx, arg[-1], argc - 3, arg))) {
            switch((ret & 0xFFFF)) {
                case ENOENT: fprintf(stderr, "%s\n", lang[ERR_RDMETA]); break;
                case EBADF: fprintf(stderr, "%s '%s'\n", lang[ERR_PRMETA], jsonflds[ret >> 16]); break;
                case EINVAL: fprintf(stderr, "%s '%s'\n", lang[ERR_ARCH], arg[ret >> 16]); break;
                case EACCES: fprintf(stderr, "%s '%s'\n", lang[ERR_PAYLOAD], arg[ret >> 16]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                case EPERM: fprintf(stderr, "%s\n", lang[ERR_WRMETA]); break;
                case EFAULT: fprintf(stderr, "%s\n", lang[ERR_SGMETA]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_GENPAYLD]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
    } else

    /*** check package validity and integrity ***/
    if(arg[0] && (!strcmp(arg[0], "check") || (arg[0][0] == FLAG[0] && arg[0][1] == 'C'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_NOMETAPL]);
            syspkg_free(ctx);
            return 1;
        }
        if((ret = syspkg_check(ctx, arg[0]))) {
            switch((ret & 0xFFFF)) {
                case ENOENT: fprintf(stderr, "%s\n", lang[ERR_RDFILE]); break;
                case EINVAL: fprintf(stderr, "%s\n", lang[ERR_BADFMT]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_UNKFMT]); break;
                case EBADF: fprintf(stderr, "%s '%s'\n", lang[ERR_PRMETA], jsonflds[ret >> 16]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_CHECK]); break;
            }
            ret = 1;
        }
        if(ctx->cert) printf("%s", ctx->cert);
        syspkg_free(ctx);
        return ret;
    } else

    /*** add repository or list configured repositories ***/
    if(arg[0] && (!strcmp(arg[0], "repo") || (arg[0][0] == FLAG[0] && arg[0][1] == 'A'))) {
        arg++;
        if(!arg[0]) {
            if(syspkg_listrepo(ctx) || !ctx->repos) {
                fprintf(stderr, "%s\n", lang[ERR_NOREPOS]);
                syspkg_free(ctx);
                return 1;
            }
            for(i = 0; ctx->repos[i].url; i++) {
                printf("%s\n    %s\n    %d %s\n\n",
                    ctx->repos[i].url,
                    ctx->repos[i].cn ? ctx->repos[i].cn : lang[UNREACH],
                    ctx->repos[i].num, lang[PACKAGES]);
            }
        } else
        if((ret = syspkg_addrepo(ctx, arg[0]))) {
            switch(ret) {
                case EEXIST: fprintf(stderr, "%s\n", lang[ERR_REPOADDED]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                case ENOENT: fprintf(stderr, "%s\n", lang[ERR_RDFILE]); break;
                case EPERM: fprintf(stderr, "%s\n", lang[ERR_WRCONF]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_ADDREPO]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
    } else

    /*** remove package, certificate or repository ***/
    if(arg[0] && (!strcmp(arg[0], "remove") || (arg[0][0] == FLAG[0] && arg[0][1] == 'R'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_REMPAR]);
            syspkg_free(ctx);
            return 1;
        }
        if((ret = syspkg_remove(ctx, arg, nodeps))) {
            switch(ret) {
                case ENOENT: fprintf(stderr, "%s\n", lang[ERR_INVREMPAR]); break;
                case EBADF:  fprintf(stderr, "%s\n", lang[ERR_NOTINST]); break;
                case EEXIST: fprintf(stderr, "%s\n", lang[ERR_LOCKED]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_REM]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
        if(ctx->removes) goto confirm;
    } else

    /*** update or search local package database ***/
    if(arg[0] && (!strcmp(arg[0], "update") || (arg[0][0] == FLAG[0] && arg[0][1] == 'u'))) {
        if((ret = syspkg_update(ctx))) {
            switch(ret) {
                case EEXIST: fprintf(stderr, "%s\n", lang[ERR_LOCKED]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_UPDATE]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
        printf(lang[PKGSUMMARY], ctx->numfiles, ctx->numinst, ctx->numpackages);
        printf("\n");
    } else
    if(arg[0] && (!strcmp(arg[0], "which") || (arg[0][0] == '-' && arg[0][1] == 'w'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_NOPATTERN]);
            syspkg_free(ctx);
            return 1;
        }
        ret = syspkg_which(ctx, arg[0]);
        goto list;
    } else
    if(arg[0] && (!strcmp(arg[0], "depends") || (arg[0][0] == '-' && arg[0][1] == 'd'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_NOPKGNAME]);
            syspkg_free(ctx);
            return 1;
        }
        ret = syspkg_search(ctx, 0, NULL, arg[0], NULL);
        goto list;
    } else
    if(arg[0] && (!strcmp(arg[0], "search") || (arg[0][0] == FLAG[0] && arg[0][1] == 's') ||
        !strcmp(arg[0], "list") || (arg[0][0] == FLAG[0] && arg[0][1] == 'l'))) {
        i = arg[0][0] == 'l' || arg[0][1] == 'l';
        arg++;
        ret = syspkg_search(ctx, i, arg[0], NULL, NULL);
list:   if(!ret && ctx->packages && ctx->packages[0]) {
            for(i = 0; ctx->packages[i]; i++) {
                /* mess up memory, but that's okay, because we'll quit syspkg after this listing */
                for(c = ctx->packages[i]->desc; c && *c; c++)
                    if(*c == '\n') { *c = 0; break; }
                printf("%s/%s %s%s%s%s%s%s\n    %s - %s\n",
                    ctx->packages[i]->category, ctx->packages[i]->id, ctx->packages[i]->release,
                    ctx->packages[i]->irelease ? " [" : "", ctx->packages[i]->irelease ? lang[INSTALLED] : "",
                    ctx->packages[i]->irelease && ctx->packages[i]->iversion != ctx->packages[i]->version ? " " : "",
                    ctx->packages[i]->irelease && ctx->packages[i]->iversion != ctx->packages[i]->version ?
                    ctx->packages[i]->irelease : "", ctx->packages[i]->irelease ? "]" : "", ctx->packages[i]->name,
                    ctx->packages[i]->desc);
            }
        } else
            printf("%s\n", lang[ERR_NOPKGSPEC]);
    } else
    if(arg[0] && (!strcmp(arg[0], "info") || (arg[0][0] == FLAG[0] && arg[0][1] == 'i'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_NOPKGNAME]);
            syspkg_free(ctx);
            return 1;
        }
        ret = syspkg_search(ctx, 0, arg[0], NULL, NULL);
        if(!ret && ctx->packages && ctx->packages[0]) {
            printf("ID: %s\n%s: %s%s%s%s%s%s\n%s: %d.%d.%d\n",
                ctx->packages[0]->id, lang[RELEASE], ctx->packages[0]->release,
                ctx->packages[0]->irelease ? " [" : "", ctx->packages[0]->irelease ? lang[INSTALLED] : "",
                ctx->packages[0]->irelease && ctx->packages[0]->iversion != ctx->packages[0]->version ? " " : "",
                ctx->packages[0]->irelease && ctx->packages[0]->iversion != ctx->packages[0]->version ?
                ctx->packages[0]->irelease : "", ctx->packages[0]->irelease ? "]" : "", lang[VERSION],
                (ctx->packages[0]->version >> 16)&255, (ctx->packages[0]->version >> 8)&255, ctx->packages[0]->version&255);
            if(ctx->packages[0]->category)
                printf("%s: %s\n", lang[CATEGORY], ctx->packages[0]->category);
            if(ctx->packages[0]->url)
                printf("URL: %s\n", ctx->packages[0]->url);
            if(ctx->packages[0]->homepage)
                printf("%s: %s\n", lang[HOMEPAGE], ctx->packages[0]->homepage);
            if(ctx->packages[0]->bugtracker)
                printf("%s: %s\n", lang[BUGTRACKER], ctx->packages[0]->bugtracker);
            if(ctx->packages[0]->maintainer)
                printf("%s: %s\n", lang[MAINTAINER], ctx->packages[0]->maintainer);
            if(ctx->packages[0]->license)
                printf("%s: %s\n", lang[LICENSE], ctx->packages[0]->license);
            if(ctx->packages[0]->depends && ctx->packages[0]->depends[0].id) {
                printf("%s:", lang[PKGDEPENDS]);
                for(i = 0; ctx->packages[0]->depends[i].id; i++) {
                    if(ctx->packages[0]->depends[i].version > 0)
                        printf("%s %s %d.%d.%d", i ? "," : "", ctx->packages[0]->depends[i].id,
                            (ctx->packages[0]->depends[i].version >> 16)&255, (ctx->packages[0]->depends[i].version >> 8)&255,
                            ctx->packages[0]->depends[i].version&255);
                    else
                        printf("%s %s", i ? "," : "", ctx->packages[0]->depends[i].id);
                }
                printf("\n");
            }
            if(ctx->packages[0]->suggests && ctx->packages[0]->suggests[0].id) {
                printf("%s:", lang[PKGSUGGESTS]);
                for(i = 0; ctx->packages[0]->suggests[i].id; i++) {
                    if(ctx->packages[0]->suggests[i].version > 0)
                        printf("%s %s %d.%d.%d", i ? "," : "", ctx->packages[0]->suggests[i].id,
                            (ctx->packages[0]->suggests[i].version >> 16)&255, (ctx->packages[0]->suggests[i].version >> 8)&255,
                            ctx->packages[0]->suggests[i].version&255);
                    else
                        printf("%s %s", i ? "," : "", ctx->packages[0]->suggests[i].id);
                }
                printf("\n");
            }
            if(ctx->packages[0]->conflicts && ctx->packages[0]->conflicts[0].id) {
                printf("%s:", lang[PKGCONFLICTS]);
                for(i = 0; ctx->packages[0]->conflicts[i].id; i++) {
                    if(ctx->packages[0]->conflicts[i].version > 0)
                        printf("%s %s %d.%d.%d", i ? "," : "", ctx->packages[0]->conflicts[i].id,
                            (ctx->packages[0]->conflicts[i].version >> 16)&255, (ctx->packages[0]->conflicts[i].version >> 8)&255,
                            ctx->packages[0]->conflicts[i].version&255);
                    else
                        printf("%s %s", i ? "," : "", ctx->packages[0]->conflicts[i].id);
                }
                printf("\n");
            }
            printf("\n%s\n%s\n", ctx->packages[0]->name, ctx->packages[0]->desc);
        } else
            printf("%s\n", lang[ERR_NOPKGSPEC]);
    } else

    /*** select packages which have newer versions available ***/
    if(arg[0] && (!strcmp(arg[0], "upgrade") || (arg[0][0] == FLAG[0] && arg[0][1] == 'U'))) {
        if((ret = syspkg_upgrade(ctx))) {
            switch(ret) {
                case EEXIST: fprintf(stderr, "%s\n", lang[ERR_LOCKED]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_UPGRADE]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
        if(ctx->removes || ctx->installs) goto confirm;
        printf("%s\n", lang[UPTODATE]);
    } else

    /*** install or upgrade packages ***/
    if(arg[0] && (!strcmp(arg[0], "install") || (arg[0][0] == FLAG[0] && arg[0][1] == 'I'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_INSTPAR]);
            syspkg_free(ctx);
            return 1;
        }
        if((ret = syspkg_install(ctx, arg, nodeps))) {
            switch(ret) {
                case EEXIST: fprintf(stderr, "%s\n", lang[ERR_LOCKED]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                case ENOENT:
                    if(ctx->conflicts || ctx->missing) goto confirm;
                    fprintf(stderr, "%s\n", lang[ERR_NOPKGSPEC]);
                    break;
                default: fprintf(stderr, "%s\n", lang[ERR_UPDATE]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
        if(ctx->removes || ctx->installs) {
confirm:    if(ctx->conflicts) {
                printf("%s:\n", lang[CONFLICTING]);
                for(i = 0; ctx->conflicts[i]; i += 2)
                    printf("  %s %s %s\n", ctx->conflicts[i], lang[CONFLICTS], ctx->conflicts[i + 1]);
            }
            if(ctx->missing) {
                printf("%s:\n", lang[MISSING]);
                for(i = 0; ctx->missing[i]; i++)
                    printf("  %s", ctx->missing[i]->id);
                printf("\n");
            }
            if(ctx->conflicts || ctx->missing) {
                syspkg_free(ctx);
                return 1;
            }
            if(ctx->removes) {
                printf("%s:\n", lang[TOBEREMOVED]);
                for(i = 0; ctx->removes[i]; i++)
                    printf("  %s", ctx->removes[i]->id);
                printf("\n");
            }
            if(ctx->installs) {
                if(ctx->suggests) {
                    printf("%s:\n", lang[SUGGESTED]);
                    for(i = 0; ctx->suggests[i]; i++)
                        printf("  %s", ctx->suggests[i]->id);
                    printf("\n");
                }
                for(i = in = up = 0; ctx->installs[i]; i++)
                    if(!ctx->installs[i]->iversion) in++; else up++;
                if(in) {
                    printf("%s:\n", lang[TOBEINSTALLED]);
                    for(i = 0; ctx->installs[i]; i++)
                        if(!ctx->installs[i]->iversion)
                            printf("  %s", ctx->installs[i]->id);
                    printf("\n");
                }
                if(up) {
                    printf("%s:\n", lang[TOBEUPGRADED]);
                    for(i = 0; ctx->installs[i]; i++)
                        if(ctx->installs[i]->iversion)
                            printf("  %s", ctx->installs[i]->id);
                    printf("\n");
                }
            }
            printf("%s: ", lang[DOWNLOADSIZE]);
            print_num(ctx->dlsize);
            printf(", %s: ", lang[NEEDEDSPACE]);
            print_num(ctx->total);
            printf("\n");
            if(yes) {
                i = '\r';
                ctx->conf = NULL; /* do not ask for accept terms or env config with force yes */
            } else {
                printf("\n%s? ", lang[PROCEED]);
                i = getchar();
            }
            printf("\n");
            if((i == '\r' || i == '\n') && (ret = syspkg_commit(ctx))) {
                switch(ret) {
                    case EEXIST: fprintf(stderr, "%s\n", lang[ERR_LOCKED]); break;
                    case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                    case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                    case ENOSPC: fprintf(stderr, "%s\n", lang[ERR_NOSPACE]); break;
                    default:
                        fprintf(stderr, "%s\n", lang[ERR_OPER]);
                        for(i = 0; ctx->packages[i]; i++)
                            if(ctx->packages[i]->err < 0)
                                fprintf(stderr, "  %s: %s\n", ctx->packages[i]->id, lang[ctx->packages[i]->err == -1 ?
                                    ERR_DOWNLOAD : (ctx->packages[i]->err == -2 ? ERR_CHKSUM : ERR_PERM)]);
                        break;
                }
                syspkg_free(ctx);
                return 1;
            }
        }
    } else
    /*** reconfigure packages ***/
    if(arg[0] && (!strcmp(arg[0], "reconf") || (arg[0][0] == FLAG[0] && arg[0][1] == 'r'))) {
        arg++;
        if(!arg[0]) {
            fprintf(stderr, "%s\n", lang[ERR_NOPKGNAME]);
            syspkg_free(ctx);
            return 1;
        }
        if((ret = syspkg_reconf(ctx, arg))) {
            switch(ret) {
                case EEXIST: fprintf(stderr, "%s\n", lang[ERR_LOCKED]); break;
                case EACCES: fprintf(stderr, "%s\n", lang[ERR_NOTADMIN]); break;
                case ENOMEM: fprintf(stderr, "%s\n", lang[ERR_NOMEM]); break;
                case ENOENT: fprintf(stderr, "%s\n", lang[ERR_NOTINST]); break;
                default: fprintf(stderr, "%s\n", lang[ERR_CONF]); break;
            }
            syspkg_free(ctx);
            return 1;
        }
    } else
    /*** unknown command ***/
        goto usage;

    syspkg_free(ctx);
    return 0;
}
