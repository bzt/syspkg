/* valid architectures */
#define VALIDARCHS  "intel", "arm"
#ifdef __x86_64__
# define MYARCH "intel"
#else
# ifdef __aarch64__
#  define MYARCH "arm"
# else
#  define MYARCH "any"
# endif
#endif

/* valid categories */
#define VALIDCATS   "education", "games", "graphics", "internet", "office", "programming", "multimedia", "tools"

/* the directory separator */
#define SEP "/"

/* user local data, cert and keys are stored here */
#define USERDIR "%s/Library/Application\ Support/SysPkg/"

/* global configuration file */
#define PKGCFG "/Applications/SysPkg.app/Contents/Resources/syspkg.json"

/* global data files' directory */
#define PKGDIR "/Applications/SysPkg.app/Contents/Resources/Var"

/* where the installed files are stored */
#define BINDIR "/Applications/%s.app/Contents/MacOS/"
#define INCDIR "/Library/Frameworks/%s.framework/Versions/A/Headers/"
#define LIBDIR "/Library/Frameworks/%s.framework/lib/"
#define ETCDIR "/Applications/%s.app/Contents/Resources/etc"
#define SRCDIR "/Applications/%s.app/Contents/Resources/src"
#define DOCDIR "/private/usr/share/man/"
#define SHRDIR "/private/usr/share/%s/"
#define VARDIR "/private/var/lib/%s/"

/* only used for the base package */
#define BASEDIR "/"
