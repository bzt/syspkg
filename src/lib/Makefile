#
#  syspkg/src/lib/Makefile
#
#  Copyright (C) 2021 bzt (bztsrc@gitlab)
#
#  Permission is hereby granted, free of charge, to any person
#  obtaining a copy of this software and associated documentation
#  files (the "Software"), to deal in the Software without
#  restriction, including without limitation the rights to use, copy,
#  modify, merge, publish, distribute, sublicense, and/or sell copies
#  of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be
#  included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#  DEALINGS IN THE SOFTWARE.
#
#  @brief Project makefile
#

TARGET = ../../lib/libsyspkg.a

DECOMPRESSORS = zlib/libz.a zstd/libzstd.a

SRCS = $(wildcard *.c)
OBJS = $(SRCS:.c=.o)

CFLAGS = -std=c99 -pedantic -Wall -Wextra -O3 -I./mbedtls/mbedtls/include -I./zlib -I./zstd -I../../inc \
	-D_FILE_OFFSET_BITS=64 -D__USE_FILE_OFFSET64 -D__USE_LARGEFILE
ifeq ($(DEBUG),1)
CFLAGS += -DDEBUG=1 -g
endif
CC ?= gcc
AR ?= ar

all: $(TARGET)

zlib/Makefile:
	@cd zlib && chmod +x ./configure && ./configure && cd ..

zlib/libz.a: zlib/Makefile
	@make -C zlib libz.a

zstd/libzstd.a:
	@make -C zstd libzstd.a ZSTD_LEGACY_SUPPORT=0 ZSTD_LIB_DICTBUILDER=0 ZSTD_LIB_DEPRECATED=0 \
		ZSTD_LIB_MINIFY=1 ZSTD_STATIC_LINKING_ONLY=1 ZSTD_STRIP_ERROR_STRINGS=1 DEBUGLEVEL=0

mbedtls/mbedtls/library/libmbedtls.a:
	@make -C mbedtls all

%: %.c
	$(CC) $(CFLAGS) $< -c $@

$(TARGET): mbedtls/mbedtls/library/libmbedtls.a $(DECOMPRESSORS) $(OBJS)
	@mkdir ../../lib 2>/dev/null || true
	@rm $@ 2>/dev/null || true
	$(AR) -frsv $@ $(OBJS) mbedtls/mbedtls/library/*.o zlib/*.o zstd/common/*.o zstd/decompress/*.o zstd/compress/*.o >/dev/null

clean:
	@rm $(TARGET) *.o 2>/dev/null || true
	@rmdir ../../lib 2>/dev/null || true

distclean: clean
	@make -C mbedtls distclean || true
	@make -C zlib clean || true
	@make -C zstd clean || true
	@rm zlib/Makefile zlib/*.log zlib/zlib.pc $(DECOMPRESSORS) 2>/dev/null || true

