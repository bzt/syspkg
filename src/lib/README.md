The System Packaging Library
============================

This is the real thing, all the logic and magic are implemented here. This is an ANSI C, dependency-free system package
management library, `libsyspkg`. Minimal dependencies it has are statically compiled in, so only `libc` remains (or some
dll if configured for WIN32 API).
