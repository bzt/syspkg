/*
 * lib/html.c
 *
 * Copyright (C) 2018 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Searchable package list HTML generator
 *
 */
#include "internal.h"

#if HTMLCATALOG
#include "html.h"

/**
 * Translate a phrase if it's given in the dictionary
 */
static char *syspkg_translate(char **dict, char *key)
{
    int i;
    int l = strlen(key);
    if(!dict) return key;
    for(i = 0; dict && dict[i]; i++)
        if(!memcmp(dict[i], key, l) && dict[i][l] == '=') return dict[i] + l + 1;
    return key;
}

/**
 * Generate a html package catalog for the repository
 */
void syspkg_html(syspkg_ctx_t *ctx, char *repourl, char *pkgurls, char *htmlfile, char **dict)
{
    syspkg_file_t *f;
    syspkg_meta_t *meta;
    int i, j, k, r, npkg, ncat = 0, a, b;
    char *buf = NULL, *frm, *dst = NULL, *dst2 = NULL, *tmpl, *tmpl2, *tmpl3, cn[MAXCNSIZE+32], rcn[MAXCNSIZE+32], **cats = NULL;
    char *realurl, *title, tmp[MAXPATHSIZE], ver[MAXVERSIZE], *cu, *uu;
    size_t n = 0, l, cs, us;
    char *c, *e, *jsonstr;
    mbedtls_x509_crt crt;
    mbedtls_x509_buf crl;
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    for(npkg = 0, c = pkgurls; *c && memcmp(c, PEM_BEG_CRT, 27); c++)
        if(*c == '\n') npkg++;
    if(c > pkgurls && *(c - 1) != '\n') npkg++;
    if(c == pkgurls || (!*c || memcmp(c, PEM_BEG_CRT, 27))) return;
    if(!(f = syspkg_open(htmlfile, 1))) return;

    tmpl = (char*)malloc(32768);
    if(!tmpl) return;
    memset(tmpl, 0, 32768);
    zcmp = ZSTD_createDCtx();
    if(!zcmp) { free(tmpl); return; }
    zi.src = binary_template_html_zst; zi.size = sizeof(binary_template_html_zst); zi.pos = 0;
    zo.dst = tmpl; zo.size = 32768; zo.pos = 0;
    ZSTD_decompressStream(zcmp, &zo, &zi);
    ZSTD_freeDCtx(zcmp);
    tmpl[zo.pos] = 0;
    for(e = tmpl; *e != '$'; e++);
    *e++ = 0; tmpl2 = e;
    for(e = tmpl; *e != '$'; e++);
    *e++ = 0; tmpl3 = e;
    mbedtls_x509_crt_init( &crt );
    crl.p = NULL; crl.len = 0;
    memset(cn, 0, sizeof(cn));
    memset(rcn, 0, sizeof(rcn));
    if(mbedtls_x509_crt_parse(&crt, (unsigned char *)c, strlen(c) + 1) != 0 || !crt.ca_istrue ||
        !(crt.key_usage & MBEDTLS_X509_KU_KEY_CERT_SIGN) ||
        mbedtls_x509_dn_gets((char*)&rcn, sizeof(rcn), &crt.subject) < 3) goto err;
    for(e = rcn; *e && *e != ',' && *e != '<'; e++);
    *e = 0;
    e = strstr(c, PEM_BEG_CRL);
    if(e) {
        e += 20;
        l = strlen(e);
        crl.p = (unsigned char *)malloc(l);
        if(!crl.p || mbedtls_base64_decode(crl.p, l, &crl.len, (unsigned char*)e, l + 1 - 18) != 0) goto err;
    }
    *c = 0;
    for(j = 0, c = pkgurls; *c; ) {
        if(ctx->progressbar) (*ctx->progressbar)(0, 0, j++, npkg + 1, SYSPKG_MSG_GENHTML);
        for(e = c; *e && *e != '\r' && *e != '\n'; e++);
        if(c == e) break;
        *e++ = 0;
        if(!syspkg_readfileall(c, (unsigned char **)&jsonstr, &l) && jsonstr && l) {
            if((!syspkg_chksig(&crt, &crl, jsonstr, (char*)&cn)) && cn[0] &&
                (meta = syspkg_readmeta(ctx, jsonstr, SYSPKG_FILTER_NONE, &r))) {
                k = 0;
                if(cats && ncat) {
                    l = 17;
                    b = ncat - 1; a = 0;
                    while(l--) {
                        k = a + ((b-a) >> 1);
                        r = strcmp(cats[k], meta->d.category);
                        if(!r) {
                            k = -1;
                            break;
                        }
                        if(a >= b) break;
                        if(r < 0) a = k + 1; else b = k;
                    }
                    if(r < 0) k++;
                }
                if(k != -1) {
                    cats = (char**)realloc(cats, (ncat + 1) * sizeof(char*));
                    if(!cats) { free(jsonstr); syspkg_freemeta(meta); goto err; }
                    if(k < ncat)
                        memmove(&cats[k+1], &cats[k], (ncat - k) * sizeof(char*));
                    cats[k] = (char*)malloc(strlen(meta->d.category) + 1);
                    if(!cats[k]) { free(jsonstr); syspkg_freemeta(meta); goto err; }
                    strcpy(cats[k], meta->d.category);
                    ncat++;
                }
                syspkg_sprintf(&buf, &dst2, &n, "<div class=\"row\"><div class=\"mini\">");
                if(meta->screenshots[0])
                    syspkg_sprintf(&buf, &dst2, &n, "<img src=\"%q\" alt=\"\" onclick=\"togglediv(this);\">",
                        meta->screenshots[0]);
                syspkg_sprintf(&buf, &dst2, &n, "<div class=\"card-text\" onclick=\"togglediv(this);\" id=\"%q\">%q</div>"
                    "<span class=\"desc\"><h3>%q</h3><small onclick=\"dosearch(this);\">%q</small><span class=\"small\">"
                    "/%q %q</span><p>%q</p></span></div>", meta->d.id, meta->d.name, meta->d.name,
                    meta->d.category, meta->d.id, meta->d.release, meta->d.desc);
                if(meta->payloads && meta->payloads[0].arch) {
                    syspkg_sprintf(&buf, &dst2, &n, "<table class=\"table table-sm table-striped table-hover dl\">"
                        "<tr><th>%q</th><th>%q</th><th>%q</th></tr>", syspkg_translate(dict, "Package Size"),
                        syspkg_translate(dict, "Installed Size"), syspkg_translate(dict, "Download"));
                    sprintf(ver, "%d.%d.%d", (meta->d.version >> 16)&255, (meta->d.version >> 8)&255, meta->d.version&255);
                    for(i = 0; meta->payloads[i].arch; i++) {
                        syspkg_url(meta->d.url, meta->payloads[i].arch, ver, meta->d.release, ctx->osver, (char*)&tmp);
                        cs = meta->payloads[i].comp; cu = "b";
                        if(cs > 1024) {
                            cs /= 1024; cu = "Kb";
                            if(cs > 1024) { cs /= 1024; cu = "Mb";
                                if(cs > 1024) { cs /= 1024; cu = "Gb"; } } }
                        us = meta->payloads[i].orig; uu = "b";
                        if(us > 1024) {
                            us /= 1024; uu = "Kb";
                            if(us > 1024) { us /= 1024; uu = "Mb";
                                if(us > 1024) { us /= 1024; uu = "Gb"; } } }
                        syspkg_sprintf(&buf, &dst2, &n, "<tr title=\"SHA256 %h\"><td>%d %s</td><td>%d %s</td><td><a href=\"",
                            &meta->payloads[i].hash, cs, cu, us, uu);
                        syspkg_sprintf(&buf, &dst2, &n, "%s", tmp);
                        syspkg_sprintf(&buf, &dst2, &n, "\" class=\"btn btn-primary\">%q</a></td></tr>", meta->payloads[i].arch);
                    }
                    syspkg_sprintf(&buf, &dst2, &n, "</table>");
                }
                syspkg_sprintf(&buf, &dst2, &n, "<table class=\"table table-sm table-striped det\">"
                    "<tr><th>Metajson:</th><td><a href=\"%q\">%q</a></td></tr>", c, c);
                if(meta->d.homepage && meta->d.homepage[0])
                    syspkg_sprintf(&buf, &dst2, &n, "<tr><th>%q:</th><td><a href=\"%q\">%q</a></td></tr>",
                        syspkg_translate(dict, "Homepage"), meta->d.homepage, meta->d.homepage);
                if(meta->d.bugtracker && meta->d.bugtracker[0])
                    syspkg_sprintf(&buf, &dst2, &n, "<tr><th>%q:</th><td><a href=\"%q\">%q</a></td></tr>",
                        syspkg_translate(dict, "Bugtracker"), meta->d.bugtracker, meta->d.bugtracker);
                syspkg_sprintf(&buf, &dst2, &n, "<tr><th>%q:</th><td>%q</td></tr><tr><th>%q</th><td>%q</td></tr>",
                    syspkg_translate(dict, "Maintainer"), cn, syspkg_translate(dict, "License"), meta->d.license);
                if(meta->d.depends && meta->d.depends[0].id) {
                    syspkg_sprintf(&buf, &dst2, &n, "<tr><th>%q:</th><td>", syspkg_translate(dict, "Depends"));
                    for(i = 0; meta->d.depends[i].id; i++) {
                        syspkg_sprintf(&buf, &dst2, &n, "%s<a onclick=\"togglediv(document.getElementById(\'%q\'));\">%q</a>",
                            i ? ", " : "", meta->d.depends[i].id, meta->d.depends[i].id);
                        if(meta->d.depends[i].version > 0)
                            syspkg_sprintf(&buf, &dst2, &n, " <span class=\"small\">%d.%d.%d</span>",
                                (meta->d.depends[i].version >> 16)&255, (meta->d.depends[i].version >> 8)&255,
                                meta->d.depends[i].version&255);
                    }
                    syspkg_sprintf(&buf, &dst2, &n, "</td></tr>");
                }
                if(meta->d.suggests && meta->d.suggests[0].id) {
                    syspkg_sprintf(&buf, &dst2, &n, "<tr><th>%q:</th><td>", "Suggests");
                    for(i = 0; meta->d.suggests[i].id; i++) {
                        syspkg_sprintf(&buf, &dst2, &n, "%s<a onclick=\"togglediv(document.getElementById(\'%q\'));\">%q</a>",
                            i ? ", " : "", meta->d.suggests[i].id, meta->d.suggests[i].id);
                        if(meta->d.suggests[i].version > 0)
                            syspkg_sprintf(&buf, &dst2, &n, " <span class=\"small\">%d.%d.%d</span>",
                                (meta->d.suggests[i].version >> 16)&255, (meta->d.suggests[i].version >> 8)&255,
                                meta->d.suggests[i].version&255);
                    }
                    syspkg_sprintf(&buf, &dst2, &n, "</td></tr>");
                }
                if(meta->d.conflicts && meta->d.conflicts[0].id) {
                    syspkg_sprintf(&buf, &dst2, &n, "<tr><th>%q:</th><td>", "Conflicts");
                    for(i = 0; meta->d.conflicts[i].id; i++) {
                        syspkg_sprintf(&buf, &dst2, &n, "%s<a onclick=\"togglediv(document.getElementById(\'%q\'));\">%q</a>",
                            i ? ", " : "", meta->d.conflicts[i].id, meta->d.conflicts[i].id);
                        if(meta->d.conflicts[i].version > 0)
                            syspkg_sprintf(&buf, &dst2, &n, " <span class=\"small\">%d.%d.%d</span>",
                                (meta->d.conflicts[i].version >> 16)&255, (meta->d.conflicts[i].version >> 8)&255,
                                meta->d.conflicts[i].version&255);
                    }
                    syspkg_sprintf(&buf, &dst2, &n, "</td></tr>");
                }
                syspkg_sprintf(&buf, &dst2, &n, "</table></div>");
                syspkg_freemeta(meta);
            }
            free(jsonstr);
        }
        for(c = e; *c == '\r' || *c == '\n'; c++);
    }
    c = "url"; realurl = syspkg_translate(dict, c);
    if(realurl == c) realurl = repourl;
    c = "title"; title = syspkg_translate(dict, "title");
    if(title == c) title = rcn + 3;
    n = FBUFSIZE; frm = dst = (char*)f->buf;
    syspkg_sprintf(&frm, &dst, &n, tmpl, title);
    for(i = 0; i < ncat; i++)
        syspkg_sprintf(&frm, &dst, &n, "#tab%d:checked ~ ul li[rel=\"tab%d\"],", i, i);
    syspkg_sprintf(&frm, &dst, &n, tmpl2, realurl, title, realurl, syspkg_translate(dict, "Repository"),
        syspkg_translate(dict, "Search"));
    for(i = 0; i < ncat; i++)
        syspkg_sprintf(&frm, &dst, &n,
            "<input type=\"radio\" name=\"tab\" id=\"tab%d\" onchange=\"dosearch(this);\" value=\"%q\"%s>", i, cats[i],
            i ? "" : " checked");
    syspkg_sprintf(&frm, &dst, &n, "<ul class=\"nav nav-tabs\">");
    for(i = 0; i < ncat; i++)
        syspkg_sprintf(&frm, &dst, &n,
            "<li class=\"nav-link\" rel=\"tab%d\"><label for=\"tab%d\" class=\"text-capitalize\">%q</label></li>", i, i,
            syspkg_translate(dict, cats[i]));
    syspkg_sprintf(&frm, &dst, &n, "</ul>\n</div>\n</div>\n</div>\n<div id=\"results\">");
    f->ilen = dst - frm; f->olen = 0; syspkg_write(f);
    frm = (char*)f->buf;
    if(buf) {
        f->buf = (unsigned char*)buf;
        f->ilen = dst2 - buf; f->olen = 0; syspkg_write(f);
    }
    f->buf = (unsigned char*)tmpl3;
    f->ilen = strlen(tmpl3); f->olen = 0; syspkg_write(f);
    f->buf = (unsigned char*)frm;
err:
    syspkg_close(f);
    mbedtls_x509_crt_free(&crt);
    if(crl.p) free(crl.p);
    crl.p = NULL; crl.len = 0;
    if(tmpl) free(tmpl);
    if(buf) free(buf);
    if(cats) {
        for(i = 0; i < ncat; i++)
            if(cats[i]) free(cats[i]);
        free(cats);
    }
    if(ctx->progressbar) (*ctx->progressbar)(0, 0, 1, 1, SYSPKG_MSG_GENHTML);
}

#endif
