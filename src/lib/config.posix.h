/* valid architectures */
#define VALIDARCHS  "x86_64", "aarch64", "riscv64"
#ifdef __x86_64__
# define MYARCH "x86_64"
#else
# ifdef __aarch64__
#  define MYARCH "aarch64"
# else
#  ifdef __riscv64__
#   define MYARCH "riscv64"
#  else
#   define MYARCH "any"
#  endif
# endif
#endif

/* valid categories */
#define VALIDCATS   "education", "games", "graphics", "internet", "office", "programming", "multimedia", "tools"

/* the directory separator */
#define SEP "/"

/* user local data, cert and keys are stored here */
#define USERDIR "%s/.config/syspkg/"

/* global configuration file */
#define PKGCFG "/etc/syspkg.json"

/* global data files' directory */
#define PKGDIR "/var/lib/syspkg/"

/* where the installed files are stored */
#define BINDIR "/usr/bin/"
#define INCDIR "/usr/include/%s/"
#define LIBDIR "/lib/%s/"
#define ETCDIR "/etc/%s/"
#define SRCDIR "/usr/src/%s/"
#define DOCDIR "/usr/share/man/"
#define SHRDIR "/usr/share/%s/"
#define VARDIR "/var/lib/%s/"

/* only used for the base package */
#define BASEDIR "/"

/* directories in payloads (optional, can also be specified on per package basis in metajson) */
#define BINPDR "bin"
#define INCPDR "include"
#define LIBPDR "lib"
#define ETCPDR "etc"
#define SRCPDR "src"
#define DOCPDR "docs"
#define SHRPDR "share"
#define VARPDR "var"
