/*
 * syspkg/src/lib/postinst.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Package postinst configuration functions
 *
 */

#include "internal.h"
#ifdef __WIN32__
#include <windows.h>
#else
#include <sys/wait.h>
#endif

/* from libc */
extern char **environ;

/**
 * Get environment variables for post installation configuration
 */
int syspkg_postenv(syspkg_ctx_t *ctx, syspkg_meta_t *meta)
{
    syspkg_input_t *input = NULL;
    size_t len;
    char *terms = NULL, *c, *d;
    int i, l, n = 0, ret = SUCCESS;

    if(!ctx || !meta) return EINVAL;
    if((!meta->payloads || !meta->payloads[0].comp || !meta->eula || !*meta->eula) &&
        (!meta->commands || !meta->commands[0] || !*meta->commands[0])) return SUCCESS;
    syspkg_readfileall(meta->eula, (unsigned char **)&terms, &len);

    /* generate user configuration form */
    if(meta->env && meta->commands && meta->commands[0] && *meta->commands[0]) {
        for(n = 0; meta->env[n].name && meta->env[n].type; n++);
        if(n > 0) {
            input = (syspkg_input_t*)malloc(n * sizeof(syspkg_input_t));
            if(!input) { if(terms) { free(terms); } return ENOMEM; }
            memset(input, 0, n * sizeof(syspkg_input_t));
            for(i = 0; i < n; i++) {
                if(meta->env[i].desc && meta->env[i].desc[0].name) {
                    input[i].name = meta->env[i].desc[0].name;
                    input[i].desc = meta->env[i].desc[0].desc;
                } else {
                    input[i].name = meta->env[i].name;
                }
                l = strlen(meta->env[i].type);
                if(!memcmp(meta->env[i].type, "str", 3)) {
                    input[i].type = SYSPKG_FORM_STR;
                    if(meta->env[i].type[3] == '(') {
                        input[i].spec.str = (char*)malloc(l);
                        if(input[i].spec.str) {
                            memcpy(input[i].spec.str, meta->env[i].type + 4, l - 5);
                            input[i].spec.str[l - 5] = 0;
                        }
                    }
                } else
                if(!memcmp(meta->env[i].type, "num", 3)) {
                    input[i].type = SYSPKG_FORM_NUM;
                    if(meta->env[i].type[3] == '(') {
                        c = meta->env[i].type + 4;
                        input[i].spec.num.min = atoi(c);
                        for(; *c && *c != ',' && *c != ')'; c++);
                        if(*c == ',') {
                            input[i].spec.num.min = atoi(++c);
                            for(; *c && *c != ',' && *c != ')'; c++);
                            if(*c == ',')
                                input[i].spec.num.def = atoi(++c);
                        }
                    }
                } else
                if(!memcmp(meta->env[i].type, "chk", 3)) {
                    input[i].type = SYSPKG_FORM_CHK;
                    if(meta->env[i].type[3] == '(') {
                        input[i].spec.chk.disabled = (char*)malloc(l);
                        input[i].spec.chk.enabled = (char*)malloc(l);
                        if(input[i].spec.chk.disabled && input[i].spec.chk.enabled) {
                            d = input[i].spec.chk.disabled;
                            for(c = meta->env[i].type + 4; *c && *c != ',' && *c != ')'; c++) {
                                if(*c == '\\') c++;
                                *d++ = *c;
                            }
                            *d = 0;
                            d = input[i].spec.chk.enabled;
                            if(*c == ',') {
                                for(c++; *c && *c != ')'; c++) {
                                    if(*c == '\\') c++;
                                    *d++ = *c;
                                }
                            }
                            *d = 0;
                        }
                    }
                } else
                if(!memcmp(meta->env[i].type, "sel", 3)) {
                    input[i].type = SYSPKG_FORM_SEL;
                    if(meta->env[i].type[3] == '(')
                        for(c = meta->env[i].type + 3; *c && *c != ')'; c++) {
                            if(*c == '(' || *c == ',') {
                                input[i].spec.sel.opts = (char**)realloc(input[i].spec.sel.opts,
                                    (input[i].spec.sel.len + 1) * sizeof(char*));
                                if(!input[i].spec.sel.opts) break;
                                for(d = c; *d && *d != ',' && *d !=')'; d++);
                                input[i].spec.sel.opts[input[i].spec.sel.len] = (char*)malloc(d - c + 1);
                                if(input[i].spec.sel.opts[input[i].spec.sel.len]) {
                                    d = input[i].spec.sel.opts[input[i].spec.sel.len++];
                                    for(; *c && *c != ',' && *c !=')'; c++) {
                                        if(*c == '\\') c++;
                                        *d++ = *c;
                                    }
                                    *d = 0;
                                }
                            }
                        }
                }
            }
        }
    }
    /* call configuration form callback */
    if(ctx->conf && !((*ctx->conf)(meta->d.id, meta->d.name, terms, n, input)))
        ret = ENOENT;
    else {
        /* save answers */
        for(i = 0; i < n; i++) {
            if(meta->env[i].envp) { free(meta->env[i].envp); meta->env[i].envp = NULL; }
            d = input[i].value;
            if(!d || !*d)
                switch(input[i].type) {
                    case SYSPKG_FORM_STR: d = input[i].spec.str; break;
                    case SYSPKG_FORM_NUM:
                        d = input[i].value = (char*)malloc(32);
                        if(d) sprintf(d, "%d", input[i].spec.num.def);
                        break;
                    case SYSPKG_FORM_CHK: d = input[i].spec.chk.disabled; break;
                    case SYSPKG_FORM_SEL: d = input[i].spec.sel.opts ? input[i].spec.sel.opts[0] : NULL; break;
                }
            if(!d) continue;
            meta->env[i].envp = (char*)malloc(strlen(meta->env[i].name) + strlen(d) + 2);
            if(meta->env[i].envp)
                sprintf(meta->env[i].envp, "%s=%s", meta->env[i].name, d);
        }
    }
    /* free resources */
    if(terms) free(terms);
    if(input) {
        for(i = 0; i < n; i++) {
            switch(input[i].type) {
                case SYSPKG_FORM_STR:
                    if(input[i].value) free(input[i].value);
                    if(input[i].spec.str) free(input[i].spec.str);
                    break;
                case SYSPKG_FORM_NUM:
                    if(input[i].value) free(input[i].value);
                    break;
                case SYSPKG_FORM_CHK:
                    if(input[i].spec.chk.enabled) free(input[i].spec.chk.enabled);
                    if(input[i].spec.chk.disabled) free(input[i].spec.chk.disabled);
                    break;
                case SYSPKG_FORM_SEL:
                    for(l = 0; l < input[i].spec.sel.len; l++)
                        if(input[i].spec.sel.opts[l]) free(input[i].spec.sel.opts[l]);
                    free(input[i].spec.sel.opts);
                    break;
            }
        }
        free(input);
    }
    return ret;
}

/**
 * Configure a package by running postinst commands
 */
int syspkg_postinst(syspkg_meta_t *meta)
{
    char **envp = NULL, *c;
    int i, j, n = 0, envc = 0, ret = SUCCESS;
#ifdef __WIN32__
    char chEnv[65536];
    char *lpstr;
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
#else
    char *argv[] = { "sh", "-c", NULL, NULL };
    pid_t pid;
#endif

    if(!meta) return EINVAL;
    if(!meta->commands || !meta->commands[0] || !*meta->commands[0]) return SUCCESS;

    /* copy inherited environment variables */
    for(i = 0; environ[i]; i++);
    if(meta->env)
        for(n = 0; meta->env[n].name && meta->env[n].type; n++);
    envp = (char**)malloc((i + n + OVERRIDELEN + 1) * sizeof(char *));
    if(!envp) return ENOMEM;
    memset(envp, 0, (i + n + OVERRIDELEN + 1) * sizeof(char *));
    for(i = envc = 0; environ[i]; i++) {
        envp[envc] = (char*)malloc(strlen(environ[i]) + 1);
        if(envp[envc]) strcpy(envp[envc++], environ[i]);
    }
    /* add answers to environment variables */
    for(i = 0; i < n; i++) {
        envp[envc] = (char*)malloc(strlen(meta->env[i].envp) + 1);
        if(envp[envc])
            strcpy(envp[envc++], meta->env[i].envp);
    }
    /* add installation directories to environment variables */
    if(!meta->base) {
        for(i = 0; i < OVERRIDELEN; i++) {
            envp[envc] = (char*)malloc(strlen(syspkg_envnames[i]) + strlen(syspkg_resolves[i]) + strlen(meta->d.id) + 2);
            if(envp[envc]) {
                j = sprintf(envp[envc], "%s=", syspkg_envnames[i]);
                sprintf(envp[envc] + strlen(syspkg_envnames[i]) + 1, syspkg_resolves[i], meta->d.id);
                /* remove the trailing directory separator */
                envp[envc][strlen(envp[envc]) - 1] = 0;
                while(!syspkg_isdir(envp[envc] + j)) {
                    c = strrchr(envp[envc] + j, SEP[0]);
                    if(!c) break;
                    *c = 0;
                }
                envc++;
            }
        }
    } else {
        envp[envc] = (char*)malloc(strlen(BASEDIR) + 9);
        if(envp[envc])
            strcpy(envp[envc++], "BASEDIR=" BASEDIR);
    }
    envp[envc] = NULL;
#ifdef __WIN32__
    for(i = 0, lpstr = chEnv; i < envc && lpstr + 1 < chEnv + sizeof(chEnv); i++) {
        strncpy(lpstr, envp[i], 256);
        lpstr += strlen(lpstr) + 1;
    }
    *lpstr = 0;
#endif
    /* execute commands */
    for(i = 0; meta->commands[i]; i++) {
#ifdef __WIN32__
        memset(&si, 0, sizeof(STARTUPINFO));
        si.cb = sizeof(STARTUPINFO);
        if(CreateProcess("cmd.exe", meta->commands[i], NULL, NULL, 1, 0, (void*)chEnv, NULL, &si, &pi))
            WaitForSingleObject(pi.hProcess, INFINITE);
#else
        argv[2] = meta->commands[i];
        pid = fork();
        if(!pid)
            execve("/bin/sh", argv, envp);
        waitpid(pid, &n, 0);
#endif
    }
    /* free resources */
    for(i = 0; i < envc; i++)
        free(envp[i]);
    free(envp);
    return ret;
}
