/*
 * syspkg/src/lib/internal.h
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Internal API and declarations
 *
 */

#include <mbedtls/config.h>
#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/x509_crt.h>
#include <mbedtls/base64.h>
#include <mbedtls/net_sockets.h>
#include <mbedtls/ssl.h>
#include <ctype.h>
#include <time.h>
#include <zlib.h>
#include <zstd.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <syspkg.h>

/*** user configurable options ***/
#include "config.h"             /* configuration to describe the OS' file system layout */
#define ATTACHMENTS         1   /* set this if you want syspkg to manage package screenshots too */
#define HTMLCATALOG         1   /* set this if you want syspkg to generate html package catalogs */
#define FBUFSIZE      1048576   /* file buffer size, must be MAXPNGSIZE at least */
#define MAXPNGSIZE     131072   /* maximum file size of screenshots */
#define MAXPATHSIZE      4084   /* path length max, must be less than 65524 */
#define MAXBCSIZE 64*1024*1024  /* maximum bytecode file size */
#define MAXDEPLEVEL        16   /* dependency check max re-entrancy level */
/* note without the terminating zero, these must match docs/meta_schema.json */
#define MAXIDSIZE          63   /* package unix name max length */
#define MAXLICSIZE         15   /* license max length, like MIT, GPL, BSD-3, PD */
#define MAXVERSIZE         15   /* version max length, major.feature.bugfix */
#define MAXRELSIZE         31   /* release max length, free form version, like 1.0-rc */
#define MAXURLSIZE        255   /* url max length */
#define MAXCATSIZE         15   /* category id max length, like tools, games */
#define MAXDSCSIZE        511   /* description max length */
#define MAXARCSIZE         15   /* architecture max length, like x86_64 */
#define MAXCNSIZE         127   /* canonical name max length */
#define MAXENVSIZE         15   /* maximum environment variable name size on forms */
#define MAXTYPSIZE        255   /* maximum environment variable type size on forms */
#define MAXLBLSIZE         31   /* maximum label size on environment forms */
#define MAXHNTSIZE        255   /* maximum label hint size on environment forms */
/*** end of user configurable options ***/

/* check the configuration */
#ifndef VALIDARCHS
#error "VALIDARCHS not defined. Should list valid architectures"
#endif
#ifndef VALIDCATS
#error "VALIDCATS not defined. Should list valid category names"
#endif
#ifndef PKGCFG
#error "PKGCFG not defined. Path and filename of the main configuration file"
#endif
#ifndef PKGDIR
#error "PKGDIR not defined. Path of global data"
#endif
#ifndef USERDIR
#error "USERDIR not defined. Path where user local data stored"
#endif
#ifndef BINDIR
#error "BINDIR not defined. Executable binaries and scripts"
#endif
#ifndef INCDIR
#error "INCDIR not defined. Include files"
#endif
#ifndef LIBDIR
#error "LIBDIR not defined. Dynamically linked shared objects"
#endif
#ifndef ETCDIR
#error "ETCDIR not defined. Configuration files"
#endif
#ifndef SRCDIR
#error "SRCDIR not defined. Source files"
#endif
#ifndef DOCDIR
#error "DOCDIR not defined. Manual pages and other help files"
#endif
#ifndef SHRDIR
#error "SHRDIR not defined. Shared files"
#endif
#ifndef VARDIR
#error "VARDIR not defined. Variable data files"
#endif
#ifndef BASEDIR
#error "BASEDIR not defined. Should be the root directory for the 'base' package"
#endif
#ifndef SEP
#define SEP "/"
#endif
#ifndef BINPDR
#define BINPDR "bin"
#endif
#ifndef INCPDR
#define INCPDR "inc"
#endif
#ifndef LIBPDR
#define LIBPDR "lib"
#endif
#ifndef ETCPDR
#define ETCPDR "etc"
#endif
#ifndef SRCPDR
#define SRCPDR "src"
#endif
#ifndef SHRPDR
#define SHRPDR "shr"
#endif
#ifndef DOCPDR
#define DOCPDR "man"
#endif
#ifndef VARPDR
#define VARPDR "var"
#endif

#define OVERRIDELEN         8   /* do not change these */
#define PACKED __attribute__((packed))
#define PEM_BEG_SIG "------BEGIN SIGNATURE------"
#define PEM_END_SIG "------END SIGNATURE------"
#define PEM_BEG_CRT "-----BEGIN CERTIFICATE-----"
#define PEM_END_CRT "-----END CERTIFICATE-----"
#define PEM_BEG_CRL "-----BEGIN CRL-----"
#define PEM_END_CRL "-----END CRL-----"

#ifndef toupper
#define toupper(a) ((a) >= 'a' && (a) <= 'z' ? (a) - ('a' - 'A') : (a))
#endif

#ifndef isdigit
#define isdigit(a) ((a) >= '0' && (a) <= '9')
#endif

/*** payload format, zip and zip64 ***/

#define ZIP_DATA_MADEBY     0x31e
#define ZIP_DATA_STORE      0
#define ZIP_DATA_DEFLATE    8
#define ZIP_DATA_ZSTD       93

#define ZIP_LOCAL_MAGIC     0x04034b50
typedef struct {
    uint32_t    magic;
    uint16_t    version;
    uint16_t    flags;
    uint16_t    method;
    uint32_t    mtime;
    uint32_t    crc32;
    uint32_t    comp;
    uint32_t    orig;
    uint16_t    fnlen;
    uint16_t    extlen;
} PACKED zip_local_t;

#define ZIP_CENTRAL_MAGIC   0x02014b50
typedef struct {
    uint32_t    magic;
    uint16_t    madeby;
    uint16_t    version;
    uint16_t    flags;
    uint16_t    method;
    uint32_t    mtime;
    uint32_t    crc32;
    uint32_t    comp;
    uint32_t    orig;
    uint16_t    fnlen;
    uint16_t    extlen;
    uint16_t    comlen;
    uint16_t    disk;
    uint16_t    iattr;
    uint32_t    eattr;
    uint32_t    offs;
} PACKED zip_central_t;

typedef struct {
    char        *fn;
    uint64_t    comp;
    uint64_t    orig;
    uint64_t    offs;
    uint32_t    mtime;
    uint32_t    crc32;
    uint32_t    eattr;
    uint16_t    version;
    uint16_t    method;
    uint16_t    fnlen;
    uint16_t    extlen;
    uint16_t    iattr;
} zip_inmem_t;

#define ZIP_EXTZIP64_MAGIC  0x0001
typedef struct {
    uint16_t    magic;
    uint16_t    size;
    uint64_t    orig;
    uint64_t    comp;
    uint64_t    offs;
    uint32_t    disk;
} PACKED zip_ext64_t;

#define ZIP_EOCD32_MAGIC    0x06054b50
typedef struct {
    uint32_t    magic;
    uint16_t    numdisk;
    uint16_t    strtdisk;
    uint16_t    totdisk;
    uint16_t    nument;
    uint32_t    cdsize;
    uint32_t    totoffs;
    uint16_t    comlen;
} PACKED zip_eocd32_t;

#define ZIP_EOCD64_MAGIC    0x06064b50
typedef struct {
    uint32_t    magic;
    uint64_t    eocdsize;
    uint16_t    madeby;
    uint16_t    version;
    uint32_t    numdisk;
    uint32_t    strtdisk;
    uint64_t    totdisk;
    uint64_t    nument;
    uint64_t    cdsize;
    uint64_t    totoffs;
} PACKED zip_eocd64_t;

#define ZIP_EOCDLOC_MAGIC   0x07064b50
typedef struct {
    uint32_t    magic;
    uint32_t    strteocd;
    uint64_t    reloffs;
    uint32_t    totdisk;
} PACKED zip_eocdloc_t;

#define ZIP_SIGNATURE_MAGIC 0x05054b50
typedef struct {
    uint32_t    magic;
    uint16_t    size;
} PACKED zip_sign_t;

#define ZIP_EXTDATA_MAGIC   0x08064b50
typedef struct {
    uint32_t    magic;
    uint32_t    size;
} PACKED zip_extdata_t;

/*** internal data structures ***/

extern const char *syspkg_validarchs[];
extern const char *syspkg_validcats[];
extern const char *syspkg_overrides[];
extern const char *syspkg_ploaddirs[];
extern const char *syspkg_resolves[];
extern const char *syspkg_envnames[];
extern const int syspkg_lenpldirs[];

typedef uint16_t pkgidx_t;

typedef struct {
    FILE *f;
#ifdef MBEDTLS_NET_C
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config conf;
    mbedtls_net_context server_fd;
    mbedtls_ctr_drbg_context ctr_drbg;
#endif
    size_t filesize;
    size_t pos;
    size_t ilen;
    size_t olen;
    unsigned char *buf;
    char *full;
} syspkg_file_t;

typedef struct {
    char *lang;
    char *name;
    char *desc;
} syspkg_desc_t;

typedef struct {
    char *name;
    char *type;
    char *envp;
    syspkg_desc_t *desc;
} syspkg_env_t;

typedef struct {
    char *arch;
    size_t comp;
    size_t orig;
    uint8_t hash[32];
} syspkg_payload_t;

enum {
    SYSPKG_ERR_ID,
    SYSPKG_ERR_DESC,
    SYSPKG_ERR_VER,
    SYSPKG_ERR_URL,
    SYSPKG_ERR_CAT,
    SYSPKG_ERR_PAYLOAD,
    SYSPKG_ERR_SIGNATURE
};
enum {
    SYSPKG_FILTER_NONE,
    SYSPKG_FILTER_STD,
    SYSPKG_FILTER_NOPAYLOAD
};

typedef struct {
    /* make sure that syspkg_package_t is the first field! */
    syspkg_package_t d;
    /* technical fields */
    syspkg_desc_t *desc;
    char *eula;
    char **screenshots;
    char *over[OVERRIDELEN];
    syspkg_payload_t *payloads;
    char **files;
    char *metaurl;
    syspkg_env_t *env;
    char **commands;
    unsigned char ovrl[OVERRIDELEN], base;
    int numfiles, repo, idx;
} syspkg_meta_t;
extern char c_assert1[offsetof(syspkg_meta_t, d) == 0 ? 1 : -1];

typedef struct {
    syspkg_meta_t *meta;
    mbedtls_sha256_context sha;
    zip_inmem_t *central;
    syspkg_file_t *f;
    uint64_t comp, orig, pos;
    syspkg_progressbar_t progressbar;
    int pl, numpl, idx;
} syspkg_build_t;

/*** prototypes ***/

#define SYSPKG_WALK_TYPE_REG    0
#define SYSPKG_WALK_TYPE_LNK    1
#define SYSPKG_WALK_TYPE_DIR    2
typedef void (*walk_cb)(int type, int64_t size, uint64_t mtime, char *full, char *name, void *data);

/* syspkg.c */
int mbedtls_hardware_poll( void *data, unsigned char *output, size_t len, size_t *olen );
int syspkg_savecfg(syspkg_ctx_t *ctx);

/* files.c */
char *syspkg_getlang();
char *syspkg_gethome();
int syspkg_isadmin();
int syspkg_lock(int acquire);
int syspkg_unlock();
char *syspkg_realpath(const char *path);
int syspkg_readlink(const char *path, char *buf, size_t len);
int syspkg_symlink(const char *target, const char *path);
int syspkg_statfs(char *path, uint64_t *fsid, int64_t *freeblks, int *blksiz);
int syspkg_walkdir(char *directory, int parent, void *data, walk_cb pre, walk_cb suf);
int syspkg_mkdir(char *dir, int perm);
int syspkg_rm(char *fn);
int syspkg_rmrf(char *fn);
int syspkg_isdir(char *path);
int64_t syspkg_filesize(char *path);
int syspkg_rename(char *oldpath, char *newpath);
int syspkg_url(char *mask, char *arch, char *ver, char *release, char *osver, char *url);
int syspkg_parseurl(char *url, char **hostname, int *port, char **path);
int syspkg_readfileall(char *url, unsigned char **buf, size_t *len);
int syspkg_writefileall(char *fn, unsigned char *buf, size_t len);
syspkg_file_t *syspkg_open(char *url, int wr);
int syspkg_seek(syspkg_file_t *f, int64_t offs);
int syspkg_read(syspkg_file_t *f);
int syspkg_write(syspkg_file_t *f);
int syspkg_close(syspkg_file_t *f);

/* payloads.c */
void syspkg_payload_count(int type, int64_t size, uint64_t mtime, char *full, char *name, void *data);
void syspkg_payload_add(int type, int64_t size, uint64_t mtime, char *full, char *name, void *data);
void syspkg_payload_close(syspkg_build_t *meta);
void syspkg_payload_dest(syspkg_meta_t *meta, int zip, char *src, int len, char *dst, int *idx);
int syspkg_payload_extract(syspkg_ctx_t *ctx, syspkg_meta_t *meta, char *fn, int inst, uint64_t dld);

/* jsonc.c */
char *syspkg_json(const char *jsonstr, char *key, int siz);

/* cert.c */
int syspkg_gensig(syspkg_ctx_t *ctx, char *buffer);
int syspkg_chksig(mbedtls_x509_crt *repocrt, mbedtls_x509_buf *repocrl, char *buffer, char *cn);

/* meta.c */
size_t syspkg_validsize(char *s);
int syspkg_validver(char *ver, uint32_t *version);
syspkg_meta_t *syspkg_readmeta(syspkg_ctx_t *ctx, char *jsonstr, int filter, int *err);
int syspkg_freemeta(syspkg_meta_t *meta);
void syspkg_sprintf(char **buf, char **dst, size_t *n, char *fmt, ...);
char *syspkg_genjson(syspkg_meta_t *meta);
void syspkg_addfnmeta(syspkg_meta_t *meta, uint64_t size, char *fn, int len);
int syspkg_store(syspkg_ctx_t *ctx, syspkg_meta_t **meta, pkgidx_t num);
int syspkg_loadfiles(syspkg_meta_t *meta);

/* package.c */
int strnatcmp(const char *a, const char *b);
int syspkg_reset(syspkg_ctx_t *ctx);
int syspkg_chkaddpkg(syspkg_package_t ***list, int *len, char *id, int sn, syspkg_meta_t *add);
void syspkg_freeinstalled(syspkg_installed_t *inst);
int syspkg_loadinstalled(syspkg_ctx_t *ctx);
int syspkg_saveinstalled(syspkg_ctx_t *ctx);

/* postinst.c */
int syspkg_postenv(syspkg_ctx_t *ctx, syspkg_meta_t *meta);
int syspkg_postinst(syspkg_meta_t *meta);

/* attach.c */
int syspkg_saveattachment(syspkg_file_t *f, syspkg_file_t *fi, char *url);

/* html.c */
void syspkg_html(syspkg_ctx_t *ctx, char *repourl, char *pkgurls, char *htmlfile, char **dict);
