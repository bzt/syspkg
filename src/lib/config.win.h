/* valid architectures */
#define VALIDARCHS  "x32", "x64"
#ifdef __x86_64__
# define MYARCH "x64"
#else
# ifdef __i386__
#  define MYARCH "x32"
# else
#  define MYARCH "any"
# endif
#endif

/* valid categories */
#define VALIDCATS   "games"

/* the directory separator */
#define SEP "\\"

/* user local data, cert and keys are stored here */
#define USERDIR "%s\\AppData\\SysPkg\\"

/* global configuration file */
#define PKGCFG "C:\\Program Files\\SysPkg\\syspkg.json"

/* global data files' directory */
#define PKGDIR "C:\\Program Files\\SysPkg\\Var"

/* where the installed files are stored */
#define BINDIR "C:\\Program Files\\%s\\bin\\"
#define INCDIR "C:\\Program Files\\%s\\include\\"
#define LIBDIR "C:\\Windows\\system\\%s\\"
#define ETCDIR "C:\\Program Files\\%s\\etc\\"
#define SRCDIR "C:\\Program Files\\%s\\src\\"
#define DOCDIR "C:\\Windows\\Help\\%s\\"
#define SHRDIR "C:\\Program Files\\Common Files\\%s\\"
#define VARDIR "C:\\Program Files\\%s\\Var\\"

/* only used for the base package */
#define BASEDIR "C:\\"
