/* valid architectures */
#define VALIDARCHS  "x86_64", "aarch64", "riscv64"
#ifdef __x86_64__
# define MYARCH "x86_64"
#else
# ifdef __aarch64__
#  define MYARCH "aarch64"
# else
#  ifdef __riscv64__
#   define MYARCH "riscv64"
#  else
#   define MYARCH "any"
#  endif
# endif
#endif

/* valid categories */
#define VALIDCATS   "education", "games", "graphics", "internet", "office", "programming", "multimedia", "tools"

/* the directory separator */
#define SEP "/"

/* user local data, cert and keys are stored here */
#define USERDIR "%s/.config/syspkg/"

/* global configuration file */
#define PKGCFG "/opt/test/etc/syspkg.json"

/* global data files' directory */
#define PKGDIR "/opt/test/var/lib/syspkg/"

/* where the installed files are stored */
#define BINDIR "/opt/test/usr/bin/"
#define INCDIR "/opt/test/usr/include/%s/"
#define LIBDIR "/opt/test/lib/%s/"
#define ETCDIR "/opt/test/etc/%s/"
#define SRCDIR "/opt/test/usr/src/%s/"
#define DOCDIR "/opt/test/usr/share/man/"
#define SHRDIR "/opt/test/usr/share/%s/"
#define VARDIR "/opt/test/var/lib/%s/"

/* only used for the base package */
#define BASEDIR "/opt/test/"
