/*
 * syspkg/src/lib/package.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Package database related functions
 *
 */

#include "internal.h"

/**
 * Compare two package names
 */
int strnatcmp(const char *a, const char *b)
{
    const char *ca, *cb;
    int result;
    for(ca = a, cb = b; *ca && *cb; ca++, cb++) {
        if(isdigit(*ca) && isdigit(*cb)) {
            for(result = 0; isdigit(*ca) && isdigit(*cb); ca++, cb++)
                if(!result) result = *ca - *cb;
            if(!isdigit(*ca) && isdigit(*cb)) return -1;
            if(!isdigit(*cb) && isdigit(*ca)) return +1;
            if(result || (!*ca && !*cb)) return result;
        }
        if(*ca != *cb) return *ca - *cb;
    }
    return 0;
}

/**
 * Free installed record
 */
void syspkg_freeinstalled(syspkg_installed_t *inst)
{
    int i;
    if(!inst) return;
    if(inst->url) free(inst->url);
    if(inst->path) free(inst->path);
    if(inst->irelease) free(inst->irelease);
    if(inst->depends) {
        for(i = 0; inst->depends[i]; i++)
            free(inst->depends[i]);
        free(inst->depends);
    }
    if(inst->conflicts) {
        for(i = 0; inst->conflicts[i]; i++)
            free(inst->conflicts[i]);
        free(inst->conflicts);
    }
    if(inst->id) free(inst->id);
}

/**
 * Load installed packages list
 */
int syspkg_loadinstalled(syspkg_ctx_t *ctx)
{
    unsigned char *s, *e, tmp[65538];
    syspkg_file_t *f;
    pkgidx_t len = 0, i = 0, j;
    int k, n;
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(!ctx) return EINVAL;
    if(ctx->inst) {
        for(j = 0; j < ctx->numinst; j++)
            syspkg_freeinstalled(&ctx->inst[j]);
        free(ctx->inst);
        ctx->inst = NULL;
    }
    ctx->numinst = 0;
    zcmp = ZSTD_createDCtx();
    if(!zcmp) return ENOMEM;

    f = syspkg_open(PKGDIR "installed", 0);
    if(f) {
        syspkg_read(f);
        zi.src = f->buf; zi.size = f->olen; zi.pos = 0;
        zo.dst = &tmp; zo.size = sizeof(pkgidx_t); zo.pos = 0;
        *((pkgidx_t*)tmp) = 0;
        ZSTD_decompressStream(zcmp, &zo, &zi);
        len = *((pkgidx_t*)tmp);
        if(!len) goto err;
        ctx->inst = (syspkg_installed_t*)malloc((len + 1) * sizeof(syspkg_installed_t));
        if(!ctx->inst) { syspkg_close(f); return ENOMEM; }
        memset(ctx->inst, 0, (len + 1) * sizeof(syspkg_installed_t));
        for(j = i = 0; j < len; j++) {
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = sizeof(uint16_t); zo.pos = 0;
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.pos != sizeof(uint16_t) || !*((uint16_t*)tmp)) break;
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = *((uint16_t*)tmp) + sizeof(uint16_t); zo.pos = sizeof(uint16_t);
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.pos != *((uint16_t*)tmp) + sizeof(uint16_t)) break;
            ctx->inst[i].iref = *((pkgidx_t*)(tmp + 2));
            ctx->inst[i].iversion = *((uint32_t*)(tmp + sizeof(pkgidx_t) + 2)) & 0xffffff;
            s = tmp + sizeof(pkgidx_t) + 7;
            for(e = s; *e; e++);
            ctx->inst[i].id = (char*)malloc(e - s + 1);
            if(!ctx->inst[i].id) goto err;
            strcpy(ctx->inst[i].id, (char*)s);
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                ctx->inst[i].irelease = (char*)malloc(e - s + 1);
                if(!ctx->inst[i].irelease) goto err;
                strcpy(ctx->inst[i].irelease, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                ctx->inst[i].path = (char*)malloc(e - s + 1);
                if(!ctx->inst[i].path) goto err;
                strcpy(ctx->inst[i].path, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                ctx->inst[i].url = (char*)malloc(e - s + 1);
                if(!ctx->inst[i].url) goto err;
                strcpy(ctx->inst[i].url, (char*)s);
            }
            if(tmp[sizeof(pkgidx_t) + 5]) {
                n = tmp[sizeof(pkgidx_t) + 5];
                ctx->inst[i].depends = (char**)malloc((n + 1) * sizeof(char*));
                if(!ctx->inst[i].depends) goto err;
                memset(ctx->inst[i].depends, 0, (n + 1) * sizeof(char*));
                for(k = 0; k < n; k++) {
                    for(s = e + 1, e = s; *e; e++);
                    ctx->inst[i].depends[k] = (char*)malloc(e - s + 1);
                    if(!ctx->inst[i].depends[k]) goto err;
                    strcpy(ctx->inst[i].depends[k], (char*)s);
                }
            }
            if(tmp[sizeof(pkgidx_t) + 6]) {
                n = tmp[sizeof(pkgidx_t) + 6];
                ctx->inst[i].conflicts = (char**)malloc((n + 1) * sizeof(char*));
                if(!ctx->inst[i].conflicts) goto err;
                memset(ctx->inst[i].conflicts, 0, (n + 1) * sizeof(char*));
                for(k = 0; k < n; k++) {
                    for(s = e + 1, e = s; *e; e++);
                    ctx->inst[i].conflicts[k] = (char*)malloc(e - s + 1);
                    if(!ctx->inst[i].conflicts[k]) goto err;
                    strcpy(ctx->inst[i].conflicts[k], (char*)s);
                }
            }
            i++;
        }
err:    syspkg_close(f);
    }
    ZSTD_freeDCtx(zcmp);
    ctx->numinst = i;

    return ctx->inst ? SUCCESS : ENOMEM;
}

/**
 * Save installed packages list
 */
int syspkg_saveinstalled(syspkg_ctx_t *ctx)
{
    unsigned char tmp[65538];
    int ret = SUCCESS;
    syspkg_file_t *f;
    pkgidx_t i, j, k;
    ZSTD_CCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(!ctx) return EINVAL;
    zcmp = ZSTD_createCCtx();
    if(!zcmp) return ENOMEM;
    ZSTD_CCtx_setParameter(zcmp, ZSTD_c_compressionLevel, 1);
    ZSTD_CCtx_setParameter(zcmp, ZSTD_c_nbWorkers, 4);

    f = syspkg_open(PKGDIR "installed", 1);
    if(f) {
        for(j = i = 0; i < (pkgidx_t)ctx->numinst; i++)
            if(ctx->inst[i].id && ctx->inst[i].iref && ctx->inst[i].irelease && ctx->inst[i].path) j++;
        if(j) {
            *((pkgidx_t*)tmp) = (pkgidx_t)j;
            zi.src = &tmp; zi.size = sizeof(pkgidx_t); zi.pos = 0;
            zo.dst = f->buf; zo.size = sizeof(tmp); zo.pos = 0;
            ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_continue);
            for(i = 0; i < (pkgidx_t)ctx->numinst; i++) {
                if(!ctx->inst[i].id || !ctx->inst[i].iref || !ctx->inst[i].irelease || !ctx->inst[i].path) continue;
                *((pkgidx_t*)(tmp + 2)) = (pkgidx_t)ctx->inst[i].iref;
                *((uint32_t*)(tmp + sizeof(pkgidx_t) + 2)) = (uint32_t)ctx->inst[i].iversion;
                *((uint8_t*)(tmp + sizeof(pkgidx_t) + 5)) = 0;
                *((uint8_t*)(tmp + sizeof(pkgidx_t) + 6)) = 0;
                zi.size = sizeof(pkgidx_t) + 7;
                j = strlen(ctx->inst[i].id) + 1; memcpy(tmp + zi.size, ctx->inst[i].id, j); zi.size += j;
                j = strlen(ctx->inst[i].irelease) + 1; memcpy(tmp + zi.size, ctx->inst[i].irelease, j); zi.size += j;
                j = strlen(ctx->inst[i].path) + 1; memcpy(tmp + zi.size, ctx->inst[i].path, j); zi.size += j;
                if(ctx->inst[i].url) {
                    j = strlen(ctx->inst[i].url) + 1; memcpy(tmp + zi.size, ctx->inst[i].url, j); zi.size += j;
                } else tmp[zi.size++] = 0;
                if(ctx->inst[i].depends && ctx->inst[i].depends[0]) {
                    for(k = 0; k < 128 && ctx->inst[i].depends[k]; k++, tmp[sizeof(pkgidx_t) + 5]++) {
                        j = strlen(ctx->inst[i].depends[k])+1; memcpy(tmp+zi.size, ctx->inst[i].depends[k], j); zi.size+=j;
                    }
                }
                if(ctx->inst[i].conflicts && ctx->inst[i].conflicts[0]) {
                    for(k = 0; k < 128 && ctx->inst[i].conflicts[k]; k++, tmp[sizeof(pkgidx_t) + 6]++) {
                        j = strlen(ctx->inst[i].conflicts[k])+1; memcpy(tmp+zi.size, ctx->inst[i].conflicts[k], j); zi.size+=j;
                    }
                }
                *((uint16_t*)(tmp + 0)) = (uint16_t)zi.size - 2;
                zi.src = &tmp; zi.pos = 0;
                zo.dst = f->buf; zo.size = sizeof(tmp);
                ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_continue);
                f->ilen = zo.pos;
                f->olen = 0;
                syspkg_write(f);
                if(f->olen != f->ilen) { ret = EIO; break; }
                zo.pos = 0;
            }
            memset(tmp, 0, sizeof(uint16_t));
            zi.src = &tmp; zi.size = sizeof(uint16_t); zi.pos = 0;
            zo.dst = f->buf; zo.size = sizeof(tmp); zo.pos = 0;
            ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_end);
            f->ilen = zo.pos;
            f->olen = 0;
            syspkg_write(f);
        }
        syspkg_close(f);
    }
    ZSTD_freeCCtx(zcmp);
    return ret;
}

/**
 * Add to the to be installed or to be removed list (sn = short name match too)
 */
int syspkg_chkaddpkg(syspkg_package_t ***list, int *len, char *id, int sn, syspkg_meta_t *add)
{
    int k, l, a, b, m;
    char *d, *dot;

    if(!list || !len || (!id && !add)) return -2;
    k = 0;
    if(*list && *len) {
        if(!id) id = add->d.id;
        dot = strchr(id, '.');
        l = (int)sizeof(pkgidx_t) * 8 + 1;
        b = (*len) - 1; a = 0;
        while(l--) {
            k = a + ((b-a) >> 1);
            m = strnatcmp((*list)[k]->id, id);
            if(m && sn && !dot && (d = strchr((*list)[k]->id, '.')))
                m = strncmp((*list)[k]->id, id, d - (*list)[k]->id);
            if(!m) return k;
            if(a >= b) break;
            if(m < 0) a = k + 1; else b = k;
        }
        if(m < 0) k++;
    }
    if(add) {
        (*list) = (syspkg_package_t**)realloc(*list, ((*len) + 2) * sizeof(syspkg_package_t*));
        if(!*list) return -2;
        if(k < *len)
            memmove(&((*list)[k+1]), &((*list)[k]), ((*len) - k) * sizeof(syspkg_package_t*));
        (*list)[k] = (syspkg_package_t*)add;
        (*len)++;
        (*list)[*len] = NULL;
    } else k = -1;
    return k;
}

/**
 * Find an installed package
 */
int syspkg_getinst(syspkg_ctx_t *ctx, char *id, int *n)
{
    int l, i, j, k;
    k = 0; *n = 1;
    if(ctx->inst && ctx->numinst) {
        l = (int)sizeof(pkgidx_t) * 8 + 1;
        j = ctx->numinst - 1; i = 0;
        while(l--) {
            k = i + ((j-i) >> 1);
            *n = strnatcmp(ctx->inst[k].id, id);
            if(!*n) return k;
            if(i >= j) break;
            if(*n < 0) i = k + 1; else j = k;
        }
        if(*n < 0) k++;
    }
    return k;
}

/**
 * Calculate installed file size on a file system
 */
void syspkg_calcinst(syspkg_ctx_t *ctx, int64_t osize, int64_t nsize, uint64_t fsid, int64_t freeblks, int blksiz)
{
    int l = 9, i, j, k = 0;
    if(blksiz < 1) return;
    if(osize < 0) osize = 0;
    osize = (osize + blksiz - 1) / blksiz;
    nsize = (nsize + blksiz - 1) / blksiz;
    if(ctx->mnts && ctx->nummnts) {
        l = 17;
        j = ctx->nummnts - 1; i = 0;
        while(l--) {
            k = i + ((j-i) >> 1);
            if(ctx->mnts[k].fsid == fsid) {
                ctx->mnts[k].freeblks -= nsize - osize;
                return;
            }
            if(i >= j) break;
            if(ctx->mnts[k].fsid < fsid) i = k + 1; else j = k;
        }
        if(ctx->mnts[k].fsid < fsid) k++;
    }
    ctx->mnts = (syspkg_mnts_t*)realloc(ctx->mnts, (ctx->nummnts + 1) * sizeof(syspkg_mnts_t));
    if(!ctx->mnts) { ctx->nummnts = 0; return; }
    if(k < ctx->nummnts)
        memmove(&ctx->mnts[k+1], &ctx->mnts[k], (ctx->nummnts - k) * sizeof(syspkg_mnts_t));
    ctx->mnts[k].fsid = fsid;
    ctx->mnts[k].blksiz = blksiz;
    ctx->mnts[k].freeblks = freeblks - nsize + osize;
    ctx->nummnts++;
}

/**
 * Update package lists
 */
int syspkg_update(syspkg_ctx_t *ctx)
{
    syspkg_meta_t **packages = NULL, *meta;
    char *pkgurls = NULL, *c = NULL, *e = NULL, *jsonstr, *id, tmp[MAXPATHSIZE + 8 + 3 + sizeof(pkgidx_t)], cn[MAXCNSIZE+32];
    size_t len, l;
    mbedtls_x509_crt crt;
    mbedtls_x509_buf crl;
    syspkg_file_t *fdb, *adb;
#if ATTACHMENTS
    syspkg_file_t *idb;
    uint32_t aidx = 0;
#endif
    int i = 0, n = 0, k, a, b, m, r, ret = SUCCESS, idx = 0, nrepo = 0, npkg = 0, nremove = 0, instch = 0, numinst;
    pkgidx_t *toremove = NULL;
    ZSTD_DCtx* zdec;
    ZSTD_CCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(!ctx) return EINVAL;
    if(!syspkg_isadmin()) return EACCES;
    if(syspkg_lock(1)) return EEXIST;
    ctx->numfiles = 0;

    zcmp = ZSTD_createCCtx();
    if(!zcmp) return ENOMEM;
    ZSTD_CCtx_setParameter(zcmp, ZSTD_c_compressionLevel, 1);
    ZSTD_CCtx_setParameter(zcmp, ZSTD_c_nbWorkers, 4);

    syspkg_loadinstalled(ctx);
    numinst = ctx->numinst;
    fdb = syspkg_open(PKGDIR "files", 1);
#if ATTACHMENTS
    adb = syspkg_open(PKGDIR "attach", 1);
    idb = syspkg_open(PKGDIR "aindex", 1);
#endif
    for(nrepo = 0; ctx->repourls && ctx->repourls[nrepo] && nrepo < (pkgidx_t)-1U; nrepo++);
    nrepo++; /* because of installed packages "repo" */

    for(i = 0; ctx->repourls && i < nrepo && idx < (pkgidx_t)-1U; i++) {
        mbedtls_x509_crt_init(&crt);
        crl.p = NULL; crl.len = 0;
        if(ctx->progressbar) (*ctx->progressbar)(i + 1, nrepo, 0, 1, SYSPKG_MSG_GETREPO);
        if(ctx->repourls[i]) {
            if(syspkg_readfileall(ctx->repourls[i], (unsigned char **)&pkgurls, &len) || !pkgurls) continue;
            for(n = npkg = 0, c = pkgurls; c < pkgurls + len && *c && memcmp(c, PEM_BEG_CRT, 27); c++)
                if(*c == '\n') npkg++;
            if(c > pkgurls && *(c - 1) != '\n') npkg++;
            if(c == pkgurls || (ctx->repourls[i] && (!*c || memcmp(c, PEM_BEG_CRT, 27)))) { free(pkgurls); continue; }
            *(c - 1) = 0;
            if(mbedtls_x509_crt_parse(&crt, (unsigned char *)c, len + 1 - (c - pkgurls)) != 0 || !crt.ca_istrue ||
                !(crt.key_usage & MBEDTLS_X509_KU_KEY_CERT_SIGN)) { ret = EFAULT; goto err; }
            e = strstr(c, PEM_BEG_CRL);
            if(e) {
                e += 20;
                l = strlen(e);
                crl.p = (unsigned char *)malloc(l);
                if(!crl.p || mbedtls_base64_decode(crl.p, l, &crl.len, (unsigned char*)e, l + 1 - 18) != 0)
                    { ret = ENOMEM; goto err; }
            }
            len = c - pkgurls; c = pkgurls;
        } else {
            len = 0;
        }
        n = 0;
        while(ctx->repourls[i] ? (c < pkgurls + len && *c) : ((int)len < ctx->numinst)) {
            if(ctx->progressbar) (*ctx->progressbar)(i + 1, nrepo, n++, npkg + 1, SYSPKG_MSG_GETMETA);
            id = NULL;
            if(!ctx->repourls[i]) {
                /* this should never happen normally, installed package removed by hand */
                if(syspkg_filesize(ctx->inst[len].path) == -1) { ctx->inst[len].iversion = 0; instch = 1; len++; continue; }
                id = ctx->inst[len].id;
                c = ctx->inst[len].url;
                len++;
                /* if we can't find an installed package in the list, add it as if this was a repo */
                if(!c || syspkg_chkaddpkg((syspkg_package_t ***)&packages, &idx, id, 0, NULL) >= 0) continue;
            } else {
                for(e = c; e < pkgurls + len && *e && *e != '\r' && *e != '\n'; e++);
                if(c == e) break;
                *e++ = 0;
            }
            if(!syspkg_readfileall(c, (unsigned char **)&jsonstr, &l) && jsonstr && l) {
                cn[0] = 0; r = 0;
                if((!syspkg_chksig(ctx->repourls[i] ? &crt : NULL, ctx->repourls[i] ? &crl : NULL, jsonstr, (char*)&cn)) &&
                    (meta = syspkg_readmeta(ctx, jsonstr, SYSPKG_FILTER_STD, &r))) {
                    if(!ctx->repourls[i] && id) {
                        /* id must match, this should never happen, but be paranoid */
                        meta->d.id = (char*)realloc(meta->d.id, strlen(id) + 1);
                        if(!meta->d.id) { ret = ENOMEM; free(jsonstr); syspkg_freemeta(meta); goto err; }
                        strcpy(meta->d.id, id);
                    }
                    k = 0;
                    if(packages && idx) {
                        l = (int)sizeof(pkgidx_t) * 8 + 1;
                        b = idx - 1; a = 0;
                        while(l--) {
                            k = a + ((b-a) >> 1);
                            m = strnatcmp(packages[k]->d.id, meta->d.id);
                            if(!m) {
                                if(packages[k]->d.version >= meta->d.version) {
                                    syspkg_freemeta(meta);
                                    goto done;
                                }
                                toremove = (pkgidx_t*)realloc(toremove, (nremove + 1) * sizeof(pkgidx_t));
                                if(!toremove) { ret = ENOMEM; free(jsonstr); syspkg_freemeta(meta); goto err; }
                                toremove[nremove++] = packages[k]->idx;
                                goto update;
                            }
                            if(a >= b) break;
                            if(m < 0) a = k + 1; else b = k;
                        }
                        if(m < 0) k++;
                    }
                    packages = (syspkg_meta_t**)realloc(packages, (idx + 1) * sizeof(syspkg_meta_t*));
                    if(!packages) { ret = ENOMEM; idx = (pkgidx_t)-1U; free(jsonstr); syspkg_freemeta(meta); goto err; }
                    if(k < idx)
                        memmove(&packages[k+1], &packages[k], (idx - k) * sizeof(syspkg_meta_t*));
update:             if(cn[0]) {
                        meta->d.maintainer = (char*)malloc(strlen(cn) + 1);
                        if(meta->d.maintainer) strcpy(meta->d.maintainer, cn);
                    }
                    meta->idx = idx;
                    meta->repo = i;
                    meta->d.afirst = meta->d.alast = -1U;
#if ATTACHMENTS
                    if(meta->screenshots) {
                        for(a = 0; meta->screenshots[a]; a++)
                            if(!syspkg_saveattachment(adb, idb, meta->screenshots[a])) {
                                if(meta->d.afirst == -1U) meta->d.afirst = aidx;
                                meta->d.alast = aidx++;
                            }
                    }
#endif
                    if(meta->files) {
                        syspkg_payload_dest(meta, 0, meta->files[0] + 8, strlen(meta->files[0] + 8), (char*)&tmp, NULL);
                        if(tmp[0]) {
                            a = syspkg_filesize((char*)&tmp);
                            b = syspkg_getinst(ctx, meta->d.id, &m);
                            if(a == -1 && !m) { ctx->inst[b].iversion = 0; instch = 1; } else
                            if(a != -1 && m) {
                                ctx->inst = (syspkg_installed_t*)realloc(ctx->inst, (ctx->numinst + 1) * sizeof(syspkg_installed_t));
                                if(!ctx->inst) { ret = ENOMEM; goto err; }
                                if(b < ctx->numinst)
                                    memmove(&ctx->inst[b+1], &ctx->inst[b], (ctx->numinst - b) * sizeof(syspkg_installed_t));
                                ctx->numinst++;
                                memset(&ctx->inst[b], 0, sizeof(syspkg_installed_t));
                                ctx->inst[b].id = (char*)malloc(strlen(meta->d.id) + 1);
                                if(ctx->inst[b].id) strcpy(ctx->inst[b].id, meta->d.id);
                                ctx->inst[b].iref = 1;
                                ctx->inst[b].iversion = meta->d.version;
                                ctx->inst[b].irelease = (char*)malloc(strlen(meta->d.release) + 1);
                                if(ctx->inst[b].irelease) strcpy(ctx->inst[b].irelease, meta->d.release);
                                ctx->inst[b].url = (char*)malloc(strlen(c) + 1);
                                if(ctx->inst[b].url) strcpy(ctx->inst[b].url, c);
                                ctx->inst[b].path = (char*)malloc(strlen(tmp) + 1);
                                if(ctx->inst[b].path) strcpy(ctx->inst[b].path, tmp);
                                if(meta->d.conflicts) {
                                    for(a = 0; meta->d.conflicts[a].id; a++);
                                    ctx->inst[b].conflicts = (char**)malloc((b + 2) * sizeof(char*));
                                    if(!ctx->inst[b].conflicts) { ret = ENOMEM; goto err; }
                                    ctx->inst[b].conflicts[a] = NULL;
                                    for(a = 0; meta->d.conflicts[a].id; a++) {
                                        ctx->inst[b].conflicts[a] = (char*)malloc(strlen(meta->d.conflicts[a].id) + 1);
                                        if(!ctx->inst[b].conflicts[a]) { ret = ENOMEM; goto err; }
                                        strcpy(ctx->inst[b].conflicts[a], meta->d.conflicts[a].id);
                                    }
                                }
                                if(meta->d.depends) {
                                    for(a = 0; meta->d.depends[a].id; a++);
                                    ctx->inst[b].depends = (char**)malloc((b + 2) * sizeof(char*));
                                    if(!ctx->inst[b].depends) { ret = ENOMEM; goto err; }
                                    ctx->inst[b].depends[a] = NULL;
                                    for(a = 0; meta->d.depends[a].id; a++) {
                                        ctx->inst[b].depends[a] = (char*)malloc(strlen(meta->d.depends[a].id) + 1);
                                        if(!ctx->inst[b].depends[a]) { ret = ENOMEM; goto err; }
                                        strcpy(ctx->inst[b].depends[a], meta->d.depends[a].id);
                                    }
                                }
                                instch = 1;
                            }
                        }
                        for(b = 0; b < meta->numfiles && meta->files[b]; b++) {
                            *((pkgidx_t*)(tmp + sizeof(uint16_t))) = idx;
                            memcpy(tmp + sizeof(uint16_t) + sizeof(pkgidx_t), meta->files[b], 8 + strlen(meta->files[b] + 8) + 1);
                            zi.src = &tmp; zi.size = sizeof(pkgidx_t) + sizeof(uint16_t) + 8 + strlen(meta->files[b] + 8) + 1;
                            zi.pos = 0;
                            zo.dst = fdb->buf; zo.size = FBUFSIZE; zo.pos = 0;
                            *((uint16_t*)tmp) = (uint16_t)zi.size - sizeof(uint16_t);
                            ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_continue);
                            fdb->ilen = zo.pos;
                            fdb->olen = 0;
                            syspkg_write(fdb);
                            free(meta->files[b]);
                        }
                        ctx->numfiles += meta->numfiles;
                        free(meta->files);
                        meta->files = NULL;
                    }
                    for(b = 0; b < OVERRIDELEN; b++)
                        if(meta->over[b]) { free(meta->over[b]); meta->over[b] = NULL; }
                    packages[k] = meta;
                    idx++; if(idx >= (pkgidx_t)-2U) break;
                }
done:           free(jsonstr);
            }
            if(ctx->repourls[i])
                for(c = e; c < pkgurls + len && (*c == '\r' || *c == '\n'); c++);
        }
err:    mbedtls_x509_crt_free(&crt);
        if(crl.p) free(crl.p);
        crl.p = NULL; crl.len = 0;
        if(pkgurls) free(pkgurls);
        pkgurls = NULL;
        if(!ctx->repourls[i]) break;
    }
    if((instch || numinst != ctx->numinst) && idx < (pkgidx_t)-1U && ret == SUCCESS) {
        /* somebody messed with the packages manually, recalculate reference counters */
        for(i = 0; i < ctx->numinst; i++) ctx->inst[i].iref = 0;
        for(i = 0; i < ctx->numinst; i++)
            if(ctx->inst[i].iversion && ctx->inst[i].depends)
                for(a = 0; ctx->inst[i].depends[a]; a++) {
                    b = syspkg_getinst(ctx, ctx->inst[i].depends[a], &m);
                    if(!m) ctx->inst[b].iref++;
                }
        for(i = 0; i < ctx->numinst; i++)
            if(ctx->inst[i].iversion) {
                if(!ctx->inst[i].iref) ctx->inst[i].iref++;
            }
        ret = syspkg_saveinstalled(ctx);
    }

    if(ctx->progressbar)
        (*ctx->progressbar)(nrepo, nrepo, 1, packages && idx < (pkgidx_t)-1U && ret == SUCCESS ? 1 : -1, SYSPKG_MSG_GETMETA);
    if(packages) {
        if(idx > 0 && idx < (pkgidx_t)-1U && ret == SUCCESS)
            ret = syspkg_store(ctx, packages, idx);
        for(i = 0; i < idx; i++)
            syspkg_freemeta(packages[i]);
        free(packages);
        ctx->numpackages = idx;
    }
    memset(tmp, 0, sizeof(uint16_t));
    zi.src = &tmp; zi.size = sizeof(uint16_t); zi.pos = 0;
    zo.dst = fdb->buf; zo.size = FBUFSIZE; zo.pos = 0;
    ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_end);
    fdb->ilen = zo.pos;
    fdb->olen = 0;
    syspkg_write(fdb);
    syspkg_close(fdb);
#if ATTACHMENTS
    syspkg_close(adb);
    syspkg_close(idb);
#endif
    if(ret == SUCCESS && nremove && toremove) {
        zdec = ZSTD_createDCtx();
        if(zdec) {
            syspkg_rename(PKGDIR "files", PKGDIR "files.old");
            adb = syspkg_open(PKGDIR "files.old", 0);
            fdb = syspkg_open(PKGDIR "files", 1);
            zi.pos = zi.size = 0;
            for(n = 0; n < nremove; n++) {
                if(ctx->progressbar)
                    (*ctx->progressbar)(0, 0, n + 1, nremove, SYSPKG_MSG_CLNMETA);
                if(zi.pos == zi.size) { syspkg_read(adb); zi.src = adb->buf; zi.size = adb->olen; zi.pos = 0; }
                zo.dst = &tmp; zo.size = sizeof(uint16_t); zo.pos = 0;
                ZSTD_decompressStream(zdec, &zo, &zi);
                if(zo.size != sizeof(uint16_t) || !*((uint16_t*)tmp)) break;
                if(zi.pos == zi.size) { syspkg_read(adb); zi.src = adb->buf; zi.size = adb->olen; zi.pos = 0; }
                zo.dst = &tmp; zo.size = *((uint16_t*)tmp); zo.pos = sizeof(uint16_t);
                ZSTD_decompressStream(zdec, &zo, &zi);
                if(zo.pos != *((uint16_t*)tmp) + sizeof(uint16_t)) break;
                for(i = 0; i < nremove && toremove[i] != *((pkgidx_t*)(tmp + sizeof(uint16_t))); i++);
                if(i == nremove) {
                    zi.src = &tmp; zi.size = zo.pos; zi.pos = 0;
                    zo.dst = fdb->buf; zo.size = FBUFSIZE; zo.pos = 0;
                    ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_continue);
                    fdb->ilen = zo.pos;
                    fdb->olen = 0;
                    syspkg_write(fdb);
                }
            }
            memset(tmp, 0, sizeof(uint16_t));
            zi.src = &tmp; zi.size = sizeof(uint16_t); zi.pos = 0;
            zo.dst = fdb->buf; zo.size = FBUFSIZE; zo.pos = 0;
            ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_end);
            fdb->ilen = zo.pos;
            fdb->olen = 0;
            syspkg_write(fdb);
            syspkg_close(fdb);
            syspkg_close(adb);
            syspkg_rm(PKGDIR "files.old");
            ZSTD_freeDCtx(zdec);
        }
    }
    if(toremove) free(toremove);
    ZSTD_freeCCtx(zcmp);
    syspkg_unlock();
    return ret;
}

/**
 * Reset dependency lists
 */
int syspkg_reset(syspkg_ctx_t *ctx)
{
    if(ctx->installs) {
        free(ctx->installs);
        ctx->installs = NULL;
    }
    if(ctx->removes) {
        free(ctx->removes);
        ctx->removes = NULL;
    }
    if(ctx->suggests) {
        free(ctx->suggests);
        ctx->suggests = NULL;
    }
    if(ctx->missing) {
        free(ctx->missing);
        ctx->missing = NULL;
    }
    if(ctx->conflicts) {
        free(ctx->conflicts);
        ctx->conflicts = NULL;
    }
    if(ctx->mnts) {
        free(ctx->mnts);
        ctx->mnts = NULL;
    }
    ctx->numinstalls = ctx->numremoves = ctx->numsuggests = ctx->nummissing = ctx->numconflicts = ctx->nummnts = 0;
    ctx->dlsize = ctx->total = ctx->comp = 0;
    return syspkg_search(ctx, 0, NULL, NULL, NULL);
}

/**
 * Reentrant version of mark for install, collects dependencies
 */
int _syspkg_install(syspkg_ctx_t *ctx, char *name, int level)
{
    syspkg_meta_t *meta;
    char *jsonstr, tmp[MAXPATHSIZE + 1], **list;
    size_t len;
    int64_t size, freeblks;
    uint64_t fsid;
    int ret = SUCCESS, k, l, n, i, blksiz;

    if(level >= MAXDEPLEVEL) return ENOENT;

    /* check if its already going to be installed */
    if(syspkg_chkaddpkg(&ctx->installs, &ctx->numinstalls, name, 0, NULL) >= 0) return SUCCESS;
    /* get package meta info */
    k = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, name, 0, NULL);
    if(k < 0) {
        /* if not found, but we can read a metajson, temporarily append to the package list */
        if(level > 0 || syspkg_readfileall(name, (unsigned char **)&jsonstr, &len)) return ENOENT;
        if(!(meta = syspkg_readmeta(ctx, jsonstr, SYSPKG_FILTER_STD, &ret))) { free(jsonstr); return ret; }
        k = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, NULL, 0, meta);
        if(k < 0) { free(jsonstr); syspkg_freemeta(meta); return ENOMEM; }
        meta->idx = ctx->numpackages - 1;
        meta->metaurl = name;
        free(jsonstr);
    } else
        meta = (syspkg_meta_t*)ctx->packages[k];
    /* resolve dependencies */
    if(meta->d.version > meta->d.iversion || (level <= 0 && meta->d.version == meta->d.iversion)) {
        if(level != -1) {
            /* if this package conflicts with one of the already installed ones */
            if(meta->d.conflicts) {
                for(i = 0; meta->d.conflicts[i].id; i++) {
                    l = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, meta->d.conflicts[i].id, 1, NULL);
                    if(l >= 0 && k != l && ctx->packages[l]->iversion &&
                        ctx->packages[l]->iversion >= meta->d.conflicts[i].version) {
                            ctx->conflicts = (char**)realloc(ctx->conflicts, (ctx->numconflicts + 3) * sizeof(char*));
                            if(!ctx->conflicts) return ENOMEM;
                            ctx->conflicts[ctx->numconflicts++] = meta->d.id;
                            ctx->conflicts[ctx->numconflicts++] = meta->d.conflicts[i].id;
                            ctx->conflicts[ctx->numconflicts] = NULL;
                            ret = ENOENT;
                    }
                }
            }
            /* if one of the already installed ones conflicts with this package */
            for(i = 0; i < ctx->numinst; i++)
                if(ctx->inst[i].conflicts)
                    for(l = 0; ctx->inst[i].conflicts[l]; l++)
                        if(!strcmp(meta->d.id, ctx->inst[i].conflicts[l])) {
                            ctx->conflicts = (char**)realloc(ctx->conflicts, (ctx->numconflicts + 3) * sizeof(char*));
                            if(!ctx->conflicts) return ENOMEM;
                            ctx->conflicts[ctx->numconflicts++] = meta->d.id;
                            ctx->conflicts[ctx->numconflicts++] = ctx->inst[i].id;
                            ctx->conflicts[ctx->numconflicts] = NULL;
                            ret = ENOENT;
                        }
            if(ret != SUCCESS) return ret;
        }
        if(level != -1 && !meta->d.iversion && meta->d.suggests) {
            for(i = 0; meta->d.suggests[i].id; i++) {
                for(l = 0; l < ctx->numsuggests && strcmp(ctx->suggests[l]->id, meta->d.suggests[i].id); l++);
                if(l < ctx->numsuggests) continue;
                l = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, meta->d.suggests[i].id, 1, NULL);
                if(l >= 0 && !ctx->packages[l]->iversion) {
                    ctx->suggests = (syspkg_dep_t**)realloc(ctx->suggests, (ctx->numsuggests + 2) * sizeof(syspkg_dep_t*));
                    if(!ctx->suggests) return ENOMEM;
                    ctx->suggests[ctx->numsuggests++] = &meta->d.suggests[i];
                    ctx->suggests[ctx->numsuggests] = NULL;
                }
            }
        }
        if(meta->payloads && meta->payloads[0].comp &&
            syspkg_chkaddpkg(&ctx->installs, &ctx->numinstalls, NULL, 0, meta) < 0) return ENOMEM;
        meta->d.err = level > 0 ? level : 0;
        k = syspkg_getinst(ctx, meta->d.id, &n);
        if(!n) {
            if(ctx->inst[k].irelease) free(ctx->inst[k].irelease);
            if(ctx->inst[k].url) { free(ctx->inst[k].url); ctx->inst[k].url = NULL; }
            if(ctx->inst[k].path) { free(ctx->inst[k].path); ctx->inst[k].path = NULL; }
            if(ctx->inst[k].conflicts) {
                for(i = 0; ctx->inst[k].conflicts[i]; i++) {
                    free(ctx->inst[k].conflicts[i]);
                }
                free(ctx->inst[k].conflicts);
                ctx->inst[k].conflicts = NULL;
            }
            if(ctx->inst[k].depends) {
                for(i = 0; ctx->inst[k].depends[i]; i++) {
                    if(level != -1) {
                        l = syspkg_getinst(ctx, ctx->inst[k].depends[i], &n);
                        if(!n) ctx->inst[l].iref--;
                    }
                    free(ctx->inst[k].depends[i]);
                }
                free(ctx->inst[k].depends);
                ctx->inst[k].depends = NULL;
            }
        } else {
            ctx->inst = (syspkg_installed_t*)realloc(ctx->inst, (ctx->numinst + 1) * sizeof(syspkg_installed_t));
            if(!ctx->inst) return ENOMEM;
            if(k < ctx->numinst)
                memmove(&ctx->inst[k+1], &ctx->inst[k], (ctx->numinst - k) * sizeof(syspkg_installed_t));
            ctx->numinst++;
            memset(&ctx->inst[k], 0, sizeof(syspkg_installed_t));
            ctx->inst[k].id = (char*)malloc(strlen(meta->d.id) + 1);
            if(ctx->inst[k].id) strcpy(ctx->inst[k].id, meta->d.id);
            ctx->inst[k].iref = 1;
        }
        ctx->inst[k].iversion = meta->d.version;
        ctx->inst[k].irelease = (char*)malloc(strlen(meta->d.release) + 1);
        if(ctx->inst[k].irelease) strcpy(ctx->inst[k].irelease, meta->d.release);
        if(meta->metaurl) {
            ctx->inst[k].url = (char*)malloc(strlen(meta->metaurl) + 1);
            if(ctx->inst[k].url) strcpy(ctx->inst[k].url, meta->metaurl);
        }
        /* if this is an upgrade, substract current file sizes, otherwise just add */
        if(meta->payloads) {
            if(meta->payloads[0].comp > ctx->comp) ctx->comp = meta->payloads[0].comp;
            ctx->dlsize += meta->payloads[0].comp;
            ctx->total += meta->payloads[0].orig;
            if(!meta->files) syspkg_loadfiles(meta);
            if(meta->files && meta->numfiles) {
                syspkg_payload_dest(meta, 0, meta->files[0] + 8, strlen(meta->files[0] + 8), (char*)&tmp, NULL);
                ctx->inst[k].path = (char*)malloc(strlen(tmp) + 1);
                if(ctx->inst[k].path) strcpy(ctx->inst[k].path, tmp);
                for(i = 0; meta->files[i] && i < meta->numfiles; i++) {
                    syspkg_payload_dest(meta, 0, meta->files[i] + 8, strlen(meta->files[i] + 8), (char*)&tmp, NULL);
                    size = tmp[0] ? syspkg_filesize(tmp) : 0;
                    if(size < 0) size = 0;
                    ctx->total -= size;
                    syspkg_statfs((char*)tmp, &fsid, &freeblks, &blksiz);
                    syspkg_calcinst(ctx, size, *((int64_t*)meta->files[i]), fsid, freeblks, blksiz);
                    free(meta->files[i]);
                }
                free(meta->files);
                meta->files = NULL;
            }
            meta->numfiles = 0;
        }
        /* we must make a copy of depends and conflicts, because installed version might
         * have different dependencies than the meta info in the updated packages list */
        if(meta->d.conflicts) {
            for(i = 0; meta->d.conflicts[i].id; i++);
            ctx->inst[k].conflicts = list = (char**)malloc((i + 1) * sizeof(char*));
            if(!ctx->inst[k].conflicts) return ENOMEM;
            list[i] = NULL;
            for(i = 0; meta->d.conflicts[i].id; i++) {
                list[i] = (char*)malloc(strlen(meta->d.conflicts[i].id) + 1);
                if(!list[i]) return ENOMEM;
                strcpy(list[i], meta->d.conflicts[i].id);
            }
        }
        if(meta->d.depends) {
            for(i = 0; meta->d.depends[i].id; i++);
            ctx->inst[k].depends = list = (char**)malloc((i + 1) * sizeof(char*));
            if(!ctx->inst[k].depends) return ENOMEM;
            list[i] = NULL;
            for(i = 0; meta->d.depends[i].id; i++) {
                list[i] = (char*)malloc(strlen(meta->d.depends[i].id) + 1);
                if(!list[i]) return ENOMEM;
                strcpy(list[i], meta->d.depends[i].id);
                if(level != -1) {
                    l = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, meta->d.depends[i].id, 1, NULL);
                    if(l < 0 || (meta->d.depends[i].version && meta->d.depends[i].version > ctx->packages[l]->iversion)) {
                        ctx->missing = (syspkg_dep_t**)realloc(ctx->missing, (ctx->nummissing + 2) * sizeof(syspkg_dep_t*));
                        if(!ctx->missing) return ENOMEM;
                        ctx->missing[ctx->nummissing++] = &meta->d.depends[i];
                        ctx->missing[ctx->nummissing] = NULL;
                        ret = ENOENT;
                    } else if(ret == SUCCESS) {
                        l = syspkg_getinst(ctx, meta->d.depends[i].id, &n);
                        if(!n) ctx->inst[l].iref++;
                        ret = _syspkg_install(ctx, meta->d.depends[i].id, level + 1);
                    }
                }
            }
        }
    }
    return ret;
}

/**
 * Mark an installed package for removal and recursively its dependencies
 * requires syspkg_commit()
 */
int _syspkg_uninstall(syspkg_ctx_t *ctx, char *name, int level)
{
    syspkg_meta_t *meta;
    int ret = SUCCESS, k, i, l, m = 0;

    if(level > MAXDEPLEVEL) return ENOENT;

    /* check if its already going to be removed */
    k = syspkg_chkaddpkg(&ctx->removes, &ctx->numremoves, name, 0, NULL);
    /* check if its going to be installed too */
    if(k >= 0 && syspkg_chkaddpkg(&ctx->installs, &ctx->numinstalls, name, 0, NULL) >= 0) {
        memmove(&ctx->removes[k], &ctx->removes[k+1], (ctx->numremoves - k) * sizeof(syspkg_package_t*));
        ctx->numremoves--;
    }
    if(k >= 0) return SUCCESS;

    /* get package meta info */
    k = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, name, 0, NULL);
    if(k < 0) return ENOENT;
    meta = (syspkg_meta_t*)ctx->packages[k];
    if(meta->payloads && meta->payloads[0].arch) {
        if(!level && !meta->d.iversion) return EBADF;
        /* add to list */
        syspkg_chkaddpkg(&ctx->removes, &ctx->numremoves, NULL, 0, meta);
        ctx->total -= meta->payloads[0].orig;
    } else
        m = (level < 1) ? 1 : 0;
    k = syspkg_getinst(ctx, meta->d.id, &l);
    if(!l) ctx->inst[k].iversion = ctx->inst[k].iref = 0;
    if(level != -1) {
        /* decrease dependencies' reference counters */
        if(meta->d.depends) {
            for(i = 0; meta->d.depends[i].id; i++) {
                k = syspkg_getinst(ctx, meta->d.depends[i].id, &l);
                if(!l) {
                    if(ctx->inst[k].iref) { if(!m) { ctx->inst[k].iref--; } else { ctx->inst[k].iref = 0; } }
                    if(!ctx->inst[k].iref)
                        _syspkg_uninstall(ctx, meta->d.depends[i].id, level + 1);
                }
            }
        }
        /* also remove packages which depend on this one */
        for(i = 0; i < ctx->numinst; i++)
            if(ctx->inst[i].iversion && ctx->inst[i].depends)
                for(l = 0; ctx->inst[i].depends[l]; l++)
                    if(!strcmp(ctx->inst[i].depends[l], meta->d.id))
                        _syspkg_uninstall(ctx, ctx->inst[i].id, 1);
    }
    return ret;
}

/**
 * Check installed packages which one needs to be removed
 */
int syspkg_chkinst(syspkg_ctx_t *ctx, int ret)
{
    int i, k, l;
    syspkg_meta_t *meta;
    /* if a previously installed package found out to be removed, make sure it's marked as uninstalled */
    for(i = 0; i < ctx->numinst; i++)
        if(!ctx->inst[i].iref || !ctx->inst[i].iversion)
            _syspkg_uninstall(ctx, ctx->inst[i].id, 0);
    /* check if one of the to be installed packages are in conflict. This must be done at the end when the list is full */
    for(k = 0; k < ctx->numinstalls; k++) {
        meta = (syspkg_meta_t*)ctx->installs[k];
        if(meta->d.conflicts) {
            for(i = 0; meta->d.conflicts[i].id; i++) {
                l = syspkg_chkaddpkg(&ctx->installs, &ctx->numinstalls, meta->d.conflicts[i].id, 1, NULL);
                if(l >= 0 && k != l) {
                    ctx->conflicts = (char**)realloc(ctx->conflicts, (ctx->numconflicts + 3) * sizeof(char*));
                    if(!ctx->conflicts) return ENOMEM;
                    ctx->conflicts[ctx->numconflicts++] = meta->d.id;
                    ctx->conflicts[ctx->numconflicts++] = meta->d.conflicts[i].id;
                    ctx->conflicts[ctx->numconflicts] = NULL;
                    if(ret == SUCCESS) ret = ENOENT;
                }
            }
        }
    }
    return ret;
}

/**
 * Mark all installed packages for upgrade if there's a newer version available
 * requires syspkg_commit()
 */
int syspkg_upgrade(syspkg_ctx_t *ctx)
{
    int i, ret = SUCCESS;
    if(!ctx) return EINVAL;
    if(!syspkg_isadmin()) return EACCES;
    if(syspkg_lock(0)) return EEXIST;
    syspkg_reset(ctx);
    for(i = 0; i < ctx->numpackages && ctx->packages[i] && ret == SUCCESS; i++)
        if(ctx->packages[i]->iversion && ctx->packages[i]->version > ctx->packages[i]->iversion)
            ret = _syspkg_install(ctx, ctx->packages[i]->id, 0);
    ret = syspkg_chkinst(ctx, ret);
    /* no suggestions when upgrading */
    if(ctx->suggests) free(ctx->suggests);
    ctx->suggests = NULL;
    ctx->numsuggests = 0;
    return ret;
}

/**
 * Remove a repository, certificate or mark packages for removal
 */
int syspkg_remove(syspkg_ctx_t *ctx, char **name, int nodeps)
{
    size_t len;
    char *jsonstr, *packageid = NULL;
    int i, r = 0;

    if(!ctx || !name || !name[0] || !name[0][0]) return EINVAL;
    if(!syspkg_isadmin()) return EACCES;
    if(syspkg_lock(0)) return EEXIST;
    syspkg_reset(ctx);

    for(i = 0; name[i]; i++) {
        /* see if a repository url or canonical name given */
        r = syspkg_delrepo(ctx, name[i]);
        if(r == SUCCESS) continue;
        if(r != ENOENT) return r;

        /* see if it's a trusted certificate's canonical name */
        if(name[i][0] == 'C' && name[i][1] == 'N' && name[i][2] == '=') {
            r = syspkg_untrust(ctx, name[i]);
            if(r == SUCCESS) continue;
            if(r != ENOENT) return r;
        }

        /* see if it's a package metajson */
        if(!syspkg_readfileall(name[i], (unsigned char **)&jsonstr, &len) && jsonstr) {
            /* remove both the trusted self-signed cert and the package */
            packageid = strstr(jsonstr, PEM_BEG_SIG);
            if(packageid) syspkg_untrust(ctx, packageid);
            packageid = syspkg_json(jsonstr, "id", MAXIDSIZE);
            free(jsonstr);
        }
        if(!packageid) packageid = name[i];

        /* remove if it's an installed package */
        r = _syspkg_uninstall(ctx, packageid, nodeps ? -1 : 0);

        /* maybe not recognized */
        if(packageid && packageid != name[i]) free(packageid);
        if(r != ENOENT) return r;
    }
    return r;
}

/**
 * Mark packages for install or upgrade
 * requires syspkg_commit()
 */
int syspkg_install(syspkg_ctx_t *ctx, char **name, int nodeps)
{
    int i, ret = SUCCESS;
    if(!ctx || !name || !*name) return EINVAL;
    if(!syspkg_isadmin()) return EACCES;
    if(syspkg_lock(0)) return EEXIST;
    syspkg_reset(ctx);

    for(i = 0; name[i] && (ret = _syspkg_install(ctx, name[i], nodeps ? -1 : 0)) == SUCCESS; i++);
    if(!nodeps && ret == SUCCESS) ret = syspkg_chkinst(ctx, ret);
    /* remove suggestions that are going to be installed along with this commit */
    if(ctx->suggests && ctx->installs) {
        for(i = 0; ctx->suggests[i]; )
            if(syspkg_chkaddpkg(&ctx->installs, &ctx->numinstalls, ctx->suggests[i]->id, 0, NULL) >= 0) {
                memmove(&ctx->suggests[i], &ctx->suggests[i + 1], (ctx->numsuggests - i) * sizeof(syspkg_dep_t*));
                ctx->numsuggests--;
            } else
                i++;
    }
    return ret;
}

/**
 * Reconfigure packages
 */
int syspkg_reconf(syspkg_ctx_t *ctx, char **name)
{
    int i, k, ret = SUCCESS;

    if(!ctx || !name || !name[0] || !name[0][0]) return EINVAL;
    if(!syspkg_isadmin()) return EACCES;
    if(syspkg_lock(0)) return EEXIST;
    ret = syspkg_search(ctx, 0, NULL, NULL, NULL);
    if(ret != SUCCESS) return ret;
    for(i = 0; name[i]; i++) {
        k = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, name[i], 0, NULL);
        if(k < 0 || !ctx->packages[k]->iversion) return ENOENT;
        ret = syspkg_postenv(ctx, (syspkg_meta_t*)ctx->packages[k]);
        if(ret != SUCCESS) return ret;
    }
    for(i = 0; name[i]; i++) {
        k = syspkg_chkaddpkg(&ctx->packages, &ctx->numpackages, name[i], 0, NULL);
        syspkg_postinst((syspkg_meta_t*)ctx->packages[k]);
    }
    return ret;
}

/**
 * Install, upgrade or remove marked packages
 */
int syspkg_commit(syspkg_ctx_t *ctx)
{
    int i, j, k, l, ret = SUCCESS, maxlvl = 0, blksiz;
    unsigned char hash[32];
    char tmp[MAXPATHSIZE], *c;
    int64_t freeblks;
    uint64_t dld = 0, fsid;
    syspkg_meta_t *meta;
    syspkg_file_t *f, *g = NULL;
    mbedtls_sha256_context sha;

    if(!ctx || !ctx->packages || !ctx->numpackages || (!ctx->installs && !ctx->removes)) return EINVAL;
    if(!syspkg_isadmin()) return EACCES;
    if(syspkg_lock(1)) return EEXIST;

    if(ctx->installs) {
        /* check free space */
        sprintf(tmp, PKGDIR);
        syspkg_statfs(tmp, &fsid, &freeblks, &blksiz);
        if(blksiz && (freeblks <= 1 || (int64_t)((ctx->comp + blksiz - 1)/blksiz) > freeblks - 1)) return ENOSPC;
        for(i = 0; i < ctx->nummnts; i++)
            if(ctx->mnts[i].freeblks < 1) return ENOSPC;

        /* IMPORTANT: do NOT interrupt install by prompting the user. Instead collect all the data in advance */

        /* accept eula and get configuration */
        for(i = 0; i < ctx->numinstalls; i++) {
            ret = syspkg_postenv(ctx, (syspkg_meta_t*)ctx->installs[i]);
            if(ret != SUCCESS) return ret;
        }

        /* download and extract packages */
        for(i = 0; i < ctx->numinstalls; i++) {
            meta = (syspkg_meta_t*)ctx->installs[i];
            if(meta->d.err > maxlvl) maxlvl = meta->d.err;
            if(!meta->payloads || !meta->payloads[0].comp) continue;
            if(meta->d.err < 0) {
                if(meta->payloads) dld += 2 * meta->payloads[0].comp;
                continue;
            }
            if(ctx->progressbar) (*ctx->progressbar)(i + 1, ctx->numinstalls, dld, 2 * ctx->dlsize + 1, SYSPKG_MSG_UNPPKG);
            /* check payload's sha. If it's a remote url, also store the file in local cache */
            f = syspkg_open(meta->d.url, 0);
            if(f && f->buf) {
                f->pos = 0;
                if(!memcmp(meta->d.url, "https://", 8)) {
                    sprintf(tmp, PKGDIR "tmp");
                    g = syspkg_open(tmp, 1);
                    if(g) { free(g->buf); g->buf = NULL; }
                } else
                    g = NULL;
                mbedtls_sha256_init(&sha);
                mbedtls_sha256_starts(&sha, 0);
                while(!syspkg_read(f)) {
                    if(ctx->progressbar) {
                        (*ctx->progressbar)(i + 1, ctx->numinstalls, dld, 2 * ctx->dlsize + 1, SYSPKG_MSG_UNPPKG);
                        dld += f->olen;
                    }
                    if(!f->pos && *((uint32_t*)f->buf) != ZIP_LOCAL_MAGIC) break;
                    mbedtls_sha256_update(&sha, (const unsigned char*)f->buf, f->olen);
                    if(g) {
                        g->buf = f->buf; g->ilen = f->olen; g->olen = 0;
                        if(!syspkg_write(g)) { ret = EPERM; break; }
                    }
                    f->pos += f->olen;
                    f->olen = 0;
                }
                syspkg_close(f);
                if(g) {
                    g->buf = NULL;
                    syspkg_close(g);
                }
                mbedtls_sha256_finish(&sha, hash);
                mbedtls_sha256_free(&sha);
                if(memcmp(meta->payloads[0].hash, hash, 32)) {
                    meta->d.err = -2;
                    ret = EBADF;
                } else {
                    /* now that we know its checksum is correct and we also cached it locally, we can extract */
                    if(!syspkg_payload_extract(ctx, meta, g ? tmp : meta->d.url, i + 1, dld)) {
                        dld += meta->payloads[0].comp;
                    } else {
                        meta->d.err = -3;
                        ret = EPERM;
                    }
                }
                if(g) syspkg_rm(tmp);
            } else {
                meta->d.err = -1;
                ret = ENOENT;
            }
        }

        /* we must configure the least dependent package first */
        for(j = 0; maxlvl >= 0; maxlvl--)
            for(i = 0; i < ctx->numinstalls; i++) {
                meta = (syspkg_meta_t*)ctx->installs[i];
                if(meta->d.err != maxlvl) continue;
                if(ctx->progressbar) { j++; (*ctx->progressbar)(j, ctx->numinstalls, j-1, ctx->numinstalls, SYSPKG_MSG_CFGPKG); }
                syspkg_postinst(meta);
            }
        /* decrease failed package dependencies' counters */
        for(i = 0; i < ctx->numinstalls; i++) {
            if(ctx->installs[i]->err >= 0) ctx->installs[i]->err = 0;
            else {
                j = syspkg_getinst(ctx, ctx->installs[i]->id, &blksiz);
                if(!blksiz) {
                    ctx->inst[j].iref = ctx->inst[j].iversion = 0;
                    for(k = 0; ctx->inst[j].depends && ctx->inst[j].depends[k]; k++) {
                        l = syspkg_getinst(ctx, ctx->inst[j].depends[k], &blksiz);
                        if(!blksiz && ctx->inst[l].iref > 1) ctx->inst[l].iref--;
                    }
                }
            }
        }
        if(ctx->progressbar)
            (*ctx->progressbar)(ctx->numinstalls, ctx->numinstalls, 1, ret == SUCCESS ? 1 : -1, SYSPKG_MSG_CFGPKG);
        if(ret == SUCCESS) {
            free(ctx->installs);
            ctx->installs = NULL;
            ctx->numinstalls = 0;
        }
    }

    if(ctx->removes) {
        /* remove packages */
        for(i = 0; i < ctx->numremoves; i++) {
            if(ctx->progressbar) (*ctx->progressbar)(0, 0, i+1, ctx->numremoves, SYSPKG_MSG_DELPKG);
            meta = (syspkg_meta_t*)ctx->removes[i];
            for(j = 0; j < OVERRIDELEN; j++) {
                /* strstr("%s") not good, that would match "%%s" too */
                c = strchr(syspkg_resolves[j], '%');
                while(c && *(c + 1) != 's') c = strchr(c + 2, '%');
                if(c && !meta->base) {
                    /* if file category mask looks like /opt/%s/bin/, remove /opt/%s */
                    k = c - syspkg_resolves[j];
                    memcpy(tmp, syspkg_resolves[j], k);
                    strcpy(tmp + k, meta->d.id);
                    syspkg_rmrf(tmp);
                } else {
                    /* if file category mask looks like /usr/bin/, remove files one by one */
                    if(!meta->files) syspkg_loadfiles(meta);
                    if(meta->files && meta->numfiles) {
                        for(k = 0; k < meta->numfiles; k++) {
                            if(memcmp(meta->files[k] + 8, syspkg_overrides[j], 3)) continue;
                            syspkg_payload_dest(meta, 0, meta->files[k] + 8, strlen(meta->files[k] + 8), (char*)&tmp, NULL);
                            syspkg_rm(tmp);
                        }
                    }
                }
            }
            if(meta->files) {
                for(j = 0; j < meta->numfiles && meta->files[j]; j++)
                    free(meta->files[j]);
                free(meta->files);
                meta->files = NULL;
            }
            meta->numfiles = 0;
        }
        free(ctx->removes);
        ctx->removes = NULL;
        ctx->numremoves = 0;
    }

    syspkg_saveinstalled(ctx);
    syspkg_unlock();
    return ret;
}

