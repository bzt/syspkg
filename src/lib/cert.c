/*
 * syspkg/src/lib/cert.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Certificate management
 *
 */

#include "internal.h"
#include <mbedtls/config.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/sha256.h>
#include <mbedtls/md.h>
#include <mbedtls/pk.h>
#include <mbedtls/bignum.h>
#include <time.h>

/* IMPORTANT: always zero out memory where we've loaded the private key */

/**
 * Generate certificate name
 */
void syspkg_getcn(char *dst, char *name, char *countrycode, int isrepo)
{
    char *c;
    if(!dst) return;
    *dst = 0;
    if(!name || !*name || !countrycode || !*countrycode) return;
    strcpy(dst, "CN="); dst += 3;
    for(c = name; *c && c < name + MAXCNSIZE; c++) {
        if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { c++; continue; } else
        if((unsigned char)*c < ' ' || *c == ',' || *c == '=') continue;
        *dst++ = *c;
    }
    strcpy(dst, ", C="); dst += 4;
    *dst++ = toupper(countrycode[0]);
    *dst++ = toupper(countrycode[1]);
    strcpy(dst, ", L="); dst += 4;
    strcpy(dst, isrepo ? "repository" : "maintainer");
}

/**
 * Generate key pair and certificate
 */
int syspkg_cert(syspkg_ctx_t *ctx, char *name, char *countrycode, int isrepo)
{
    unsigned char output_buf[8192], *buf = NULL, *buf2 = NULL;
    char *fn, cn[MAXCNSIZE+32], expire[24];
    int ret = SUCCESS;
    size_t len, len2;
    time_t t;
    struct tm *ts;
    mbedtls_pk_context pk;
    mbedtls_mpi N, P, Q, D, E, DP, DQ, QP;
    mbedtls_x509_crt crtin;
    mbedtls_x509_crt crtnew;
    mbedtls_x509write_cert crtout;
    mbedtls_mpi serial;
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;

    if(!ctx) return EINVAL;
    fn = malloc(strlen(ctx->cfgdir) + 16);
    if(!fn) return ENOMEM;

    mbedtls_pk_init( &pk );
    mbedtls_mpi_init( &serial );
    mbedtls_x509_crt_init( &crtin );
    mbedtls_x509_crt_init( &crtnew );
    mbedtls_x509write_crt_init( &crtout );
    mbedtls_entropy_init( &entropy );
    memset(output_buf, 0, sizeof(output_buf));
    mbedtls_ctr_drbg_init( &ctr_drbg );
    t = time(NULL);
    ts = gmtime(&t);
    sprintf(expire, "%04d1231235959", ts->tm_year + 2000);
    sprintf(cn,"salt%ld", t);
    mbedtls_entropy_add_source(&entropy, mbedtls_hardware_poll, NULL, 32, MBEDTLS_ENTROPY_SOURCE_STRONG);
    mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, (const unsigned char*)cn, strlen(cn));
    mbedtls_mpi_read_string( &serial, 10, cn + 4);
    if(ctx->cert) { free(ctx->cert); ctx->cert = NULL; }

    /* read cert if exists */
    sprintf(fn, "%scert", ctx->cfgdir);
    if(syspkg_readfileall(fn, &buf, &len) == 0 && buf && len && (ret = mbedtls_x509_crt_parse(&crtin, buf, len + 1)) == 0 &&
        mbedtls_x509_dn_gets((char*)output_buf, sizeof(cn), &crtin.subject) > 0) {
        /* if a new, signed cert is given, replace the current one */
        if(!isrepo && !countrycode && name && *name && syspkg_readfileall(name, &buf2, &len2) == 0 && buf2 && len2 &&
            mbedtls_x509_crt_parse(&crtnew, buf2, len2 + 1) == 0 &&
            mbedtls_x509_dn_gets((char*)output_buf + sizeof(cn), sizeof(cn), &crtnew.subject) > 0 &&
            mbedtls_x509_dn_gets((char*)output_buf + 2*sizeof(cn), sizeof(cn), &crtnew.issuer) > 0 &&
            !strcmp((char*)output_buf, (char*)output_buf + sizeof(cn)) &&
            strcmp((char*)output_buf + sizeof(cn), (char*)output_buf + 2*sizeof(cn))) {
                memcpy(output_buf, buf2, len2 + 1 < sizeof(output_buf) ? len2 + 1 : sizeof(output_buf));
                output_buf[sizeof(output_buf)-1] = 0;
                free(buf2); buf2 = NULL;
                goto wr;
        }
        if(buf2) { free(buf2); buf2 = NULL; }
        cn[0] = 0;
        if(name && *name && countrycode && *countrycode)
            syspkg_getcn(cn, name, countrycode, isrepo);
        sprintf(fn, "%skeys", ctx->cfgdir);
        if((!cn[0] || !strcmp((char*)output_buf, cn)) && syspkg_readfileall(fn, &buf2, &len2) == 0 && buf2 && len2 &&
            mbedtls_pk_parse_key(&pk, buf2, len2 + 1, NULL, 0, mbedtls_ctr_drbg_random, &ctr_drbg) == 0 &&
            mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(pk), mbedtls_pk_rsa(pk)) == 0 &&
            mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(crtin.pk), mbedtls_pk_rsa(pk)) == 0) {
            if(buf2) memset(buf2, 0, len2);
            memcpy(output_buf, buf, len + 1 < sizeof(output_buf) ? len + 1 : sizeof(output_buf));
            output_buf[sizeof(output_buf)-1] = 0;
            goto retstr;
        }
        if(buf2) memset(buf2, 0, len2);
    }
    if(!name || !*name || !countrycode || !*countrycode)
        { ret = EINVAL; goto end; }

    /* read the keys */
    sprintf(fn, "%skeys", ctx->cfgdir);
    if(buf2) free(buf2);
    if(syspkg_readfileall(fn, &buf2, &len2) || !buf2 || !len2 || mbedtls_pk_parse_key(&pk, buf2, len2 + 1, NULL, 0,
        mbedtls_ctr_drbg_random, &ctr_drbg) != 0 ||
        mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(pk), mbedtls_pk_rsa(pk)) != 0) {
        if(buf2) memset(buf2, 0, len2);
        /* generate the keys */
        if(ctx->progressbar) (*ctx->progressbar)(0, 0, 0, 1, SYSPKG_MSG_GENKEYS);
        mbedtls_mpi_init( &N ); mbedtls_mpi_init( &P ); mbedtls_mpi_init( &Q );
        mbedtls_mpi_init( &D ); mbedtls_mpi_init( &E ); mbedtls_mpi_init( &DP );
        mbedtls_mpi_init( &DQ ); mbedtls_mpi_init( &QP );
        if(mbedtls_pk_setup(&pk, mbedtls_pk_info_from_type(MBEDTLS_PK_RSA)) != 0 ||
            mbedtls_rsa_gen_key(mbedtls_pk_rsa(pk), mbedtls_ctr_drbg_random, &ctr_drbg, 4096, 65537) != 0) {
                if(ctx->progressbar) (*ctx->progressbar)(0, 0, 0, -1, SYSPKG_MSG_GENKEYS);
                ret = EFAULT; goto end;
        }
        if(ctx->progressbar) (*ctx->progressbar)(0, 0, 1, 1, SYSPKG_MSG_GENKEYS);
        memset(output_buf, 0, sizeof(output_buf));
        if((ret = mbedtls_pk_write_pubkey_pem(&pk, output_buf, sizeof(output_buf))) != 0)
            { ret = EFAULT; goto end; }
        len = strlen((char*)output_buf);
        if(mbedtls_pk_write_key_pem( &pk, output_buf + len, sizeof(output_buf) - len) != 0 )
            { ret = EFAULT; goto end; }
        if((ret = syspkg_writefileall(fn, output_buf, strlen((char*)output_buf)))) goto end;
    }
    if(buf2) memset(buf2, 0, len2);
    /* generate new certificate */
    if(ctx->progressbar) (*ctx->progressbar)(0, 0, 0, 1, SYSPKG_MSG_GENCERT);
    syspkg_getcn(cn, name, countrycode, isrepo);
    mbedtls_x509write_crt_set_subject_key( &crtout, &pk );
    mbedtls_x509write_crt_set_issuer_key( &crtout, &pk );
    mbedtls_x509write_crt_set_version( &crtout, MBEDTLS_X509_CRT_VERSION_3 );
    mbedtls_x509write_crt_set_md_alg( &crtout, MBEDTLS_MD_SHA256 );
    if( mbedtls_x509write_crt_set_subject_name( &crtout, cn) != 0 ||
        mbedtls_x509write_crt_set_issuer_name( &crtout, cn) != 0 ||
        mbedtls_x509write_crt_set_serial( &crtout, &serial) != 0 ||
        mbedtls_x509write_crt_set_validity( &crtout, "20210101000000", expire ) != 0 ||
        (isrepo && mbedtls_x509write_crt_set_basic_constraints( &crtout, 1, -1) != 0) ||
        mbedtls_x509write_crt_set_key_usage( &crtout,
            isrepo ? MBEDTLS_X509_KU_KEY_CERT_SIGN : MBEDTLS_X509_KU_DIGITAL_SIGNATURE) != 0 ||
        mbedtls_x509write_crt_pem( &crtout, output_buf, sizeof(output_buf), mbedtls_ctr_drbg_random, &ctr_drbg) != 0) {
            if(ctx->progressbar) (*ctx->progressbar)(0, 0, 0, -1, SYSPKG_MSG_GENCERT);
            ret = EFAULT; goto end;
    }
wr: sprintf(fn, "%scert", ctx->cfgdir);
    if((ret = syspkg_writefileall(fn, output_buf, strlen((char*)output_buf)))) goto end;
    if(ctx->progressbar) (*ctx->progressbar)(0, 0, 1, 1, SYSPKG_MSG_GENCERT);

retstr:
    if(output_buf[0]) {
        len = strlen((char*)output_buf);
        ctx->cert = malloc(len + 1);
        if(!ctx->cert) { ret = ENOMEM; goto end; }
        memcpy(ctx->cert, output_buf, len + 1);
    }
end:
    mbedtls_ctr_drbg_free( &ctr_drbg );
    mbedtls_entropy_free( &entropy );
    mbedtls_mpi_free( &serial );
    mbedtls_x509_crt_free( &crtin );
    mbedtls_x509_crt_free( &crtnew );
    mbedtls_x509write_crt_free( &crtout );
    mbedtls_pk_free( &pk );
    if(buf) free(buf);
    if(buf2) free(buf2);
    free(fn);
    memset(output_buf, 0, sizeof(output_buf));
    return ret;
}

/**
 * Sign a certificate
 */
int syspkg_sign(syspkg_ctx_t *ctx, char *crtfile)
{
    unsigned char output_buf[8192], hash[32], *buf = NULL, *buf2 = NULL, *pklist = NULL, *crl, *s;
    char *fn, cn[MAXCNSIZE+32], expire[24];
    int ret = SUCCESS;
    size_t len, len2, i;
    mbedtls_pk_context pk;
    mbedtls_x509_crt crtin;
    mbedtls_x509_crt tosign;
    mbedtls_sha256_context sha;
    mbedtls_x509write_cert crtout;
    mbedtls_mpi serial;
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;

    if(!ctx || !crtfile || !*crtfile) return EINVAL;
    fn = malloc(strlen(ctx->cfgdir) + 16);
    if(!fn) return ENOMEM;
    if(ctx->cert) { free(ctx->cert); ctx->cert = NULL; }

    mbedtls_pk_init( &pk );
    mbedtls_entropy_init( &entropy );
    mbedtls_ctr_drbg_init( &ctr_drbg );
    sprintf(cn,"salt%ld", time(NULL));
    mbedtls_entropy_add_source(&entropy, mbedtls_hardware_poll, NULL, 32, MBEDTLS_ENTROPY_SOURCE_STRONG);
    mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, (const unsigned char*)cn, strlen(cn));

    /* read the keys */
    sprintf(fn, "%skeys", ctx->cfgdir);
    if(syspkg_readfileall(fn, &buf, &len) != 0 || !buf || !len || mbedtls_pk_parse_key(&pk, buf, len + 1, NULL, 0,
        mbedtls_ctr_drbg_random, &ctr_drbg) != 0 ||
        mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(pk), mbedtls_pk_rsa(pk)) != 0)
        { if(buf) { memset(buf, 0, len); } ret = EFAULT; goto end; }
    memset(buf, 0, len);
    free(buf);
    /* read certs */
    mbedtls_mpi_init( &serial );
    mbedtls_x509_crt_init( &crtin );
    mbedtls_x509_crt_init( &tosign );
    mbedtls_x509write_crt_init( &crtout );
    sprintf(fn, "%scert", ctx->cfgdir);
    if(!(syspkg_readfileall(fn, &buf, &len) == 0 && buf && syspkg_readfileall(crtfile, &buf2, &len2) == 0 && buf2)) {
        ret = EINVAL;
    } else
    if(!(mbedtls_x509_crt_parse( &crtin, buf, len + 1) == 0 &&
        crtin.ca_istrue && (crtin.key_usage & MBEDTLS_X509_KU_KEY_CERT_SIGN) &&
        mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(crtin.pk), mbedtls_pk_rsa(pk)) == 0 &&
        mbedtls_x509_dn_gets((char*)output_buf, sizeof(cn), &crtin.subject) > 0)) {
        ret = EINVAL;
    } else
    if(!(mbedtls_x509_crt_parse( &tosign, buf2, len2 + 1) == 0 && !tosign.ca_istrue &&
        !(tosign.key_usage & MBEDTLS_X509_KU_KEY_CERT_SIGN) &&
        mbedtls_x509_dn_gets(cn, sizeof(cn), &tosign.subject) > 0)) {
        ret = EBADF;
    } else {
        /* we have a repository certificate, and the other one is a maintainer cert */
        mbedtls_x509write_crt_set_subject_key( &crtout, &tosign.pk );
        mbedtls_x509write_crt_set_issuer_key( &crtout, &pk );
        mbedtls_x509write_crt_set_version( &crtout, MBEDTLS_X509_CRT_VERSION_3 );
        mbedtls_x509write_crt_set_md_alg( &crtout, MBEDTLS_MD_SHA256 );
        mbedtls_mpi_read_binary( &serial, tosign.serial.p, tosign.serial.len);
        sprintf(expire, "%04d1231235959", tosign.valid_to.year);
        if( mbedtls_x509write_crt_set_subject_name( &crtout, cn) != 0 ||
            mbedtls_x509write_crt_set_issuer_name( &crtout, (char*)output_buf) != 0 ||
            mbedtls_x509write_crt_set_serial( &crtout, &serial) != 0 ||
            mbedtls_x509write_crt_set_validity( &crtout, "20210101000000", expire ) != 0 ||
            mbedtls_x509write_crt_set_key_usage( &crtout, MBEDTLS_X509_KU_DIGITAL_SIGNATURE) != 0 ||
            mbedtls_x509write_crt_pem( &crtout, output_buf, sizeof(output_buf), mbedtls_ctr_drbg_random, &ctr_drbg) != 0)
            { ret = EFAULT; goto end; }
        mbedtls_x509write_crt_free( &crtout );

        len = strlen((char*)output_buf);
        ctx->cert = malloc(len + 1);
        if(!ctx->cert) { ret = ENOMEM; goto end; }
        memcpy(ctx->cert, output_buf, len + 1);

        /* we ain't finished yet. If we have revoked this cert's public key earlier, remove the blocking */
        crl = (unsigned char*)strstr((char*)buf, PEM_BEG_CRL);
        if(crl) {
            mbedtls_sha256_init(&sha);
            mbedtls_sha256_starts(&sha, 0);
            mbedtls_sha256_update(&sha, (const unsigned char*)tosign.pk_raw.p, tosign.pk_raw.len);
            mbedtls_sha256_finish(&sha, hash);
            mbedtls_sha256_free(&sha);
            crl += 20;
            len2 = strlen((char*)crl);
            pklist = (unsigned char *)malloc(len2);
            if(buf2 && mbedtls_base64_decode(pklist, len2, &len2, crl, len2 - 18) == 0) {
                for(i = 0; i < len2 && memcmp(pklist + i, hash, 32); i += 32);
                if(i == len2) goto end;
                if(i + 32 < len2)
                    memmove(pklist + i, pklist + i + 32, len2 - i - 32);
                len2 -= 32;
                if(len2 > 0) {
                    buf2 = (unsigned char*)realloc(buf2, 2 * len2 + 2);
                    if(buf2 && mbedtls_base64_encode(buf2, 2 * len2 + 1, &len2, pklist, len2) == 0) {
                        for(crl--, i = 0, s = buf2; i < len2; i++) {
                            if(!(i % 64)) *crl++ = '\n';
                            *crl++ = *s++;
                        }
                        if(*(crl - 1) != '\n') *crl++ = '\n';
                        crl += sprintf((char*)crl, PEM_END_CRL "\n");
                    }
                } else {
                    crl -= 20; *crl = 0;
                }
                syspkg_writefileall(fn, buf, crl - buf);
            }
        }
    }
end:
    mbedtls_ctr_drbg_free( &ctr_drbg );
    mbedtls_entropy_free( &entropy );
    mbedtls_mpi_free( &serial );
    mbedtls_x509_crt_free( &tosign );
    mbedtls_x509_crt_free( &crtin );
    mbedtls_pk_free( &pk );
    free(fn);
    if(buf) free(buf);
    if(buf2) free(buf2);
    if(pklist) free(pklist);
    return ret;
}

/**
 * Revoke signed a certificate
 */
int syspkg_revoke(syspkg_ctx_t *ctx, char *crtfile)
{
    unsigned char *buf = NULL, *buf2 = NULL, *pklist = NULL, hash[32];
    unsigned int flags = 0;
    char *fn, *crl, *s, *d;
    int ret = SUCCESS;
    size_t len, len2, i;
    mbedtls_pk_context pk;
    mbedtls_x509_crt crtin;
    mbedtls_x509_crt torevoke;
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_sha256_context sha;

    if(!ctx || !crtfile || !*crtfile) return EINVAL;
    fn = malloc(strlen(ctx->cfgdir) + 16);
    if(!fn) return ENOMEM;
    if(ctx->cert) { free(ctx->cert); ctx->cert = NULL; }

    mbedtls_pk_init( &pk );
    mbedtls_entropy_init( &entropy );
    mbedtls_entropy_add_source(&entropy, mbedtls_hardware_poll, NULL, 32, MBEDTLS_ENTROPY_SOURCE_STRONG);
    mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, (const unsigned char*)"salt", 4);

    /* read the keys */
    sprintf(fn, "%skeys", ctx->cfgdir);
    if(syspkg_readfileall(fn, &buf, &len) != 0 || !buf || !len || mbedtls_pk_parse_key(&pk, buf, len + 1, NULL, 0,
        mbedtls_ctr_drbg_random, &ctr_drbg) != 0 ||
        mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(pk), mbedtls_pk_rsa(pk)) != 0)
        { if(buf) { memset(buf, 0, len); } ret = EFAULT; goto end; }
    memset(buf, 0, len);
    free(buf);
    /* read certs */
    mbedtls_x509_crt_init( &crtin );
    mbedtls_x509_crt_init( &torevoke );
    sprintf(fn, "%scert", ctx->cfgdir);
    if(!(syspkg_readfileall(fn, &buf, &len) == 0 && buf && syspkg_readfileall(crtfile, &buf2, &len2) == 0 && buf2)) {
        ret = EINVAL;
    } else
    if(!(mbedtls_x509_crt_parse( &crtin, buf, len + 1) == 0 && crtin.ca_istrue &&
        (crtin.key_usage & MBEDTLS_X509_KU_KEY_CERT_SIGN) &&
        mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(crtin.pk), mbedtls_pk_rsa(pk)) == 0)) {
        ret = EINVAL;
    } else
    if(!(mbedtls_x509_crt_parse( &torevoke, buf2, len2 + 1) == 0 && !torevoke.ca_istrue &&
        !(torevoke.key_usage & MBEDTLS_X509_KU_KEY_CERT_SIGN) &&
        mbedtls_x509_crt_verify(&torevoke, &crtin, NULL, NULL, &flags, NULL, NULL) == 0)) {
        ret = EBADF;
    } else {
        mbedtls_sha256_init(&sha);
        mbedtls_sha256_starts(&sha, 0);
        mbedtls_sha256_update(&sha, (const unsigned char*)torevoke.pk_raw.p, torevoke.pk_raw.len);
        mbedtls_sha256_finish(&sha, hash);
        mbedtls_sha256_free(&sha);
        /* if we already have a crl, read it in */
        crl = strstr((char*)buf, PEM_BEG_CRL);
        if(crl) {
            *crl = 0; len = crl - (char*)buf; crl += 20;
            len2 = strlen(crl);
            pklist = (unsigned char *)malloc(len2);
            if(!pklist) { ret = ENOMEM; goto end; }
            if(mbedtls_base64_decode(pklist, len2, &len2, (unsigned char*)crl, len2 - 18) == 0) {
                for(i = 0; i < len2 && memcmp(pklist + i, hash, 32); i += 32);
                if(i < len2) goto enc;
            } else len2 = 0;
        } else len2 = 0;
        pklist = (unsigned char*)realloc(pklist, len2 + 32);
        if(!pklist) { ret = ENOMEM; goto end; }
        memcpy(pklist + len2, hash, 32);
        len2 += 32;
enc:    buf2 = (unsigned char*)realloc(buf2, 2 * len2 + 1);
        if(!buf2) { ret = ENOMEM; goto end; }
        if(mbedtls_base64_encode( buf2, 2 * len2 + 1, &len2, pklist, len2) != 0) { ret = EFAULT; goto end; }
        ctx->cert = malloc(len + len2 + len2 / 64 + 20 + 18 + 2);
        if(!ctx->cert) { ret = ENOMEM; goto end; }
        memcpy(ctx->cert, buf, len);
        d = ctx->cert + len + sprintf(ctx->cert + len, PEM_BEG_CRL);
        for(i = 0, s = (char*)buf2; i < len2; i++) {
            if(!(i % 64)) *d++ = '\n';
            *d++ = *s++;
        }
        if(*(d - 1) != '\n') *d++ = '\n';
        d += sprintf(d, PEM_END_CRL "\n");
        ret = syspkg_writefileall(fn, (unsigned char *)ctx->cert, d - ctx->cert);
    }
end:
    mbedtls_ctr_drbg_free( &ctr_drbg );
    mbedtls_entropy_free( &entropy );
    mbedtls_x509_crt_free( &torevoke );
    mbedtls_x509_crt_free( &crtin );
    mbedtls_pk_free( &pk );
    free(fn);
    if(buf) free(buf);
    if(buf2) free(buf2);
    if(pklist) free(pklist);
    return ret;
}

/**
 * Add a certificate to the trusted list
 */
int syspkg_trust(syspkg_ctx_t *ctx, char *crtfile)
{
    unsigned char hash[32];
    char *s, *e, fn[MAXPATHSIZE], *pem = NULL;
    int ret = EINVAL;
    size_t l = 0, n;
    mbedtls_sha256_context sha;

    if(!syspkg_isadmin()) return EACCES;
    if(!ctx || !crtfile || !*crtfile) return EINVAL;
    if(syspkg_readfileall( crtfile, (unsigned char**)&pem, &l ) != 0 || l < 24) goto end;
    s = pem;
    if(*s == '{') {
        for(l = n = 0; *s; s++) {
            if(*s == '{') n++;
            if(*s == '}') { n--; if(!n) { s++; break; } }
        }
        if(n) goto end;
        if(*s == '\n') s++;
        if(memcmp(s, PEM_BEG_SIG, 27)) goto end;
        e = strchr(s + 24, '%');
        if(e) {
            memcpy(s, PEM_BEG_CRT, 27);
            strcpy(e, "\n" PEM_END_CRT "\n");
            l = e - s + 27;
        }
    } else
    if(memcmp(s, PEM_BEG_CRT, 27)) goto end;
    mbedtls_sha256_init(&sha);
    mbedtls_sha256_starts(&sha, 0);
    mbedtls_sha256_update(&sha, (const unsigned char*)s, l);
    mbedtls_sha256_finish(&sha, hash);
    mbedtls_sha256_free(&sha);
    e = fn + sprintf(fn, "%strusted" SEP, PKGDIR);
    syspkg_mkdir(fn, 0775);
    for(n = 0; n < 32; n++)
        e += sprintf(e, "%02x", hash[n]);
    ret = syspkg_writefileall(fn, (unsigned char*)s, l);
end:if(pem) free(pem);
    return ret;
}

/**
 * Remove a certificate from the truxted list
 */
static int _syspkg_untrust_ret;
void _syspkg_untrust(int type, int64_t size, uint64_t mtime, char *full, char *name, void *data)
{
    unsigned char *buf = NULL;
    char *cn = (char *)data, tmp[MAXCNSIZE+32];
    size_t l;
    mbedtls_x509_crt crtin;
    (void)size; (void)mtime; (void)name;

    if(type == SYSPKG_WALK_TYPE_REG && _syspkg_untrust_ret == ENOENT) {
        mbedtls_x509_crt_init( &crtin );
        if(syspkg_readfileall(full, &buf, &l) == 0 && buf && l && mbedtls_x509_crt_parse(&crtin, buf, l + 1) == 0 &&
            mbedtls_x509_dn_gets(tmp, sizeof(tmp), &crtin.subject) > 0 && !strcmp(tmp, cn) && !unlink(full))
                _syspkg_untrust_ret = SUCCESS;
        if(buf) free(buf);
        mbedtls_x509_crt_free( &crtin );
    }
}

int syspkg_untrust(syspkg_ctx_t *ctx, char *cn)
{
    unsigned char hash[32];
    char fn[MAXPATHSIZE], *e;
    int i;
    mbedtls_sha256_context sha;

    if(!syspkg_isadmin()) return EACCES;
    if(!ctx || !cn || !*cn) return EINVAL;
    /* if we got a certificate or signature */
    if(!memcmp(cn, PEM_BEG_SIG, 27)) {
        e = strchr(cn + 24, '%');
        if(e) {
            memcpy(cn, PEM_BEG_CRT, 27);
            strcpy(e, "\n" PEM_END_CRT "\n");
        }
    }
    if(!memcmp(cn, PEM_BEG_CRT, 27)) {
        mbedtls_sha256_init(&sha);
        mbedtls_sha256_starts(&sha, 0);
        mbedtls_sha256_update(&sha, (const unsigned char*)cn, strlen(cn));
        mbedtls_sha256_finish(&sha, hash);
        mbedtls_sha256_free(&sha);
        e = fn + sprintf(fn, "%strusted" SEP, PKGDIR);
        for(i = 0; i < 32; i++)
            e += sprintf(e, "%02x", hash[i]);
        return syspkg_rm(fn);
    }
    /* if we got a canonical name, read in certs and see which one's cn match */
    if(cn[0]!='C' || cn[1]!='N') return EINVAL;
    sprintf(fn, "%strusted" SEP, PKGDIR);
    _syspkg_untrust_ret = ENOENT;
    syspkg_walkdir(fn, 0, (void*)cn, NULL, _syspkg_untrust);
    return _syspkg_untrust_ret;
}

/**
 * Generate metainfo signature
 */
int syspkg_gensig(syspkg_ctx_t *ctx, char *buffer)
{
    unsigned char output_buf[8192], hash[32], sig[1024], *buf = NULL, *s, *d;
    char *fn;
    int ret = SUCCESS;
    size_t l, e, n;
    mbedtls_pk_context pk;
    mbedtls_x509_crt crtin;
    mbedtls_sha256_context sha;
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;

    if(!ctx || !buffer || *buffer != '{') return EINVAL;
    for(l = n = 0; buffer[l]; l++) {
        if(buffer[l] == '{') n++;
        if(buffer[l] == '}') { n--; if(!n) { l++; break; } }
    }
    if(n) return EINVAL;
    mbedtls_sha256_init(&sha);
    mbedtls_sha256_starts(&sha, 0);
    mbedtls_sha256_update(&sha, (const unsigned char*)buffer, l);
    mbedtls_sha256_finish(&sha, hash);
    mbedtls_sha256_free(&sha);
    mbedtls_entropy_init( &entropy );
    mbedtls_ctr_drbg_init( &ctr_drbg );
    mbedtls_entropy_add_source(&entropy, mbedtls_hardware_poll, NULL, 32, MBEDTLS_ENTROPY_SOURCE_STRONG);
    mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, (const unsigned char*)hash, sizeof(hash));
    l = n = 0;
    fn = malloc(strlen(ctx->cfgdir) + 16);
    if(!fn) return ENOMEM;
    if(ctx->cert) { free(ctx->cert); ctx->cert = NULL; }

    mbedtls_pk_init( &pk );

    /* read the keys */
    sprintf(fn, "%skeys", ctx->cfgdir);
    if(syspkg_readfileall(fn, &buf, &l) != 0 || !buf || !l || mbedtls_pk_parse_key(&pk, buf, l + 1, NULL, 0,
        mbedtls_ctr_drbg_random, &ctr_drbg) != 0 ||
        mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(pk), mbedtls_pk_rsa(pk)) != 0)
        { if(buf) { memset(buf, 0, l); } ret = EINVAL; goto end; }
    if(buf) { memset(buf, 0, l); free(buf); }
    memset(output_buf, 0, sizeof(output_buf));
    memset(sig, 0, sizeof(sig));
    /* read cert */
    mbedtls_x509_crt_init( &crtin );
    sprintf(fn, "%scert", ctx->cfgdir);
    if(syspkg_readfileall(fn, &buf, &l) == 0 && buf && l && mbedtls_x509_crt_parse(&crtin, buf, l + 1) == 0 &&
/* allow signing with repository certs too */
/*        !crtin.ca_istrue && (crtin.key_usage & MBEDTLS_X509_KU_DIGITAL_SIGNATURE) && */
        mbedtls_rsa_check_pub_priv(mbedtls_pk_rsa(crtin.pk), mbedtls_pk_rsa(pk)) == 0 &&
        mbedtls_pk_sign( &pk, MBEDTLS_MD_SHA256, hash, 32, sig, e, &e, mbedtls_ctr_drbg_random, &ctr_drbg ) == 0 && e &&
        mbedtls_base64_encode( output_buf, sizeof(output_buf), &n, sig, e) == 0) {
        ctx->cert = malloc(l + n + n/64 + 64);
        if(!ctx->cert) { ret = ENOMEM; goto end; }
        memcpy(ctx->cert, buf, l - 27);
        memcpy(ctx->cert, PEM_BEG_SIG, 27);
        d = (unsigned char*)ctx->cert + l - 27;
        for(l = 1; d[-(l+1)] != '\n'; l++);
        if(l > 63) { *d++ = '\n'; l = 0; }
        *d++ = '%'; l++;
        for(s = output_buf; s < output_buf + n; l++) {
            if(l == 64) { l = 0; *d++ = '\n'; }
            *d++ = *s++;
        }
        if(l) *d++ = '\n';
        strcpy((char*)d, PEM_END_SIG "\n");
    }

end:
    mbedtls_ctr_drbg_free( &ctr_drbg );
    mbedtls_entropy_free( &entropy );
    mbedtls_x509_crt_free( &crtin );
    mbedtls_pk_free( &pk );
    if(buf) free(buf);
    free(fn);
    return ret;
}

/**
 * Check the metainfo's signature
 */
int syspkg_chksig(mbedtls_x509_crt *repocrt, mbedtls_x509_buf *repocrl, char *buffer, char *cn)
{
    unsigned char hash[32], sig[1024], *buf = NULL;
    char *s, *e, *d, *fn, c[32];
    int ret = EFAULT, r;
    unsigned int flags = 0;
    size_t l, n, m;
    mbedtls_x509_crt crtin;
    mbedtls_sha256_context sha;

    if(!buffer || *buffer != '{') return EINVAL;
    for(l = n = 0; buffer[l]; l++) {
        if(buffer[l] == '{') n++;
        if(buffer[l] == '}') { n--; if(!n) { l++; break; } }
    }
    if(n) return EINVAL;
    mbedtls_sha256_init(&sha);
    mbedtls_sha256_starts(&sha, 0);
    mbedtls_sha256_update(&sha, (const unsigned char*)buffer, l);
    mbedtls_sha256_finish(&sha, hash);
    mbedtls_sha256_free(&sha);
    mbedtls_x509_crt_init( &crtin );
    if(buffer[l] == '\n') l++;
    s = buffer + l;
    if(!*s || memcmp(s, PEM_BEG_SIG, 27)) return EINVAL;
    e = strchr(s + 24, '%');
    if(e) {
        memcpy(s, PEM_BEG_CRT, 27);
        memcpy(c, e, 32);
        strcpy(e, "\n" PEM_END_CRT "\n");
        l = e - s + 27;
        if(mbedtls_x509_crt_parse(&crtin, (unsigned char *)s, l + 1) == 0) {
            if(cn) mbedtls_x509_dn_gets(cn, MAXCNSIZE+32, &crtin.subject);
            /* if we have a crl and the certificate's key is listed in it */
            if(repocrl && repocrl->p && repocrl->len) {
                mbedtls_sha256_init(&sha);
                mbedtls_sha256_starts(&sha, 0);
                mbedtls_sha256_update(&sha, (const unsigned char*)crtin.pk_raw.p, crtin.pk_raw.len);
                mbedtls_sha256_finish(&sha, hash);
                mbedtls_sha256_free(&sha);
                for(m = 0; m < repocrl->len && memcmp(repocrl->p + m, hash, 32); m += 32);
                if(m < n) { ret = EPERM; goto end; }
            }
            /* if the issuer is the repository owner, we're good */
            if(!repocrt || mbedtls_x509_crt_verify(&crtin, repocrt, NULL, NULL, &flags, NULL, NULL)) {
                /* otherwise for self-signed maintainer certs check if they are trusted locally */
                mbedtls_sha256_init(&sha);
                mbedtls_sha256_starts(&sha, 0);
                mbedtls_sha256_update(&sha, (const unsigned char*)s, l);
                mbedtls_sha256_finish(&sha, hash);
                mbedtls_sha256_free(&sha);
                fn = malloc(strlen(PKGDIR) + 80);
                if(fn) {
                    d = fn + sprintf(fn, "%strusted" SEP, PKGDIR);
                    for(n = 0; n < 32; n++)
                        d += sprintf(d, "%02x", hash[n]);
                    r = syspkg_readfileall( fn, &buf, &m );
                    if(r || m < l || !buf || !strstr((char*)buf, s)) r = 1;
                    if(buf) free(buf);
                    free(fn);
                    if(r) { ret = ENOENT; goto end; }
                }
            }
            memcpy(e, c, 32);
            ret = EPERM;
            if(mbedtls_base64_decode(sig, sizeof(sig), &n, (unsigned char*)e + 1, strlen(e + 1) - 27) == 0 && n &&
                mbedtls_pk_verify( &crtin.pk, MBEDTLS_MD_SHA256, hash, 0, sig, n ) == 0 )
                    ret = SUCCESS;
        }
    }
end:
    mbedtls_x509_crt_free( &crtin );
    return ret;
}
