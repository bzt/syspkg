/*
 * syspkg/src/lib/meta.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief IPackage meta information routines
 *
 */

#include "internal.h"

/**
 * Parse a size (number) parameter and check for validity
 */
size_t syspkg_validsize(char *s)
{
    size_t r = 0;
    if(s) { while(*s >= '0' && *s <= '9') { r *= 10; r += *s++ - '0'; } }
    return r;
}

/**
 * Parse a package id and check for validity
 */
int syspkg_validid(char *id)
{
    char *c;
    if(!id || !*id || *id == '_' || *id == '-' || *id == '.') return 0;
    for(c = id; *c; c++) {
        if(!((*c >= 'a' && *c <= 'z') || (*c >= 'A' && *c <= 'Z') || (*c >= '0' && *c <= '9') ||
            *c == '_' || *c == '-' || *c == '.'))
                return 0;
    }
    return c - id > 2 ? 1 : 0;
}

/**
 * Parse a canonized version and check for validity
 */
int syspkg_validver(char *ver, uint32_t *version)
{
    char *c, i = 0;
    if(!ver || !version) return 0;
    *version = ((unsigned char)syspkg_validsize(ver)) << 16;
    for(c = ver; *c && *c != '\t'; c++) {
        if((*c < '0' || *c > '9') && *c != '.') return 0;
        if(*c == '.') {
            c++; i++;
            if(i == 1) *version |= ((unsigned char)syspkg_validsize(c) << 8); else
            if(i == 2) *version |= (unsigned char)syspkg_validsize(c); else
                return 0;
        }
    }
    return (*version != 0) ? 1 : 0;
}

/**
 * Buffer allocating, json safe sprintf
 */
void syspkg_sprintf(char **buf, char **dst, size_t *n, char *fmt, ...)
{
    uint64_t arg;
    unsigned char e;
    int i;
    char *p, *c, *d, tmpstr[65];
    __builtin_va_list args;
    __builtin_va_start(args, fmt);

    if(!*buf) {
        *n = 4096;
        *buf = (char*)malloc(*n);
        if(!*buf) return;
        *dst = *buf;
    }
    d = *dst;
    while(*fmt) {
        if(*fmt == '%') {
            fmt++;
            if(*fmt=='%') goto put;
            if(*fmt == 'd') {
                arg = __builtin_va_arg(args, unsigned long int);
                if(arg > 9223372036854775807UL) arg = 9223372036854775807UL;
                i = 19;
                tmpstr[i] = 0;
                do {
                    tmpstr[--i] = '0' + (arg%10);
                    arg /= 10;
                } while(arg != 0 && i > 0);
                p = &tmpstr[i];
                goto copystring;
            } else
            if(*fmt == 'h') {
                p = __builtin_va_arg(args, char*);
                for(i = 0, c = tmpstr; i < 32; i++, p++) {
                    e = (unsigned char)*p >> 4; *c++ = e+(e>9?0x57:0x30);
                    e = (unsigned char)*p & 15; *c++ = e+(e>9?0x57:0x30);
                }
                *c = 0;
                p = tmpstr;
                goto copystring;
            } else
            if(*fmt == 's' || *fmt == 'q') {
                p = __builtin_va_arg(args, char*);
copystring:     if(p == NULL) p="(null)";
                while(*p) {
                    if((size_t)(d - *buf) + 3 > *n) {
                        *n <<= 1;
                        d -= (uintptr_t)*buf;
                        *buf = (char*)realloc(*buf, *n);
                        if(!*buf) return;
                        d += (uintptr_t)*buf;
                    }
                    if((unsigned char)*p == 0xC0 && (unsigned char)*(p + 1) == 0x80) { p += 2; } else
                    if(*fmt == 's' && *p == '\n') { *d++ = '\\'; *d++ = 'n'; p++; } else
                    if(*fmt == 'q' && *p == '\n') { *d++='<';*d++='b';*d++='r';*d++='/';*d++='>'; p++; } else
                    if(*fmt == 'q' && *p == '<') { *d++='&';*d++='l';*d++='t';*d++=';'; p++; } else
                    if(*fmt == 'q' && *p == '>') { *d++='&';*d++='g';*d++='t';*d++=';'; p++; } else
                    if(*fmt == 'q' && *p == '\"') { *d++='&';*d++='q';*d++='u';*d++='o';*d++='t';*d++=';'; p++; } else
                    if(*fmt == 'q' && *p == '\'') { *d++='&';*d++='a';*d++='c';*d++='u';*d++='t';*d++='e';*d++=';'; p++; } else
                    if((unsigned char)*p >= ' ') {
                        if(*fmt == 's' && *p == '\"') *d++ = '\\';
                        *d++ = *p++;
                    } else
                        p++;
                }
            } else
                fmt++;
        } else {
put:        if((size_t)(d - *buf) + 2 > *n) {
                *n <<= 1;
                d -= (uintptr_t)*buf;
                *buf = (char*)realloc(*buf, *n);
                if(!*buf) return;
                d += (uintptr_t)*buf;
            }
            *d++ = *fmt;
        }
        fmt++;
    }
    *d = 0;
    *dst = d;
}

/**
 * Parse json into in-memory struct
 */
syspkg_meta_t *syspkg_readmeta(syspkg_ctx_t *ctx, char *jsonstr, int filter, int *err)
{
    syspkg_meta_t *meta;
    char *tmp1, *tmp2, *tmp3, *tmp4, *c, key[36], *fp;
    int i, j, k, l, m, t;
    if(!ctx || !jsonstr || !*jsonstr || !err) return NULL;
    *err = 0;
    /* cut off signature if there's any */
    for(l = k = 0; jsonstr[l]; l++) {
        if(jsonstr[l] == '{') k++;
        if(jsonstr[l] == '}') { k--; if(!k) { l++; break; } }
    }
    if(k) return NULL;
    jsonstr[l] = 0;
    /* files is the last key, and it can be very long. Speed up non-matching key
     * searches by terminating the json string before that long file list */
    fp = syspkg_json(jsonstr, "files", -1);
    if(fp) *(fp - 1) = 0;
    /* parse json */
    meta = (syspkg_meta_t*)malloc(sizeof(syspkg_meta_t));
    if(!meta) { *err = ENOMEM; return NULL; }
    memset(meta, 0, sizeof(syspkg_meta_t));
    /* mandatory fields, and ones that might fail comes first */
    meta->d.id = syspkg_json(jsonstr, "id", MAXIDSIZE);
    if(!syspkg_validid(meta->d.id)) {
        syspkg_freemeta(meta);
        *err = (SYSPKG_ERR_ID << 16) | (EBADF & 0xFFFF);
        return NULL;
    }
    meta->base = (strlen(meta->d.id) > 3 && !memcmp(meta->d.id, "base", 4) && (meta->d.id[4] == 0 || meta->d.id[4] == '.'));
    meta->d.license = syspkg_json(jsonstr, "license", MAXLICSIZE);
    for(c = meta->d.license; c && *c; c++)
        if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
    if(filter == SYSPKG_FILTER_STD && ctx->license && ctx->license[0] && meta->d.license) {
        for(i = k = 0; ctx->license[i]; i++)
            if(!memcmp(meta->d.license, ctx->license[i], strlen(ctx->license[i]))) { k = 1; break; }
        if(!k) {
            syspkg_freemeta(meta);
            return NULL;
        }
    }
    tmp1 = syspkg_json(jsonstr, "version", MAXVERSIZE);
    if(!syspkg_validver(tmp1, &meta->d.version)) {
        if(tmp1) free(tmp1);
        syspkg_freemeta(meta);
        *err = (SYSPKG_ERR_VER << 16) | (EBADF & 0xFFFF);
        return NULL;
    }
    if(tmp1) free(tmp1);
    meta->d.url = syspkg_json(jsonstr, "url", MAXURLSIZE);
    if(meta->d.url) {
        if((memcmp(meta->d.url, "https://", 8) && memcmp(meta->d.url, "file://", 7))) {
            syspkg_freemeta(meta);
            *err = (SYSPKG_ERR_URL << 16) | (EBADF & 0xFFFF);
            return NULL;
        }
        for(c = meta->d.url; *c; c++)
            if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
    }
    meta->d.category = syspkg_json(jsonstr, "category", MAXCATSIZE);
    if(!meta->d.category) {
caterr:  syspkg_freemeta(meta);
        *err = (SYSPKG_ERR_CAT << 16) | (EBADF & 0xFFFF);
        return NULL;
    }
    for(c = meta->d.category; *c; c++)
        if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
    for(i = k = 0; syspkg_validcats[i]; i++)
        if(!strcmp(meta->d.category, syspkg_validcats[i])) { k = 1; break; }
    if(!k) goto caterr;
    l = ctx->lang ? strlen(ctx->lang) : 0;
    for(t = 0; t < 3; t++) {
        for(i = k = 0, tmp1 = tmp2 = tmp3 = ""; tmp1 && tmp2 && tmp3; i++) {
            sprintf(key, "description.%d.0", i); tmp1 = syspkg_json(jsonstr, key, 5);
            if(tmp1) {
                for(c = tmp1; *c; c++)
                    if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
                if(filter == SYSPKG_FILTER_STD) {
                    if(t == 2) {
                        if(memcmp(tmp1, "en", 2))
                            { free(tmp1); continue; }
                    } else {
                        if(!t) j = strlen(tmp1);
                        if(ctx->lang && memcmp(tmp1, ctx->lang, t == 1 ? 2 : (j < l ? j : l)))
                            { free(tmp1); continue; }
                    }
                }
            }
            sprintf(key, "description.%d.1", i); tmp2 = syspkg_json(jsonstr, key, MAXIDSIZE);
            sprintf(key, "description.%d.2", i); tmp3 = syspkg_json(jsonstr, key, MAXDSCSIZE);
            meta->desc = (syspkg_desc_t*)realloc(meta->desc, (k + 1) * sizeof(syspkg_desc_t));
            if(!meta->desc) {
memerr:         *err = ENOMEM;
                syspkg_freemeta(meta);
                return NULL;
            }
            if(tmp1 && *tmp1 && tmp2 && *tmp2 && tmp3 && *tmp3) {
                for(c = tmp2; *c; c++) {
                    if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { *c++ = '_'; *c = '_'; } else
                    if((unsigned char)*c < ' ') *c = '_';
                }
                for(c = tmp3; *c; c++) {
                    if((unsigned char)*c < ' ' && *c != '\n') *c = ' '; else
                    if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { *c++ = '_'; *c = '_'; } else
                    if(*c == '\\' && *(c + 1) == 'n') { *c = '\r'; *(c + 1) = '\n'; } else
                    if(*c == '\\' && *(c + 1) == '\"') { memcpy(c, c + 1, strlen(c)); }
                }
                meta->desc[k].lang = tmp1;
                meta->desc[k].name = tmp2;
                meta->desc[k].desc = tmp3;
                k++;
            } else {
                if(tmp1) free(tmp1);
                meta->desc[k].lang = NULL;
                if(tmp2) free(tmp2);
                meta->desc[k].name = NULL;
                if(tmp3) free(tmp3);
                meta->desc[k].desc = NULL;
                break;
            }
        }
        if(!meta->desc || !meta->desc[0].lang || !meta->desc[0].lang[0] ||
            !meta->desc[0].name || !meta->desc[0].name[0] ||
            !meta->desc[0].desc || !meta->desc[0].desc[0]) {
                if(t == 2) {
                    syspkg_freemeta(meta);
                    *err = (SYSPKG_ERR_DESC << 16) | (EBADF & 0xFFFF);
                    return NULL;
                }
        } else
            break;
    }
    meta->d.name = meta->desc[0].name;
    meta->d.desc = meta->desc[0].desc;
    if(filter != SYSPKG_FILTER_NOPAYLOAD) {
        for(l = 0; filter == SYSPKG_FILTER_STD ? ctx->arch[l] != NULL : (l < 1); l++) {
            for(i = k = 0, tmp1 = tmp2 = tmp3 = tmp4 = ""; tmp1 && tmp2 && tmp3 && tmp4; i++) {
                sprintf(key, "payloads.%d.0", i); tmp1 = syspkg_json(jsonstr, key, MAXARCSIZE);
                if(tmp1) {
                    for(c = tmp1; *c; c++)
                        if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
                    for(j = 0; syspkg_validarchs[j]; j++)
                        if(!strcmp(tmp1, syspkg_validarchs[j])) break;
                    if(!syspkg_validarchs[j] || (filter == SYSPKG_FILTER_STD && memcmp(tmp1, "any", 4) &&
                        memcmp(tmp1, ctx->arch[l], strlen(ctx->arch[l])))) {
                        free(tmp1);
                        continue;
                    }
                }
                sprintf(key, "payloads.%d.1", i); tmp2 = syspkg_json(jsonstr, key, 21);
                sprintf(key, "payloads.%d.2", i); tmp3 = syspkg_json(jsonstr, key, 21);
                sprintf(key, "payloads.%d.3", i); tmp4 = syspkg_json(jsonstr, key, 64);
                meta->payloads = (syspkg_payload_t*)realloc(meta->payloads, (k + 2) * sizeof(syspkg_payload_t));
                if(!meta->payloads) goto memerr;
                memset(&meta->payloads[k], 0, 2*sizeof(syspkg_payload_t));
                if(tmp1 && *tmp1 && tmp2 && *tmp2 && tmp3 && *tmp3 && tmp4 && *tmp4) {
                    meta->payloads[k].arch = tmp1;
                    meta->payloads[k].comp = syspkg_validsize(tmp2); free(tmp2);
                    meta->payloads[k].orig = syspkg_validsize(tmp3); free(tmp3);
                    for(j = 0, c = tmp4; j < 32 && *c; j++, c++) {
                        meta->payloads[k].hash[j] = (
                            *c >= '0' && *c <= '9' ? *c - '0' : (
                            *c >= 'a' && *c <= 'f' ? *c - 'a' + 10 : (
                            *c >= 'A' && *c <= 'F' ? *c - 'A' + 10 : 0
                            ))) << 4;
                        c++; if(!*c) break;
                        meta->payloads[k].hash[j] += (
                            *c >= '0' && *c <= '9' ? *c - '0' : (
                            *c >= 'a' && *c <= 'f' ? *c - 'a' + 10 : (
                            *c >= 'A' && *c <= 'F' ? *c - 'A' + 10 : 0
                            )));
                    }
                    free(tmp4);
                    k++;
                    if(filter == SYSPKG_FILTER_STD) goto plok;
                } else {
                    if(tmp1) free(tmp1);
                    meta->payloads[k].arch = NULL;
                    if(tmp2) free(tmp2);
                    if(tmp3) free(tmp3);
                    if(tmp4) free(tmp4);
                    break;
                }
            }
        }
plok:   if(fp) {
            /* the file list can be very long, so use a specialized and very optimized parser */
            for(i = 0, c = fp; *c && *c != ']'; c++)
                if(*c == '}') i++;
            meta->files = (char**)realloc(meta->files, (i + 2) * sizeof(char*));
            if(!meta->files) goto memerr;
            for(i = 0, tmp1 = fp; *tmp1 && *tmp1 != ']'; i++) {
                for(tmp2 = tmp1; *tmp2 && *tmp2 != '\"'; tmp2++);
                for(tmp3 = tmp2; *tmp3 && *tmp3 != '}'; tmp3++);
                tmp4 = tmp2 + MAXPATHSIZE < tmp3 ? tmp2 + MAXPATHSIZE : tmp3;
                meta->files[i] = (char*)malloc(tmp4 - tmp2 + 9);
                if(!meta->files[i]) goto memerr;
                *((uint64_t*)meta->files[i]) = syspkg_validsize(tmp1);
                for(c = tmp2 + 1, tmp1 = meta->files[i] + 8; c < tmp4 && *c != '\"' && *c != '}'; c++) {
                    if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { c++; continue; } else
                    if((unsigned char)*c < ' ') *tmp1++ = '_'; else
                    if(*c == '\\') { c++; *tmp1++ = *c; } else *tmp1++ = *c;
                }
                *tmp1 = 0;
                for(tmp1 = tmp3; *tmp1 && *tmp1 != ']' && (*tmp1 < '0' || *tmp1 > '9'); tmp1++);
            }
            meta->files[i] = NULL;
            meta->numfiles = i;
        }
        /* if we have url or files but no payloads, that's an error */
        if((meta->d.url || meta->numfiles) && (!meta->payloads || !meta->payloads[0].arch || !meta->payloads[0].arch[0] ||
            !meta->payloads[0].comp)) {
plerr:          syspkg_freemeta(meta);
                *err = (SYSPKG_ERR_PAYLOAD << 16) | (EBADF & 0xFFFF);
                return NULL;
        }
    }
    /* if we have payloads but no url, that's an error too */
    if(meta->payloads && meta->payloads[0].arch && (!meta->d.url || !*meta->d.url)) {
        syspkg_freemeta(meta);
        *err = (SYSPKG_ERR_URL << 16) | (EBADF & 0xFFFF);
        return NULL;
    }
    /* optional, or fields that cannot fail afterwards */
    for(i = k = 0, tmp1 = ""; tmp1; i++) {
        sprintf(key, "depends.%d", i); tmp1 = syspkg_json(jsonstr, key, MAXIDSIZE + MAXVERSIZE + 1);
        meta->d.depends = (syspkg_dep_t*)realloc(meta->d.depends, (k + 1) * sizeof(syspkg_dep_t));
        if(!meta->d.depends) goto memerr;
        if(tmp1 && *tmp1) {
            meta->d.depends[k].id = tmp1;
            meta->d.depends[k].version = 0;
            for(c = tmp1; *c && *c != ' '; c++)
                if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
            if(*c == ' ') {
                while(*c == ' ') *c++ = 0;
                syspkg_validver(c, &meta->d.depends[k].version);
            }
            k++;
        } else {
            if(tmp1) free(tmp1);
            meta->d.depends[k].id = NULL;
            break;
        }
    }
    /* Meta packages have no files and no payloads but they must have a depends array */
    if(!k && filter != SYSPKG_FILTER_NOPAYLOAD && !meta->numfiles) goto plerr;
    for(i = k = 0, tmp1 = ""; tmp1; i++) {
        sprintf(key, "suggests.%d", i); tmp1 = syspkg_json(jsonstr, key, MAXIDSIZE + MAXVERSIZE + 1);
        meta->d.suggests = (syspkg_dep_t*)realloc(meta->d.suggests, (k + 1) * sizeof(syspkg_dep_t));
        if(!meta->d.suggests) goto memerr;
        if(tmp1 && *tmp1) {
            meta->d.suggests[k].id = tmp1;
            meta->d.suggests[k].version = 0;
            for(c = tmp1; *c && *c != ' '; c++)
                if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
            if(*c == ' ') {
                while(*c == ' ') *c++ = 0;
                syspkg_validver(c, &meta->d.suggests[k].version);
            }
            k++;
        } else {
            if(tmp1) free(tmp1);
            meta->d.suggests[k].id = NULL;
            break;
        }
    }
    for(i = k = 0, tmp1 = ""; tmp1; i++) {
        sprintf(key, "conflicts.%d", i); tmp1 = syspkg_json(jsonstr, key, MAXIDSIZE + MAXVERSIZE + 1);
        meta->d.conflicts = (syspkg_dep_t*)realloc(meta->d.conflicts, (k + 1) * sizeof(syspkg_dep_t));
        if(!meta->d.conflicts) goto memerr;
        if(tmp1 && *tmp1) {
            meta->d.conflicts[k].id = tmp1;
            meta->d.conflicts[k].version = 0;
            for(c = tmp1; *c && *c != ' '; c++)
                if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
            if(*c == ' ') {
                while(*c == ' ') *c++ = 0;
                syspkg_validver(c, &meta->d.conflicts[k].version);
            }
            k++;
        } else {
            if(tmp1) free(tmp1);
            meta->d.conflicts[k].id = NULL;
            break;
        }
    }
    meta->d.release = syspkg_json(jsonstr, "release", MAXRELSIZE);
    for(c = meta->d.release; c && *c; c++)
        if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
    tmp2 = meta->desc && meta->desc[0].lang ? meta->desc[0].lang : "en";
    for(i = 0, tmp3 = ""; tmp1; i++) {
        sprintf(key, "screenshots.%d", i);
        meta->screenshots = (char**)realloc(meta->screenshots, (i + 1) * sizeof(char*));
        if(!meta->screenshots) goto memerr;
        meta->screenshots[i] = tmp3 = syspkg_json(jsonstr, key, MAXURLSIZE);
        for(c = tmp3, tmp1 = tmp3; c && *c; c++) {
            if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
            if(filter == SYSPKG_FILTER_STD) {
                if(*c == '$' && c[1] == 'L' && c[2] == 'A' && c[3] == 'N' && c[4] == 'G') {
                    *tmp1++ = tmp2[0]; *tmp1 = tmp2[1];
                    c += 4;
                } else
                if(tmp1 != c) *tmp1 = *c;
            }
        }
        if(tmp1) *tmp1 = 0;
    }
    meta->eula = syspkg_json(jsonstr, "eula", MAXURLSIZE);
    for(c = meta->eula, tmp1 = meta->eula; c && *c; c++, tmp1++) {
        if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
        if(filter == SYSPKG_FILTER_STD) {
            if(*c == '$' && c[1] == 'L' && c[2] == 'A' && c[3] == 'N' && c[4] == 'G') {
                *tmp1++ = tmp2[0]; *tmp1 = tmp2[1];
                c += 4;
            } else
            if(tmp1 != c) *tmp1 = *c;
        }
    }
    if(tmp1) *tmp1 = 0;
    meta->d.homepage = syspkg_json(jsonstr, "homepage", MAXURLSIZE);
    for(c = meta->d.homepage, tmp1 = meta->d.homepage; c && *c; c++, tmp1++) {
        if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
        if(filter == SYSPKG_FILTER_STD) {
            if(*c == '$' && c[1] == 'L' && c[2] == 'A' && c[3] == 'N' && c[4] == 'G') {
                *tmp1++ = tmp2[0]; *tmp1 = tmp2[1];
                c += 4;
            } else
            if(tmp1 != c) *tmp1 = *c;
        }
    }
    if(tmp1) *tmp1 = 0;
    meta->d.bugtracker = syspkg_json(jsonstr, "bugtracker", MAXURLSIZE);
    for(c = meta->d.bugtracker, tmp1 = meta->d.bugtracker; c && *c; c++, tmp1++) {
        if((unsigned char)*c <= ' ' || (unsigned char)*c > 127) *c = '_';
        if(filter == SYSPKG_FILTER_STD) {
            if(*c == '$' && c[1] == 'L' && c[2] == 'A' && c[3] == 'N' && c[4] == 'G') {
                *tmp1++ = tmp2[0]; *tmp1 = tmp2[1];
                c += 4;
            } else
            if(tmp1 != c) *tmp1 = *c;
        }
    }
    if(tmp1) *tmp1 = 0;
    for(i = 0; i < OVERRIDELEN; i++) {
        sprintf(key, "override.%s", syspkg_overrides[i]);
        meta->over[i] = c = syspkg_json(jsonstr, key, MAXURLSIZE);
        for(meta->ovrl[i] = 0; c && *c; c++, meta->ovrl[i]++) {
            if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { *c++ = '_'; *c = '_'; } else
            if((unsigned char)*c < ' ') *c = '_';
        }
    }
    if(meta->d.url && *meta->d.url) {
        for(i = 0, tmp1 = ""; tmp1 && i < 8; i++) {
            sprintf(key, "postinst.commands.%d", i);
            meta->commands = (char**)realloc(meta->commands, (i + 1) * sizeof(char*));
            if(!meta->commands) goto memerr;
            meta->commands[i] = tmp1 = syspkg_json(jsonstr, key, MAXURLSIZE);
            for(c = tmp1; c && *c; c++)
                if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
        }
        /* only parse environment form if there were postinst commands */
        if(meta->commands) {
            for(m = 0; m < 32; m++) {
                sprintf(key, "postinst.env.%d.name", m); tmp1 = syspkg_json(jsonstr, key, MAXENVSIZE);
                sprintf(key, "postinst.env.%d.type", m); tmp2 = syspkg_json(jsonstr, key, MAXTYPSIZE);
                if(tmp1 && *tmp1 && tmp2 && *tmp2) {
                    for(c = tmp1; *c; c++)
                        if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
                    for(c = tmp2; *c; c++) {
                        if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { *c++ = '_'; *c = '_'; } else
                        if((unsigned char)*c < ' ') *c = '_';
                    }
                    meta->env = (syspkg_env_t*)realloc(meta->env, (m + 2) * sizeof(syspkg_env_t));
                    if(!meta->env) goto memerr;
                    meta->env[m+1].name = NULL;
                    meta->env[m].name = tmp1;
                    meta->env[m].type = tmp2;
                    meta->env[m].desc = NULL;
                    meta->env[m].envp = NULL;
                    l = ctx->lang ? strlen(ctx->lang) : 0;
                    for(t = 0; t < 3; t++) {
                        for(i = k = 0, tmp1 = tmp2 = tmp3 = ""; tmp1 && tmp2 && tmp3; i++) {
                            sprintf(key, "postinst.env.%d.desc.%d.0", m, i); tmp1 = syspkg_json(jsonstr, key, 5);
                            if(tmp1) {
                                for(c = tmp1; *c; c++)
                                    if((unsigned char)*c < ' ' || (unsigned char)*c > 127) *c = '_';
                                if(filter == SYSPKG_FILTER_STD) {
                                    if(t == 2) {
                                        if(memcmp(tmp1, "en", 2))
                                            { free(tmp1); continue; }
                                    } else {
                                        if(!t) j = strlen(tmp1);
                                        if(ctx->lang && memcmp(tmp1, ctx->lang, t == 1 ? 2 : (j < l ? j : l)))
                                            { free(tmp1); continue; }
                                    }
                                }
                            }
                            sprintf(key, "postinst.env.%d.desc.%d.1", m, i); tmp2 = syspkg_json(jsonstr, key, MAXLBLSIZE);
                            sprintf(key, "postinst.env.%d.desc.%d.2", m, i); tmp3 = syspkg_json(jsonstr, key, MAXHNTSIZE);
                            meta->env[m].desc = (syspkg_desc_t*)realloc(meta->env[m].desc, (k + 2) * sizeof(syspkg_desc_t));
                            if(!meta->env[m].desc) goto memerr;
                            if(tmp1 && *tmp1 && tmp2 && *tmp2 && tmp3 && *tmp3) {
                                for(c = tmp2; *c; c++) {
                                    if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { *c++ = '_'; *c = '_'; }
                                    else if((unsigned char)*c < ' ') *c = '_';
                                }
                                for(c = tmp3; *c; c++) {
                                    if((unsigned char)*c == 0xC0 && (unsigned char)*(c + 1) == 0x80) { *c++ = '_'; *c = '_'; }
                                    else if((unsigned char)*c < ' ') *c = '_';
                                }
                                meta->env[m].desc[k].lang = tmp1;
                                meta->env[m].desc[k].name = tmp2;
                                meta->env[m].desc[k].desc = tmp3;
                                k++;
                            } else {
                                if(tmp1) free(tmp1);
                                meta->env[m].desc[k].lang = tmp1 = NULL;
                                if(tmp2) free(tmp2);
                                meta->env[m].desc[k].name = tmp2 = NULL;
                                if(tmp3) free(tmp3);
                                meta->env[m].desc[k].desc = tmp3 = NULL;
                                break;
                            }
                        }
                        if(meta->env[m].desc[0].lang) break;
                    }
                } else {
                    if(tmp1) free(tmp1);
                    if(tmp2) free(tmp2);
                    break;
                }
            }
        }
    }
    return meta;
}

/**
 * Free an in-memory json struct
 */
int syspkg_freemeta(syspkg_meta_t *meta)
{
    int i, j;
    if(!meta) return EINVAL;
    if(meta->d.id) free(meta->d.id);
    if(meta->d.license) free(meta->d.license);
    if(meta->d.url) free(meta->d.url);
    if(meta->d.category) free(meta->d.category);
    if(meta->d.release) free(meta->d.release);
    if(meta->d.irelease) free(meta->d.irelease);
    if(meta->eula) free(meta->eula);
    if(meta->d.homepage) free(meta->d.homepage);
    if(meta->d.bugtracker) free(meta->d.bugtracker);
    if(meta->d.maintainer) free(meta->d.maintainer);
    if(meta->d.depends) {
        for(i = 0; meta->d.depends[i].id; i++)
            free(meta->d.depends[i].id);
        free(meta->d.depends);
    }
    if(meta->d.suggests) {
        for(i = 0; meta->d.suggests[i].id; i++)
            free(meta->d.suggests[i].id);
        free(meta->d.suggests);
    }
    if(meta->d.conflicts) {
        for(i = 0; meta->d.conflicts[i].id; i++)
            free(meta->d.conflicts[i].id);
        free(meta->d.conflicts);
    }
    /* do not free meta->d.name and meta->d.desc, they are just pointers to meta->desc[] records */
    if(meta->desc) {
        for(i = 0; meta->desc[i].lang; i++) {
            free(meta->desc[i].lang);
            free(meta->desc[i].name);
            free(meta->desc[i].desc);
        }
        free(meta->desc);
    }
    if(meta->payloads) {
        for(i = 0; meta->payloads[i].arch; i++)
            free(meta->payloads[i].arch);
        free(meta->payloads);
    }
    if(meta->files) {
        for(i = 0; i < meta->numfiles && meta->files[i]; i++)
            free(meta->files[i]);
        free(meta->files);
    }
    if(meta->screenshots) {
        for(i = 0; meta->screenshots[i]; i++)
            free(meta->screenshots[i]);
        free(meta->screenshots);
    }
    for(i = 0; i < OVERRIDELEN; i++)
        if(meta->over[i]) free(meta->over[i]);
    if(meta->commands) {
        for(i = 0; meta->commands[i]; i++)
            free(meta->commands[i]);
        free(meta->commands);
    }
    if(meta->env) {
        for(i = 0; meta->env[i].name; i++) {
            free(meta->env[i].name);
            free(meta->env[i].type);
            if(meta->env[i].envp) free(meta->env[i].envp);
            if(meta->env[i].desc) {
                for(j = 0; meta->env[i].desc[j].lang; j++) {
                    free(meta->env[i].desc[j].lang);
                    free(meta->env[i].desc[j].name);
                    free(meta->env[i].desc[j].desc);
                }
                free(meta->env[i].desc);
            }
        }
        free(meta->env);
    }
    memset(meta, 0, sizeof(syspkg_meta_t));
    free(meta);
    return SUCCESS;
}

/**
 * Generate json string from an in-memory struct
 */
char *syspkg_genjson(syspkg_meta_t *meta)
{
    char *buf = NULL, *dst = NULL;
    size_t n = 0;
    int i, j;
    if(!meta) return NULL;
    syspkg_sprintf(&buf, &dst, &n, "{\n  \"id\": \"%s\",\n  \"description\": [\n", meta->d.id);
    if(meta->desc)
        for(i = 0; meta->desc[i].lang; i++)
            syspkg_sprintf(&buf, &dst, &n, "    { \"%s\", \"%s\", \"%s\" }%s\n", meta->desc[i].lang, meta->desc[i].name,
                meta->desc[i].desc, meta->desc[i+1].lang ? "," : "");
    syspkg_sprintf(&buf, &dst, &n, "  ],\n  \"version\": \"%d.%d.%d\",\n", (meta->d.version >> 16) & 255,
        (meta->d.version >> 8) & 255, meta->d.version & 255);
    if(meta->d.release)
        syspkg_sprintf(&buf, &dst, &n, "  \"release\": \"%s\",\n", meta->d.release);
    if(meta->d.license)
        syspkg_sprintf(&buf, &dst, &n, "  \"license\": \"%s\",\n", meta->d.license);
    if(meta->d.category)
        syspkg_sprintf(&buf, &dst, &n, "  \"category\": \"%s\",\n", meta->d.category);
    if(meta->d.url)
        syspkg_sprintf(&buf, &dst, &n, "  \"url\": \"%s\",\n", meta->d.url);
    if(meta->eula)
        syspkg_sprintf(&buf, &dst, &n, "  \"eula\": \"%s\",\n", meta->eula);
    if(meta->d.homepage)
        syspkg_sprintf(&buf, &dst, &n, "  \"homepage\": \"%s\",\n", meta->d.homepage);
    if(meta->d.bugtracker)
        syspkg_sprintf(&buf, &dst, &n, "  \"bugtracker\": \"%s\",\n", meta->d.bugtracker);
    if(meta->d.depends && meta->d.depends[0].id) {
        syspkg_sprintf(&buf, &dst, &n, "  \"depends\": [");
        for(i = 0; meta->d.depends[i].id; i++) {
            if(meta->d.depends[i].version > 0)
                syspkg_sprintf(&buf, &dst, &n, "%s \"%s %d.%d.%d\"", i ? "," : "", meta->d.depends[i].id,
                    (meta->d.depends[i].version >> 16)&255, (meta->d.depends[i].version >> 8)&255, meta->d.depends[i].version&255);
            else
                syspkg_sprintf(&buf, &dst, &n, "%s \"%s\"", i ? "," : "", meta->d.depends[i].id);
        }
        syspkg_sprintf(&buf, &dst, &n, " ],\n");
    }
    if(meta->d.suggests && meta->d.suggests[0].id) {
        syspkg_sprintf(&buf, &dst, &n, "  \"suggests\": [");
        for(i = 0; meta->d.suggests[i].id; i++) {
            if(meta->d.suggests[i].version > 0)
                syspkg_sprintf(&buf, &dst, &n, "%s \"%s %d.%d.%d\"", i ? "," : "", meta->d.suggests[i].id,
                    (meta->d.suggests[i].version >> 16)&255, (meta->d.suggests[i].version >> 8)&255, meta->d.suggests[i].version&255);
            else
                syspkg_sprintf(&buf, &dst, &n, "%s \"%s\"", i ? "," : "", meta->d.suggests[i].id);
        }
        syspkg_sprintf(&buf, &dst, &n, " ],\n");
    }
    if(meta->d.conflicts && meta->d.conflicts[0].id) {
        syspkg_sprintf(&buf, &dst, &n, "  \"conflicts\": [");
        for(i = 0; meta->d.conflicts[i].id; i++) {
            if(meta->d.conflicts[i].version > 0)
                syspkg_sprintf(&buf, &dst, &n, "%s \"%s %d.%d.%d\"", i ? "," : "", meta->d.conflicts[i].id,
                    (meta->d.conflicts[i].version >> 16)&255, (meta->d.conflicts[i].version>>8)&255, meta->d.conflicts[i].version&255);
            else
                syspkg_sprintf(&buf, &dst, &n, "%s \"%s\"", i ? "," : "", meta->d.conflicts[i].id);
        }
        syspkg_sprintf(&buf, &dst, &n, " ],\n");
    }
    if(meta->screenshots && meta->screenshots[0]) {
        syspkg_sprintf(&buf, &dst, &n, "  \"screenshots\": [\n");
        for(i = 0; meta->screenshots[i]; i++)
            syspkg_sprintf(&buf, &dst, &n, "    \"%s\"%s\n", meta->screenshots[i], meta->screenshots[i + 1] ? "," : "");
        syspkg_sprintf(&buf, &dst, &n, "  ],\n");
    }
    for(j = -1, i = 0; i < OVERRIDELEN; i++)
        if(meta->over[i]) j = i;
    if(j != -1) {
        syspkg_sprintf(&buf, &dst, &n, "  \"overrides\": {\n");
        for(i = 0; i < OVERRIDELEN; i++)
            if(meta->over[i])
                syspkg_sprintf(&buf, &dst, &n, "    \"%s\": \"%s\"%s\n", i != j ? "," : "");
        syspkg_sprintf(&buf, &dst, &n, "  },\n");
    }
    if(meta->commands) {
        syspkg_sprintf(&buf, &dst, &n, "  \"postinst\": {\n");
        if(meta->env) {
            syspkg_sprintf(&buf, &dst, &n, "    \"env\": [\n");
            for(i = 0; meta->env[i].name; i++) {
                syspkg_sprintf(&buf, &dst, &n, "      {\n        \"name\": \"%s\",\n        \"type\": \"%s\",\n"
                            "        \"desc\": [\n", meta->env[i].name, meta->env[i].type);
                if(meta->env[i].desc)
                    for(j = 0; meta->env[i].desc[j].lang; j++)
                        syspkg_sprintf(&buf, &dst, &n, "          { \"%s\", \"%s\", \"%s\" }%s\n", meta->env[i].desc[j].lang,
                            meta->env[i].desc[j].name, meta->env[i].desc[j].desc, meta->env[i].desc[j+1].lang ? "," : "");
                syspkg_sprintf(&buf, &dst, &n, "        ]\n      }%s\n", meta->env[i+1].name ? "," : "");
            }
            syspkg_sprintf(&buf, &dst, &n, "    ]\n");
        }
        syspkg_sprintf(&buf, &dst, &n, "    \"commands\": [\n");
        for(i = 0; meta->commands[i]; i++)
            syspkg_sprintf(&buf, &dst, &n, "      \"%s\"%s\n", meta->commands[i], meta->commands[i + 1] ? "," : "");
        syspkg_sprintf(&buf, &dst, &n, "    ]\n");
        syspkg_sprintf(&buf, &dst, &n, "  },\n");
    }
    if(meta->payloads && meta->payloads[0].arch && meta->files && meta->files[0]) {
        syspkg_sprintf(&buf, &dst, &n, "  \"payloads\": [\n");
        if(meta->payloads)
            for(i = 0; meta->payloads[i].arch; i++)
                syspkg_sprintf(&buf, &dst, &n, "    { \"%s\", %d, %d, \"%h\" }%s\n", meta->payloads[i].arch,
                    meta->payloads[i].comp, meta->payloads[i].orig, &meta->payloads[i].hash,
                    meta->payloads[i + 1].arch ? "," : "");
        syspkg_sprintf(&buf, &dst, &n, "  ],\n  \"files\": [\n");
        if(meta->files)
            for(i = 0; i < meta->numfiles && meta->files[i]; i++)
                syspkg_sprintf(&buf, &dst, &n, "    { %d, \"%s\" }%s\n", *((int64_t*)meta->files[i]),
                    meta->files[i] + 8, meta->files[i + 1] ? "," : "");
        syspkg_sprintf(&buf, &dst, &n, "  ]");
    } else dst -= 2;
    syspkg_sprintf(&buf, &dst, &n, "\n}\n");
    return buf;
}

/**
 * Add a filename to meta in a sorted, unique array
 */
void syspkg_addfnmeta(syspkg_meta_t *meta, uint64_t size, char *fn, int len)
{
    int i=0, j=0, k=0, l, n;
    if(len < 1) len = strlen(fn);
    if(len > MAXPATHSIZE) len = MAXPATHSIZE;
    if(meta->numfiles) {
        l = 33;
        j = meta->numfiles - 1;
        while(l--) {
            k = i + ((j-i) >> 1);
            n = memcmp(meta->files[k] + 8, fn, len);
            if(!n && (int)strlen(meta->files[k] + 8) == len) {
                if(*((uint64_t*)meta->files[k]) < size) *((uint64_t*)meta->files[k]) = size;
                return;
            }
            if(i >= j) break;
            if(n < 0) i = k + 1; else j = k;
        }
        if(n < 0) k++;
    }
    meta->files = (char**)realloc(meta->files, (meta->numfiles + 2) * sizeof(char*));
    if(!meta->files) return;
    if(k < meta->numfiles)
        memmove(&meta->files[k+1], &meta->files[k], (meta->numfiles - k) * sizeof(char*));
    meta->files[k] = (char*)malloc(len + 9);
    *((uint64_t*)meta->files[k]) = size;
    memcpy(meta->files[k] + 8, fn, len);
    meta->files[k][len + 8] = 0;
    meta->numfiles++;
    meta->files[meta->numfiles] = NULL;
}

/**
 * Make a package, create (or check) payloads, generate and sign json
 */
int syspkg_build(syspkg_ctx_t *ctx, char *jsonfile, int numpl, char **plspec)
{
    syspkg_build_t build;
    unsigned char hash[32];
    char *jsonstr, *c, *e, *fn, ver[MAXVERSIZE], tmp[512+MAXPATHSIZE];
    int ret = SUCCESS, i, j, k, l, port, isdir, pl = 0;
    uint64_t next, nend, pend, len, lst;
    zip_local_t *loc = (zip_local_t*)&tmp;
    zip_ext64_t *ext;
    syspkg_meta_t *meta = NULL;

    /* read old json */
    if(!ctx || !jsonfile || !*jsonfile || (numpl > 0 && !plspec)) return EINVAL;
    if(syspkg_readfileall(jsonfile, (unsigned char **)&jsonstr, (size_t*)&len)) return ENOENT;
    /* if it's a repository list */
    if(!memcmp(jsonstr, "https://", 8) || !memcmp(jsonstr, "file:///", 8)) {
        syspkg_cert(ctx, NULL, NULL, 0);
        if(!ctx->cert || !*ctx->cert) { free(jsonstr); return EFAULT; }
        c = strstr(jsonstr, PEM_BEG_CRT);
        if(!c) c = jsonstr + len;
        c -= (uintptr_t)jsonstr;
        jsonstr = (char*)realloc(jsonstr, (uintptr_t)c + strlen(ctx->cert) + 3);
        if(!jsonstr) return ENOMEM;
 /* gcc thinks that "ptr is used after free", well, gcc is simply wrong. */
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wuse-after-free"
#endif
        c += (uintptr_t)jsonstr;
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
        if(*(c - 1) != '\n') *c++ = '\n';
        strcpy(c, ctx->cert);
        if(!memcmp(jsonfile, "https://", 8)) {
            printf("%s", jsonstr);
            ret = SUCCESS;
        } else
            ret = syspkg_writefileall(jsonfile, (unsigned char*)jsonstr, strlen(jsonstr));
#if HTMLCATALOG
        if(numpl && plspec && plspec[0])
            syspkg_html(ctx, jsonfile, jsonstr, plspec[0], plspec + 1);
#endif
        free(jsonstr);
        return ret;
    }
    if(!(meta = syspkg_readmeta(ctx, jsonstr, SYSPKG_FILTER_NOPAYLOAD, &ret)))
        { free(jsonstr); return ret; }
    free(jsonstr);

    /* generate payloads and update meta data */
    sprintf(ver, "%d.%d.%d", (meta->d.version >> 16)&255, (meta->d.version >> 8)&255, meta->d.version&255);
    if(meta->d.url && (numpl < 1 || !plspec || !plspec[0])) {
        for(numpl = 0; syspkg_validarchs[numpl]; numpl++);
        plspec = (char **)syspkg_validarchs;
    }
    meta->payloads = (syspkg_payload_t*)realloc(meta->payloads, (numpl + 1) * sizeof(syspkg_payload_t));
    if(!meta->payloads) return ENOMEM;
    memset(meta->payloads, 0, (numpl + 1) * sizeof(syspkg_payload_t));
    meta->numfiles = 0;
    /* for each payload spec "(arch)=(payload)" */
    for(i = 0; i < numpl; i++) {
        if(ctx->progressbar) (*ctx->progressbar)(i + 1, numpl, 0, 1, SYSPKG_MSG_CHKPAYLOAD);
        /* does it start with a valid architecture? */
        for(c = plspec[i], l = 0; *c && *c != '='; c++, l++);
        for(j = 0; syspkg_validarchs[j]; j++)
            if((int)strlen(syspkg_validarchs[j]) == l && !memcmp(plspec[i], syspkg_validarchs[j], l)) break;
        if(!syspkg_validarchs[j]) {
inval:      if(ctx->progressbar) (*ctx->progressbar)(i + 1, numpl, 0, -1, SYSPKG_MSG_CHKPAYLOAD);
            syspkg_freemeta(meta);
            return (i << 16) | (EINVAL & 0xFFFF);
        }
        /* has this arch been specified before? */
        for(k = 0; k < pl; k++)
            if(meta->payloads[k].arch && !strcmp(meta->payloads[k].arch, syspkg_validarchs[j])) goto inval;
        /* does it have a filename specified? */
        if(*c == '=') {
            fn = c + 1;
            isdir = syspkg_isdir(fn);
        } else {
            /* canonize URL and replace variables in it */
            syspkg_url(meta->d.url, (char*)syspkg_validarchs[j], ver, meta->d.release, ctx->osver, (char*)&tmp);
            fn = tmp;
            isdir = -1;
        }
        if(!fn) continue;
        build.meta = meta;
        /* get the file. Generate it if the given spec was a directory */
        mbedtls_sha256_init(&build.sha);
        mbedtls_sha256_starts(&build.sha, 0);
        build.comp = build.orig = build.pos = 0;
        if(isdir != 1) {
            /* local tarball or URL, only check and calculate SHA checksum */
            build.f = syspkg_open(fn, 0);
            if(!build.f) {
                if(isdir != -1) {
errpl:              if(ctx->progressbar) (*ctx->progressbar)(i + 1, numpl, 0, -1, SYSPKG_MSG_CHKPAYLOAD);
                    syspkg_freemeta(meta);
                    return (i << 16) | (EACCES & 0xFFFF);
                } else
                    continue;
            }
            /* in zip all directory separators are '/' */
            if(SEP[0] != '/') {
                for(k = 0; k < OVERRIDELEN; k++) {
                    if(meta->over[k]) {
                        for(c = meta->over[k]; *c; c++)
                            if(*c == SEP[0]) *c = '/';
                    }
                }
            }
            /* read in the file, locate zip local headers */
            next = 0; nend = sizeof(zip_local_t); port = 0;
            while(!syspkg_read(build.f)) {
                if(!next && *((uint32_t*)build.f->buf) != ZIP_LOCAL_MAGIC) {
                    syspkg_close(build.f);
                    goto errpl;
                }
                /* this is problematic, because we don't know how big chunks we can read, header could be cut in half */
                pend = build.f->pos + build.f->olen; lst = -1;
                while(next != (uint64_t)-1 && next < pend) {
                    if(lst == nend) break;
                    lst = nend;
                    if(next < build.f->pos) {
                        memcpy(tmp + build.f->pos-next, build.f->buf, build.f->olen > sizeof(tmp) ? sizeof(tmp) : build.f->olen);
                    } else {
                        memcpy(tmp, build.f->buf + next-build.f->pos, (pend < nend ? pend : nend) - next);
                    }
                    if(pend <= nend) break;
                    if(nend < pend) {
                        if(nend - next == sizeof(zip_local_t)) {
                            /* we got the entire first part, now expand it with the filename and extensions */
                            nend = next + sizeof(zip_local_t) + loc->fnlen + loc->extlen;
                            if(pend <= nend) break;
                        } else if(loc->magic == ZIP_LOCAL_MAGIC) {
                            /* we got the entire header, parse it */
                            nend = loc->comp;
                            len = loc->orig;
                            if(nend == 0xffffffff || pend == 0xffffffff) {
                                e = tmp + sizeof(zip_local_t) + loc->fnlen + loc->extlen;
                                if(e > tmp + sizeof(tmp)) e = tmp + sizeof(tmp);
                                for(c = tmp + sizeof(zip_local_t) + loc->fnlen; c < e; c += *((uint16_t*)(c + 2)) + 4)
                                    if(*((uint16_t*)c) == ZIP_EXTZIP64_MAGIC) {
                                        ext = (zip_ext64_t *)c;
                                        nend = ext->comp;
                                        len = ext->orig;
                                        break;
                                    }
                            }
                            /* check if filename is under an allowed path */
                            if(meta->base)
                                k = 0;
                            else
                                for(k = 0; k < OVERRIDELEN; k++) {
                                    if(meta->over[k]) { e = meta->over[k]; l = meta->ovrl[k]; }
                                    else { e = (char*)syspkg_ploaddirs[k]; l = syspkg_lenpldirs[k]; }
                                    if(!memcmp(tmp + sizeof(zip_local_t) + port, e, l) &&
                                        tmp[sizeof(zip_local_t) + port + l] == '/') break;
                                }
                            if(k == OVERRIDELEN) {
                                /* for the first entry if it's a directory, set it as top level dir */
                                if(!next && tmp[sizeof(zip_local_t) + loc->fnlen - 1] == '/') port = loc->fnlen;
                            } else if(tmp[sizeof(zip_local_t) + loc->fnlen - 1] != '/') {
                                syspkg_addfnmeta(meta, len, tmp + sizeof(zip_local_t) + port, loc->fnlen - port);
                                build.orig += len;
                            }
                            next += sizeof(zip_local_t) + loc->fnlen + loc->extlen + nend;
                            nend = next + sizeof(zip_local_t);
                            loc->magic = 0;
                        } else
                            next = (uint64_t)-1;
                    }
                }
                if(ctx->progressbar) (*ctx->progressbar)(i + 1, numpl, build.f->pos, build.f->filesize + 1, SYSPKG_MSG_CHKPAYLOAD);
                mbedtls_sha256_update(&build.sha, (const unsigned char*)build.f->buf, build.f->olen);
                build.f->pos += build.f->olen;
                build.f->olen = 0;
            }
            build.comp = build.f->pos;
            syspkg_close(build.f);
            /* convert back directory separators to native */
            if(SEP[0] != '/') {
                for(k = 0; k < OVERRIDELEN; k++) {
                    if(meta->over[k]) {
                        for(c = meta->over[k]; *c; c++)
                            if(*c == '/') *c = SEP[0];
                    }
                }
            }
        } else {
            /* directory, generate local tarball too */
            sprintf(tmp, "%s-%s-%s.zip", meta->d.id, ver, syspkg_validarchs[j]);
            build.f = syspkg_open(tmp, 1);
            if(!build.f) goto errpl;
            /* count number and sizes of files in directory */
            build.idx = 0;
            build.pl = i + 1;
            build.numpl = numpl;
            build.meta = meta;
            build.progressbar = ctx->progressbar;
            syspkg_walkdir(fn, 0, &build, syspkg_payload_count, NULL);
            /* go 2nd round, collect zip central directory and generate payload */
            if(!build.idx || !(build.central = (zip_inmem_t*)malloc(build.idx*sizeof(zip_inmem_t)))) {
                syspkg_close(build.f);
                goto errpl;
            }
            memset(build.central, 0, build.idx*sizeof(zip_inmem_t));
            build.idx = build.pos = 0;
            syspkg_walkdir(fn, 0, &build, syspkg_payload_add, NULL);
            /* write out zip catalog and oecd */
            syspkg_payload_close(&build);
            syspkg_close(build.f);
            free(build.central);
        }
        mbedtls_sha256_finish(&build.sha, hash);
        mbedtls_sha256_free(&build.sha);
        if(!build.comp || !build.orig) goto errpl;
        /* add payload info to meta */
        meta->payloads[pl].arch = (char*)malloc(MAXARCSIZE + 1);
        if(meta->payloads[pl].arch) {
            strcpy(meta->payloads[pl].arch, syspkg_validarchs[j]);
            meta->payloads[pl].comp = build.comp;
            meta->payloads[pl].orig = build.orig;
            memcpy(meta->payloads[pl].hash, hash, 32);
            pl++;
        }
    }
    /* failsafe, make sure there is at least one payload or one depends record in meta */
    if((meta->d.url || !meta->d.depends || !meta->d.depends[0].id) && (!pl || !meta->payloads || !meta->payloads[0].arch ||
        !meta->payloads[0].arch[0] || !meta->payloads[0].comp || !meta->payloads[0].orig)) {
            if(ctx->progressbar) (*ctx->progressbar)(numpl, numpl, 0, -1, SYSPKG_MSG_CHKPAYLOAD);
            syspkg_freemeta(meta);
            return EIO;
    }
    if(ctx->progressbar) (*ctx->progressbar)(numpl, numpl, 1, 1, SYSPKG_MSG_CHKPAYLOAD);

    /* generate new, signed json */
    jsonstr = syspkg_genjson(meta);
    syspkg_freemeta(meta);
    if(!jsonstr) return ENOMEM;
    if(syspkg_gensig(ctx, jsonstr)) { free(jsonstr); return EFAULT; }
    l = strlen(jsonstr);
    jsonstr = realloc(jsonstr, l + strlen(ctx->cert) + 1);
    if(!jsonstr) return EFAULT;
    strcpy(jsonstr + l, ctx->cert);
    ret = SUCCESS;
    /* replace the old file with the new signed one */
    if(syspkg_writefileall(jsonfile, (unsigned char *)jsonstr, strlen(jsonstr))) ret = EPERM;
    free(jsonstr);
    return ret;
}

/**
 * Check a metajson or payload for validity
 */
int syspkg_check(syspkg_ctx_t *ctx, char *url)
{
    unsigned char hash[32], *s, *c, *e, tmp[512+1024+MAXPATHSIZE];
    char *str, *o;
    int i, ret = SUCCESS, skip = 0, first = 1, report = 1;
    unsigned long int numloc=0, numcnt=0;
    uint64_t l, n=0, next, nend, pend, len, orig=0, offs, lst;
    zip_local_t *loc = (zip_local_t*)&tmp;
    zip_central_t *cnt = (zip_central_t*)&tmp;
    zip_ext64_t *ext;
    zip_inmem_t *dir = NULL;
    syspkg_meta_t *meta = NULL;
    syspkg_file_t *f;
    mbedtls_sha256_context sha;
    mbedtls_x509_crt crtin;

    if(!ctx || !url || !*url) return EINVAL;
    f = syspkg_open(url, 0);
    if(!f) return ENOENT;
    if(ctx->cert) free(ctx->cert);
    str = ctx->cert = malloc(65536);
    if(!ctx->cert) { syspkg_close(f); return ENOMEM; }
    memset(ctx->cert, 0, 65536);
    mbedtls_sha256_init(&sha);
    mbedtls_sha256_starts(&sha, 0);
    memset(tmp, 0, sizeof(tmp));

    /* read in the file, locate zip local headers */
    next = 0; nend = sizeof(zip_local_t) + 2;
    while(!syspkg_read(f)) {
        if(!next) {
            if(*((uint32_t*)f->buf) == ZIP_LOCAL_MAGIC) {
                str += sprintf(ctx->cert, "application/zip\n");
            } else
            if(f->buf[0] == '{') {
                do {
                    syspkg_read(f);
                } while(f->olen);
                str += sprintf(ctx->cert, "text/metajson\n");
                for(l = n = 0; f->buf[l]; l++) {
                    if(f->buf[l] == '{') n++;
                    if(f->buf[l] == '}') { n--; if(!n) { l++; break; } }
                }
                if(n) { syspkg_close(f); return EINVAL; }
                mbedtls_sha256_update(&sha, (const unsigned char*)f->buf, l);
                mbedtls_sha256_finish(&sha, hash);
                mbedtls_sha256_free(&sha);
                mbedtls_x509_crt_init( &crtin );
                if(f->buf[l] == '\n') l++;
                if(memcmp(f->buf + l, PEM_BEG_SIG, 27)) { ret = (SYSPKG_ERR_SIGNATURE << 16) | (EBADF & 0xFFFF); goto err; }
                s = f->buf + l;
                e = (unsigned char*)strchr((char*)s + 24, '%');
                if(e) {
                    memcpy(s, PEM_BEG_CRT, 27);
                    memcpy(tmp + 512, e, 32);
                    strcpy((char*)e, "\n" PEM_END_CRT "\n");
                    l = e - s + 27;
                    if(mbedtls_x509_crt_parse(&crtin, (unsigned char *)s, l + 1) == 0 &&
                        mbedtls_x509_dn_gets((char*)tmp + 256, 256, &crtin.issuer) > 0 &&
                        mbedtls_x509_dn_gets((char*)tmp, 256, &crtin.subject) > 0) {
                            str += sprintf(str, "Signature:  ");
                            for(i = 0; i < 32; i++)
                                str += sprintf(str, "%02x", hash[i]);
                        memcpy(e, tmp + 512, 32);
                        if(mbedtls_base64_decode(tmp + 512, 1024, (size_t*)&n, (unsigned char*)e + 1, strlen((char*)e + 1) - 27)
                            == 0 && n && mbedtls_pk_verify( &crtin.pk, MBEDTLS_MD_SHA256, hash, 0, tmp + 512, n ) == 0 )
                            str += sprintf(str, " OK\n");
                        else
                            str += sprintf(str, " ERROR\n");
                        str += sprintf(str, "Repository: %s\nMaintainer: %s\n", tmp + 256, tmp);
                    } else { ret = (SYSPKG_ERR_SIGNATURE << 16) | (EBADF & 0xFFFF); goto err; }
                } else { ret = (SYSPKG_ERR_SIGNATURE << 16) | (EBADF & 0xFFFF); goto err; }
                if(!(meta = syspkg_readmeta(ctx, (char*)f->buf, SYSPKG_FILTER_NONE, &ret))) goto err;
                str += sprintf(str, "Package:    %s %d.%d.%d\n", meta->d.id, (meta->d.version >> 16)&255, (meta->d.version >> 8)&255,
                    meta->d.version&255);
                for(i = 0, orig = 0; i < meta->numfiles && meta->files[i]; i++)
                    orig += *((uint64_t*)meta->files[i]);
                str += sprintf(str, "Original:   %ld bytes, %d files\n",
#ifdef __WIN32__
                    (unsigned long int)
#endif
                    orig, meta->numfiles);
                syspkg_freemeta(meta);
                mbedtls_x509_crt_free( &crtin );
                goto err;
            } else {
                ret = EACCES;
                goto err;
            }
        }
        pend = f->pos + f->olen; lst = -1;
        while(next != (uint64_t)-1 && next < pend) {
            if(lst == nend) break;
            lst = nend;
            if(next < f->pos) {
                memcpy(tmp + f->pos-next, f->buf, f->olen > sizeof(tmp) ? sizeof(tmp) : f->olen);
            } else {
                memcpy(tmp, f->buf + next-f->pos, (pend < nend ? pend : nend) - next);
            }
            if(pend <= nend) break;
            if(nend < pend) {
                if(nend - next == sizeof(zip_local_t) + 2) {
                    if(loc->magic == ZIP_LOCAL_MAGIC)
                        nend = next + sizeof(zip_local_t) + loc->fnlen + loc->extlen;
                    else if(cnt->magic == ZIP_CENTRAL_MAGIC)
                        nend = next + sizeof(zip_central_t) + cnt->fnlen + cnt->extlen;
                    if(pend <= nend) break;
                } else if(loc->magic == ZIP_LOCAL_MAGIC) {
                    if(loc->flags & 8) {
                        str += sprintf(str, "Unsupported flags in local header #%ld\n", numloc + 1);
                        ret = EINVAL;
                        goto err;
                    }
                    if(loc->method != ZIP_DATA_STORE && loc->method != ZIP_DATA_DEFLATE && loc->method != ZIP_DATA_ZSTD) {
                        str += sprintf(str, "Unsupported compression method in local header #%ld\n", numloc + 1);
                        ret = EINVAL;
                        goto err;
                    }
                    if(loc->fnlen >= MAXPATHSIZE) {
                        str += sprintf(str, "Path too long in local header #%ld (max %d)\n", numloc + 1, MAXPATHSIZE);
                        ret = EINVAL;
                        goto err;
                    }
                    for(i = 0; i < OVERRIDELEN; i++) {
                        o = (char*)syspkg_ploaddirs[i]; l = syspkg_lenpldirs[i];
                        if(!memcmp(tmp + sizeof(zip_local_t) + skip, o, l) && tmp[sizeof(zip_local_t) + l + skip] == '/') break;
                    }
                    if(i == OVERRIDELEN) {
                        if(first && tmp[sizeof(zip_local_t) + loc->fnlen - 1] == '/') skip = loc->fnlen;
                        else if(report) {
                            i = skip + loc->fnlen;
                            if(i > 255) i = 255;
                            report = tmp[sizeof(zip_local_t) + i];
                            tmp[sizeof(zip_local_t) + i] = 0;
                            str += sprintf(str, "Unsupported path in local header #%ld: %s%s\n", numloc + 1,
                                tmp + sizeof(zip_local_t), i < skip + loc->fnlen ? "..." : "");
                            tmp[sizeof(zip_local_t) + i] = report;
                            report = 0;
                        }
                        first = 0;
                    }
                    dir = (zip_inmem_t*)realloc(dir, (numloc + 1) * sizeof(zip_inmem_t));
                    if(!dir) { syspkg_close(f); return ENOMEM; }
                    dir[numloc].fn = (char*)malloc(loc->fnlen);
                    if(!dir[numloc].fn) { syspkg_close(f); return ENOMEM; }
                    memcpy(dir[numloc].fn, tmp + sizeof(zip_local_t), loc->fnlen);
                    dir[numloc].fnlen = loc->fnlen;
                    dir[numloc].extlen = loc->extlen;
                    dir[numloc].crc32 = loc->crc32;
                    dir[numloc].method = loc->method;
                    dir[numloc].comp = loc->comp;
                    dir[numloc].orig = loc->orig;
                    dir[numloc].offs = next;
                    if(loc->comp == 0xffffffff || loc->orig == 0xffffffff) {
                        e = tmp + sizeof(zip_local_t) + loc->fnlen + loc->extlen;
                        if(e > tmp + sizeof(tmp)) e = tmp + sizeof(tmp);
                        for(c = tmp + sizeof(zip_local_t) + loc->fnlen; c < e; c += *((uint16_t*)(c + 2)) + 4)
                            if(*((uint16_t*)c) == ZIP_EXTZIP64_MAGIC) {
                                ext = (zip_ext64_t *)c;
                                dir[numloc].comp = ext->comp;
                                dir[numloc].orig = ext->orig;
                                break;
                            }
                    }
                    orig += dir[numloc].orig;
                    next += sizeof(zip_local_t) + loc->fnlen + loc->extlen + dir[numloc].comp;
                    nend = next + sizeof(zip_local_t) + 2;
                    loc->magic = 0;
                    numloc++;
                } else if(loc->magic == ZIP_SIGNATURE_MAGIC) {
                    next += sizeof(zip_sign_t) + ((zip_sign_t*)tmp)->size;
                    nend = next + sizeof(zip_local_t) + 2;
                    loc->magic = 0;
                } else if(loc->magic == ZIP_EXTDATA_MAGIC) {
                    next += sizeof(zip_extdata_t) + ((zip_extdata_t*)tmp)->size;
                    nend = next + sizeof(zip_local_t) + 2;
                    loc->magic = 0;
                } else if(cnt->magic == ZIP_CENTRAL_MAGIC) {
                    nend = cnt->comp;
                    len = cnt->orig;
                    offs = cnt->offs;
                    if(nend == 0xffffffff || pend == 0xffffffff) {
                        e = tmp + sizeof(zip_central_t) + cnt->fnlen + cnt->extlen;
                        if(e > tmp + sizeof(tmp)) e = tmp + sizeof(tmp);
                        for(c = tmp + sizeof(zip_local_t) + cnt->fnlen; c < e; c += *((uint16_t*)(c + 2)) + 4)
                            if(*((uint16_t*)c) == ZIP_EXTZIP64_MAGIC) {
                                ext = (zip_ext64_t *)c;
                                nend = ext->comp;
                                len = ext->orig;
                                if(ext->size > 16) offs = ext->offs;
                                break;
                            }
                    }
                    if(numloc <= numcnt || dir[numcnt].fnlen != cnt->fnlen || dir[numcnt].extlen != cnt->extlen ||
                        dir[numcnt].crc32 != cnt->crc32 || dir[numcnt].method != cnt->method || dir[numcnt].comp != nend ||
                        dir[numcnt].orig != len || memcmp(dir[numcnt].fn, tmp + sizeof(zip_central_t), dir[numcnt].fnlen) ||
                        dir[numcnt].offs != offs) {
                            str += sprintf(str, "Inconsistent local headers and central directory #%ld\n", numcnt + 1);
                            ret = EINVAL;
                            goto err;
                    }
                    next += sizeof(zip_central_t) + cnt->fnlen + cnt->extlen;
                    nend = next + sizeof(zip_local_t) + 2;
                    cnt->magic = 0;
                    numcnt++;
                } else if(cnt->magic == ZIP_EOCD32_MAGIC || cnt->magic == ZIP_EOCD64_MAGIC) {
                    next = (uint64_t)-1;
                } else {
                    str += sprintf(str, "Unknown block magic %04x\n", *((uint32_t*)tmp));
                    ret = EINVAL;
                    goto err;
                }
            }
        }
        mbedtls_sha256_update(&sha, (const unsigned char*)f->buf, f->olen);
        f->pos += f->olen;
        f->olen = 0;
    }
    if(numloc != numcnt) {
        str += sprintf(str, "Inconsistent local headers and central directory length %ld != %ld\n", numloc, numcnt);
        ret = EINVAL;
        goto err;
    }
    mbedtls_sha256_finish(&sha, hash);
    str += sprintf(str, "sha256sum:  ");
    for(i = 0; i < 32; i++)
        str += sprintf(str, "%02x", hash[i]);
    str += sprintf(str, "\nOriginal:   %ld bytes, %ld files  Compressed: %ld bytes\n",
#ifdef __WIN32__
        (unsigned long int)
#endif
        orig, numloc,
#ifdef __WIN32__
        (unsigned long int)
#endif
        f->pos);
err:syspkg_close(f);
    mbedtls_sha256_free(&sha);
    if(dir) {
        for(n = 0; n < numloc; n++)
            free(dir[n].fn);
        free(dir);
    }
    return ret;
}

/**
 * Serialize and save a meta info
 */
int syspkg_store(syspkg_ctx_t *ctx, syspkg_meta_t **meta, pkgidx_t num)
{
    unsigned char tmp[65538];
    char ver[MAXVERSIZE];
    int ret = SUCCESS;
    syspkg_file_t *f;
    pkgidx_t i, j, k;
    ZSTD_CCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;
    (void)ctx;

    if(!meta) return EINVAL;
    zcmp = ZSTD_createCCtx();
    if(!zcmp) return ENOMEM;
    ZSTD_CCtx_setParameter(zcmp, ZSTD_c_compressionLevel, 1);
    ZSTD_CCtx_setParameter(zcmp, ZSTD_c_nbWorkers, 4);

    f = syspkg_open(PKGDIR "packages", 1);
    if(f) {
        *((pkgidx_t*)tmp) = (pkgidx_t)num;
        zi.src = &tmp; zi.size = sizeof(pkgidx_t); zi.pos = 0;
        zo.dst = f->buf; zo.size = sizeof(tmp); zo.pos = 0;
        ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_continue);
        for(i = 0; i < (pkgidx_t)num; i++) {
            if(!meta[i]->d.id || !meta[i]->d.version) continue;
            zi.size = 0;
            *((uint16_t*)(tmp + 2)) = (uint16_t)meta[i]->repo;
            *((pkgidx_t*)(tmp + 4)) = (pkgidx_t)meta[i]->idx;
            *((uint32_t*)(tmp + sizeof(pkgidx_t) + 4)) = (uint32_t)meta[i]->d.version;
            tmp[sizeof(pkgidx_t) + 7] = 0; /* numdepends */
            tmp[sizeof(pkgidx_t) + 8] = 0; /* numsuggests */
            tmp[sizeof(pkgidx_t) + 9] = 0; /* numconflicts */
            tmp[sizeof(pkgidx_t) + 10] = 0; /* numcommands, numenv */
            *((uint32_t*)(tmp + sizeof(pkgidx_t) + 11)) = (uint32_t)meta[i]->d.afirst;
            *((uint32_t*)(tmp + sizeof(pkgidx_t) + 15)) = (uint32_t)meta[i]->d.alast;
            zi.size = sizeof(pkgidx_t) + 19;

            j = strlen(meta[i]->d.id) + 1; memcpy(tmp + zi.size, meta[i]->d.id, j); zi.size += j;
            if(meta[i]->d.name) {
                j = strlen(meta[i]->d.name) + 1; memcpy(tmp + zi.size, meta[i]->d.name, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->d.desc) {
                j = strlen(meta[i]->d.desc) + 1; memcpy(tmp + zi.size, meta[i]->d.desc, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->d.release) {
                j = strlen(meta[i]->d.release) + 1; memcpy(tmp + zi.size, meta[i]->d.release, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->d.category) {
                j = strlen(meta[i]->d.category) + 1; memcpy(tmp + zi.size, meta[i]->d.category, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->d.license) {
                j = strlen(meta[i]->d.license) + 1; memcpy(tmp + zi.size, meta[i]->d.license, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->d.maintainer) {
                j = strlen(meta[i]->d.maintainer) + 1; memcpy(tmp + zi.size, meta[i]->d.maintainer, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->eula) {
                j = strlen(meta[i]->eula) + 1; memcpy(tmp + zi.size, meta[i]->eula, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->d.homepage) {
                j = strlen(meta[i]->d.homepage) + 1; memcpy(tmp + zi.size, meta[i]->d.homepage, j); zi.size += j;
            } else tmp[zi.size++] = 0;
            if(meta[i]->d.bugtracker) {
                j = strlen(meta[i]->d.bugtracker) + 1; memcpy(tmp + zi.size, meta[i]->d.bugtracker, j); zi.size += j;
            } else tmp[zi.size++] = 0;

            if(meta[i]->d.url && *meta[i]->d.url && meta[i]->payloads && meta[i]->payloads[0].arch) {
                sprintf(ver, "%d.%d.%d", (meta[i]->d.version >> 16)&255, (meta[i]->d.version >> 8)&255, meta[i]->d.version&255);
                zi.size += syspkg_url(meta[i]->d.url, meta[i]->payloads[0].arch, ver, meta[i]->d.release, ctx->osver,
                    (char*)tmp + zi.size) + 1;
                j = strlen(meta[i]->payloads[0].arch) + 1;
                memcpy(tmp + zi.size, meta[i]->payloads[0].arch, j); zi.size += j;
                *((uint64_t*)(tmp + zi.size)) = (uint64_t)meta[i]->payloads[0].comp; zi.size += 8;
                *((uint64_t*)(tmp + zi.size)) = (uint64_t)meta[i]->payloads[0].orig; zi.size += 8;
                memcpy(tmp + zi.size, &meta[i]->payloads[0].hash, 32); zi.size += 32;
                *((uint32_t*)(tmp + zi.size)) = (uint32_t)meta[i]->numfiles; zi.size += 4;
            } else tmp[zi.size++] = 0;

            if(meta[i]->d.depends && meta[i]->d.depends[0].id) {
                for(k = 0; k < 128 && meta[i]->d.depends[k].id; k++, tmp[sizeof(pkgidx_t) + 7]++) {
                    *((uint32_t*)(tmp + zi.size)) = (uint32_t)meta[i]->d.depends[k].version; zi.size += 3;
                    j = strlen(meta[i]->d.depends[k].id)+1; memcpy(tmp+zi.size, meta[i]->d.depends[k].id, j); zi.size+=j;
                }
            }

            if(meta[i]->d.suggests && meta[i]->d.suggests[0].id) {
                for(k = 0; k < 128 && meta[i]->d.suggests[k].id; k++, tmp[sizeof(pkgidx_t) + 8]++) {
                    *((uint32_t*)(tmp + zi.size)) = (uint32_t)meta[i]->d.suggests[k].version; zi.size += 3;
                    j = strlen(meta[i]->d.suggests[k].id)+1; memcpy(tmp+zi.size, meta[i]->d.suggests[k].id, j); zi.size+=j;
                }
            }

            if(meta[i]->d.conflicts && meta[i]->d.conflicts[0].id) {
                for(k = 0; k < 128 && meta[i]->d.conflicts[k].id; k++, tmp[sizeof(pkgidx_t) + 9]++) {
                    *((uint32_t*)(tmp + zi.size)) = (uint32_t)meta[i]->d.conflicts[k].version; zi.size += 3;
                    j = strlen(meta[i]->d.conflicts[k].id)+1; memcpy(tmp+zi.size, meta[i]->d.conflicts[k].id, j); zi.size+=j;
                }
            }

            if(meta[i]->commands && meta[i]->commands[0]) {
                for(k = 0; k < 8 && meta[i]->commands[k]; k++, tmp[sizeof(pkgidx_t) + 10] += 32) {
                    j = strlen(meta[i]->commands[k])+1; memcpy(tmp+zi.size, meta[i]->commands[k], j); zi.size+=j;
                }
                if(meta[i]->env && meta[i]->env[0].name && meta[i]->env[0].type) {
                    for(k = 0; k < 32 && meta[i]->env[k].name; k++, tmp[sizeof(pkgidx_t) + 10]++) {
                        j = strlen(meta[i]->env[k].name)+1; memcpy(tmp+zi.size, meta[i]->env[k].name, j); zi.size+=j;
                        j = strlen(meta[i]->env[k].type)+1; memcpy(tmp+zi.size, meta[i]->env[k].type, j); zi.size+=j;
                        if(meta[i]->env[k].desc && meta[i]->env[k].desc[0].name && meta[i]->env[k].desc[0].desc) {
                            j = strlen(meta[i]->env[k].desc[0].name)+1; memcpy(tmp+zi.size, meta[i]->env[k].desc[0].name, j);
                            zi.size+=j;
                            j = strlen(meta[i]->env[k].desc[0].desc)+1; memcpy(tmp+zi.size, meta[i]->env[k].desc[0].desc, j);
                            zi.size+=j;
                        } else tmp[zi.size++] = 0;
                    }
                }
            }

            *((uint16_t*)(tmp + 0)) = (uint16_t)zi.size - 2;
            zi.src = &tmp; zi.pos = 0;
            zo.dst = f->buf; zo.size = sizeof(tmp);
            ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_continue);
            f->ilen = zo.pos;
            f->olen = 0;
            syspkg_write(f);
            if(f->olen != f->ilen) { ret = EIO; break; }
            zo.pos = 0;
        }
        memset(tmp, 0, sizeof(uint16_t));
        zi.src = &tmp; zi.size = sizeof(uint16_t); zi.pos = 0;
        zo.dst = f->buf; zo.size = sizeof(tmp); zo.pos = 0;
        ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_end);
        f->ilen = zo.pos;
        f->olen = 0;
        syspkg_write(f);
        syspkg_close(f);
    }
    ZSTD_freeCCtx(zcmp);
    return ret;
}

/**
 * Load back serialized data matching specific patterns. All filters are optional
 * @installed - when non-zero, only return installed packages
 * @search - only return if packageid, name or description contains this string
 * @depends - only return if the package depends on this package
 * @ids - a 0xffff terminated list of numeric package ids to return
 */
int syspkg_search(syspkg_ctx_t *ctx, int installed, char *search, char *depends, int *ids)
{
    unsigned char *s, *e, tmp[65538];
    syspkg_meta_t **meta = NULL;
    syspkg_file_t *f;
    pkgidx_t len = 0, i = 0, j, l = 0;
    int k, m, d;
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(!ctx) return EINVAL;
    if(ctx->packages) {
        for(j = 0; ctx->packages[j]; j++)
            syspkg_freemeta((syspkg_meta_t*)ctx->packages[j]);
        free(ctx->packages);
        ctx->packages = NULL;
    }
    ctx->numpackages = 0;
    syspkg_loadinstalled(ctx);

    zcmp = ZSTD_createDCtx();
    if(!zcmp) return ENOMEM;

    f = syspkg_open(PKGDIR "packages", 0);
    if(f) {
        syspkg_read(f);
        zi.src = f->buf; zi.size = f->olen; zi.pos = 0;
        zo.dst = &tmp; zo.size = sizeof(pkgidx_t); zo.pos = 0;
        ZSTD_decompressStream(zcmp, &zo, &zi);
        len = *((pkgidx_t*)tmp);
        meta = (syspkg_meta_t**)malloc((len + 1) * sizeof(syspkg_meta_t*));
        if(!meta) { syspkg_close(f); return ENOMEM; }
        memset(meta, 0, (len + 1) * sizeof(syspkg_meta_t*));
        for(j = i = 0; j < len; j++) {
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = sizeof(uint16_t); zo.pos = 0;
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.pos != sizeof(uint16_t) || !*((uint16_t*)tmp)) break;
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = *((uint16_t*)tmp) + sizeof(uint16_t); zo.pos = sizeof(uint16_t);
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.pos != *((uint16_t*)tmp) + sizeof(uint16_t)) break;
            if(!(*((uint32_t*)(tmp + sizeof(pkgidx_t) + 4)) & 0xffffff)) continue;
            if(ids) {
                for(k = 0; ids[k] != (pkgidx_t)-1U && ids[k] != *((pkgidx_t*)(tmp + 4)); k++);
                if(ids[k] == (pkgidx_t)-1U) continue;
            }
            s = tmp + sizeof(pkgidx_t) + 19;
            for(k = -1; l < ctx->numinst; ) {
                k = strnatcmp(ctx->inst[l].id, (char*)s);
                if(k >= 0) break;
                if(k < 0) l++;
            }
            if(installed && k) continue;
            meta[i] = (syspkg_meta_t*)malloc(sizeof(syspkg_meta_t));
            if(!meta[i]) goto err;
            memset(meta[i], 0, sizeof(syspkg_meta_t));
            for(e = s; *e; e++);
            meta[i]->d.id = (char*)malloc(e - s + 1);
            if(!meta[i]->d.id) goto err;
            strcpy(meta[i]->d.id, (char*)s);
            meta[i]->base = (e - s > 3 && !memcmp(s, "base", 4) && (s[4] == 0 || s[4] == '.'));
            if(!k && l < ctx->numinst && ctx->inst[l].iversion && ctx->inst[l].irelease) {
                meta[i]->d.iversion = ctx->inst[l].iversion;
                meta[i]->d.irelease = (char*)malloc(strlen(ctx->inst[l].irelease) + 1);
                if(!meta[i]->d.irelease) goto err;
                strcpy(meta[i]->d.irelease, ctx->inst[l].irelease);
            }
            for(s = e + 1, e = s; *e; e++);
            if(search && !strstr(meta[i]->d.id, search) && !strstr((char*)s, search) && !strstr((char*)e + 1, search)) {
                free(meta[i]->d.id);
                if(meta[i]->d.irelease) free(meta[i]->d.irelease);
                free(meta[i]);
                meta[i] = NULL;
                continue;
            }
            meta[i]->repo = *((uint16_t*)(tmp + 2));
            meta[i]->idx = *((pkgidx_t*)(tmp + 4));
            meta[i]->d.version = *((uint32_t*)(tmp + sizeof(pkgidx_t) + 4)) & 0xffffff;
            meta[i]->d.afirst = *((uint32_t*)(tmp + sizeof(pkgidx_t) + 11));
            meta[i]->d.alast = *((uint32_t*)(tmp + sizeof(pkgidx_t) + 15));
            meta[i]->desc = (syspkg_desc_t*)malloc(2 * sizeof(syspkg_desc_t));
            if(!meta[i]->desc) goto err;
            memset(meta[i]->desc, 0, 2 * sizeof(syspkg_desc_t));
            meta[i]->desc[0].lang = (char*)malloc(6);
            if(!meta[i]->desc[0].lang) goto err;
            strcpy(meta[i]->desc[0].lang, ctx->lang);
            meta[i]->desc[0].name = (char*)malloc(e - s + 1);
            if(!meta[i]->desc[0].name) goto err;
            strcpy(meta[i]->desc[0].name, (char*)s);
            meta[i]->d.name = meta[i]->desc[0].name;
            for(s = e + 1, e = s; *e; e++);
            meta[i]->desc[0].desc = (char*)malloc(e - s + 1);
            if(!meta[i]->desc[0].desc) goto err;
            strcpy(meta[i]->desc[0].desc, (char*)s);
            meta[i]->d.desc = meta[i]->desc[0].desc;
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->d.release = (char*)malloc(e - s + 1);
                if(!meta[i]->d.release) goto err;
                strcpy(meta[i]->d.release, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->d.category = (char*)malloc(e - s + 1);
                if(!meta[i]->d.category) goto err;
                strcpy(meta[i]->d.category, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->d.license = (char*)malloc(e - s + 1);
                if(!meta[i]->d.license) goto err;
                strcpy(meta[i]->d.license, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->d.maintainer = (char*)malloc(e - s + 1);
                if(!meta[i]->d.maintainer) goto err;
                strcpy(meta[i]->d.maintainer, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->eula = (char*)malloc(e - s + 1);
                if(!meta[i]->eula) goto err;
                strcpy(meta[i]->eula, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->d.homepage = (char*)malloc(e - s + 1);
                if(!meta[i]->d.homepage) goto err;
                strcpy(meta[i]->d.homepage, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->d.bugtracker = (char*)malloc(e - s + 1);
                if(!meta[i]->d.bugtracker) goto err;
                strcpy(meta[i]->d.bugtracker, (char*)s);
            }
            for(s = e + 1, e = s; *e; e++);
            if(e - s > 1) {
                meta[i]->d.url = (char*)malloc(e - s + 1);
                if(!meta[i]->d.url) goto err;
                strcpy(meta[i]->d.url, (char*)s);
                meta[i]->payloads = (syspkg_payload_t*)malloc(2 * sizeof(syspkg_payload_t));
                if(!meta[i]->payloads) goto err;
                memset(meta[i]->payloads, 0, 2 * sizeof(syspkg_payload_t));
                for(s = e + 1, e = s; *e; e++);
                meta[i]->payloads[0].arch = (char*)malloc(e - s + 1);
                if(!meta[i]->payloads[0].arch) goto err;
                strcpy(meta[i]->payloads[0].arch, (char*)s);
                s = e + 1;
                meta[i]->payloads[0].comp = *((uint64_t*)s); s += 8;
                meta[i]->payloads[0].orig = *((uint64_t*)s); s += 8;
                memcpy(&meta[i]->payloads[0].hash, s, 32); s += 32;
                meta[i]->numfiles = *((uint32_t*)s); s += 4;
            } else
                s++;
            d = 0;
            if(tmp[sizeof(pkgidx_t) + 7]) {
                meta[i]->d.depends = (syspkg_dep_t*)malloc((tmp[sizeof(pkgidx_t) + 7] + 1) * sizeof(syspkg_dep_t));
                if(!meta[i]->d.depends) goto err;
                for(k = 0; k < tmp[sizeof(pkgidx_t) + 7]; k++) {
                    meta[i]->d.depends[k].version = *((uint32_t*)s) & 0xffffff; s += 3;
                    for(e = s; *e; e++);
                    if(depends && *depends && !strcmp((char*)s, depends)) d = 1;
                    meta[i]->d.depends[k].id = (char*)malloc(e - s + 1);
                    if(!meta[i]->d.depends[k].id) goto err;
                    strcpy(meta[i]->d.depends[k].id, (char*)s);
                    s = e + 1;
                }
                meta[i]->d.depends[k].id = NULL;
                meta[i]->d.depends[k].version = 0;
            }
            if(depends && *depends && !d) {
                free(meta[i]->d.id);
                if(meta[i]->d.irelease) free(meta[i]->d.irelease);
                if(meta[i]->d.release) free(meta[i]->d.release);
                if(meta[i]->d.category) free(meta[i]->d.category);
                if(meta[i]->d.license) free(meta[i]->d.license);
                if(meta[i]->d.maintainer) free(meta[i]->d.maintainer);
                if(meta[i]->d.homepage) free(meta[i]->d.homepage);
                if(meta[i]->d.bugtracker) free(meta[i]->d.bugtracker);
                if(meta[i]->d.url) free(meta[i]->d.url);
                if(meta[i]->eula) free(meta[i]->eula);
                if(meta[i]->desc) {
                    if(meta[i]->desc[0].lang) free(meta[i]->desc[0].lang);
                    if(meta[i]->desc[0].name) free(meta[i]->desc[0].name);
                    if(meta[i]->desc[0].desc) free(meta[i]->desc[0].desc);
                    free(meta[i]->desc);
                }
                if(meta[i]->payloads) {
                    if(meta[i]->payloads[0].arch) free(meta[i]->payloads[0].arch);
                    free(meta[i]->payloads);
                }
                if(meta[i]->d.depends) {
                    for(k = 0; meta[i]->d.depends[k].id; k++)
                        free(meta[i]->d.depends[k].id);
                    free(meta[i]->d.depends);
                }
                free(meta[i]);
                meta[i] = NULL;
                continue;
            }
            if(tmp[sizeof(pkgidx_t) + 8]) {
                meta[i]->d.suggests = (syspkg_dep_t*)malloc((tmp[sizeof(pkgidx_t) + 8] + 1) * sizeof(syspkg_dep_t));
                if(!meta[i]->d.suggests) goto err;
                for(k = 0; k < tmp[sizeof(pkgidx_t) + 8]; k++) {
                    meta[i]->d.suggests[k].version = *((uint32_t*)s) & 0xffffff; s += 3;
                    for(e = s; *e; e++);
                    meta[i]->d.suggests[k].id = (char*)malloc(e - s + 1);
                    if(!meta[i]->d.suggests[k].id) goto err;
                    strcpy(meta[i]->d.suggests[k].id, (char*)s);
                    s = e + 1;
                }
                meta[i]->d.suggests[k].id = NULL;
                meta[i]->d.suggests[k].version = 0;
            }
            if(tmp[sizeof(pkgidx_t) + 9]) {
                meta[i]->d.conflicts = (syspkg_dep_t*)malloc((tmp[sizeof(pkgidx_t) + 9] + 1) * sizeof(syspkg_dep_t));
                if(!meta[i]->d.conflicts) goto err;
                for(k = 0; k < tmp[sizeof(pkgidx_t) + 9]; k++) {
                    meta[i]->d.conflicts[k].version = *((uint32_t*)s) & 0xffffff; s += 3;
                    for(e = s; *e; e++);
                    meta[i]->d.conflicts[k].id = (char*)malloc(e - s + 1);
                    if(!meta[i]->d.conflicts[k].id) goto err;
                    strcpy(meta[i]->d.conflicts[k].id, (char*)s);
                    s = e + 1;
                }
                meta[i]->d.conflicts[k].id = NULL;
                meta[i]->d.conflicts[k].version = 0;
            }
            if(tmp[sizeof(pkgidx_t) + 10] & 0xE0) {
                m = (tmp[sizeof(pkgidx_t) + 10] >> 5) & 7;
                meta[i]->commands = (char**)malloc((m + 1) * sizeof(char*));
                if(!meta[i]->commands) goto err;
                memset(meta[i]->commands, 0, (m + 1) * sizeof(char*));
                for(k = 0; k < m; k++) {
                    for(e = s; *e; e++);
                    meta[i]->commands[k] = (char*)malloc(e - s + 1);
                    if(!meta[i]->commands[k]) goto err;
                    strcpy(meta[i]->commands[k], (char*)s);
                    s = e + 1;
                }
                m = tmp[sizeof(pkgidx_t) + 10] & 31;
                if(m) {
                    meta[i]->env = (syspkg_env_t*)malloc((m + 1) * sizeof(syspkg_env_t));
                    if(!meta[i]->env) goto err;
                    memset(meta[i]->env, 0, (m + 1) * sizeof(syspkg_env_t));
                    for(k = 0; k < m; k++) {
                        for(e = s; *e; e++);
                        meta[i]->env[k].name = (char*)malloc(e - s + 1);
                        if(!meta[i]->env[k].name) goto err;
                        strcpy(meta[i]->env[k].name, (char*)s);
                        for(s = e + 1, e = s; *e; e++);
                        meta[i]->env[k].type = (char*)malloc(e - s + 1);
                        if(!meta[i]->env[k].type) goto err;
                        strcpy(meta[i]->env[k].type, (char*)s);
                        for(s = e + 1, e = s; *e; e++);
                        if(*s) {
                            meta[i]->env[k].desc = (syspkg_desc_t*)malloc(2 * sizeof(syspkg_desc_t));
                            if(!meta[i]->env[k].desc) goto err;
                            memset(meta[i]->env[k].desc, 0, 2 * sizeof(syspkg_desc_t));
                            meta[i]->env[k].desc[0].lang = (char*)malloc(6);
                            if(!meta[i]->env[k].desc[0].lang) goto err;
                            strcpy(meta[i]->env[k].desc[0].lang, ctx->lang);
                            meta[i]->env[k].desc[0].name = (char*)malloc(e - s + 1);
                            if(!meta[i]->env[k].desc[0].name) goto err;
                            strcpy(meta[i]->env[k].desc[0].name, (char*)s);
                            for(s = e + 1, e = s; *e; e++);
                            meta[i]->env[k].desc[0].desc = (char*)malloc(e - s + 1);
                            if(!meta[i]->env[k].desc[0].desc) goto err;
                            strcpy(meta[i]->env[k].desc[0].desc, (char*)s);
                            s = e + 1;
                        } else s++;
                    }
                }
            }
            i++;
        }
err:    syspkg_close(f);
    }
    ZSTD_freeDCtx(zcmp);
    if(i < len && meta && meta[i]) { syspkg_freemeta(meta[i]); meta[i] = NULL; }
    if(i && meta && meta[0]) {
        ctx->packages = (syspkg_package_t**)meta;
        ctx->numpackages = i;
    } else
    if(meta) free(meta);
    return ctx->packages ? SUCCESS : ENOENT;
}

/**
 * Return packages that has a certain file
 */
int syspkg_which(syspkg_ctx_t *ctx, char *search)
{
    unsigned char tmp[MAXPATHSIZE + 8 + 3 + sizeof(pkgidx_t)];
    syspkg_file_t *f;
    int i, *ids = NULL, ret = ENOENT;
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(!ctx) return EINVAL;
    if(ctx->packages) {
        for(i = 0; ctx->packages[i]; i++)
            syspkg_freemeta((syspkg_meta_t*)ctx->packages[i]);
        free(ctx->packages);
        ctx->packages = NULL;
    }
    if(!search || !*search) return EINVAL;
    zcmp = ZSTD_createDCtx();
    if(!zcmp) return ENOMEM;

    f = syspkg_open(PKGDIR "files", 0);
    if(f) {
        zi.pos = zi.size = 0; i = 0;
        while(1) {
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = sizeof(uint16_t); zo.pos = 0;
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.size != sizeof(uint16_t) || !*((uint16_t*)tmp)) break;
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = *((uint16_t*)tmp) + sizeof(uint16_t); zo.pos = sizeof(uint16_t);
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.pos != *((uint16_t*)tmp) + sizeof(uint16_t)) break;
            if(!strstr((char*)tmp + sizeof(uint16_t) + sizeof(pkgidx_t) + 8, search)) continue;
            ids = (int*)realloc(ids, (i + 2) * sizeof(int));
            if(!ids) break;
            ids[i] = *((pkgidx_t*)(tmp + sizeof(uint16_t)));
            ids[i + 1] = (pkgidx_t)-1U;
            i++;
        }
        syspkg_close(f);
    }
    ZSTD_freeDCtx(zcmp);
    if(ids) {
        ret = syspkg_search(ctx, 0, NULL, NULL, ids);
        free(ids);
    }
    return ret;
}

/**
 * Load file list for a package
 */
int syspkg_loadfiles(syspkg_meta_t *meta)
{
    unsigned char tmp[MAXPATHSIZE + 8 + 3 + sizeof(pkgidx_t)];
    syspkg_file_t *f;
    int i, ret = SUCCESS, was = 0;
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(!meta) return EINVAL;
    if(meta->files) {
        for(i = 0; i < meta->numfiles && meta->files[i]; i++)
            free(meta->files[i]);
        free(meta->files);
        meta->files = NULL;
    }
    meta->numfiles = 0;
    zcmp = ZSTD_createDCtx();
    if(!zcmp) return ENOMEM;

    f = syspkg_open(PKGDIR "files", 0);
    if(f) {
        zi.pos = zi.size = 0; i = 0;
        while(1) {
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = sizeof(uint16_t); zo.pos = 0;
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.size != sizeof(uint16_t) || !*((uint16_t*)tmp)) break;
            if(zi.pos == zi.size) { syspkg_read(f); zi.src = f->buf; zi.size = f->olen; zi.pos = 0; }
            zo.dst = &tmp; zo.size = *((uint16_t*)tmp) + sizeof(uint16_t); zo.pos = sizeof(uint16_t);
            ZSTD_decompressStream(zcmp, &zo, &zi);
            if(zo.pos != *((uint16_t*)tmp) + sizeof(uint16_t)) break;
            if(*((pkgidx_t*)(tmp + sizeof(uint16_t))) != meta->idx) {
                if(was) break;
                else continue;
            }
            was = 1;
            meta->files = (char**)realloc(meta->files, (meta->numfiles + 2) * sizeof(char *));
            if(!meta->files) { ret = ENOMEM; break; }
            meta->files[meta->numfiles + 1] = NULL;
            meta->files[meta->numfiles] = (char*)malloc(*((uint16_t*)tmp) - 2);
            if(!meta->files[meta->numfiles]) { ret = ENOMEM; break; }
            memcpy(meta->files[meta->numfiles], tmp + sizeof(uint16_t) + sizeof(pkgidx_t), *((uint16_t*)tmp) - 2);
            meta->numfiles++;
        }
        syspkg_close(f);
    }
    ZSTD_freeDCtx(zcmp);
    return ret;
}
