/*
 * syspkg/src/lib/payloads.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Handling package payloads (aka tarballs)
 *
 */

#include "internal.h"

/**
 * Count number of files and accumulate file sizes
 */
void syspkg_payload_count(int type, int64_t size, uint64_t mtime, char *full, char *name, void *data)
{
    syspkg_build_t *build = (syspkg_build_t*)data;
    syspkg_meta_t *meta = build->meta;
    char *o;
    int i, j;
    (void)mtime; (void)full;
    if(size < 0 || strchr(name, ':')) return;
    if(!meta->base) {
        for(i = 0; i < OVERRIDELEN; i++) {
            if(meta->over[i]) { o = meta->over[i]; j = meta->ovrl[i]; }
            else { o = (char*)syspkg_ploaddirs[i]; j = syspkg_lenpldirs[i]; }
            if(!memcmp(name, o, j) && (!name[j] || name[j] == SEP[0])) break;
        }
        if(i == OVERRIDELEN) return;
    }
    if(type == SYSPKG_WALK_TYPE_REG)
        build->orig += size;
    build->idx++;
}

/**
 * Add an entry to the zip archive
 */
void syspkg_payload_add(int type, int64_t size, uint64_t mtime, char *full, char *name, void *data)
{
    static uint32_t emode[] = { 0100000, 0120000, 0040000 };    /* struct stat's st_mode for SYSPKG_WALK_TYPE_x */
    syspkg_build_t *build = (syspkg_build_t*)data;
    syspkg_meta_t *meta = build->meta;
    syspkg_file_t *in;
    zip_local_t *l = (zip_local_t*)build->f->buf;
    zip_ext64_t *z;
    unsigned char *b;
    char *canonized = syspkg_realpath(full), *s, *d, *o, fn[MAXPATHSIZE+1];
    struct tm *tm;
    int n, i, j, is64;
    uint64_t per, old = -1UL, remaining;
    ZSTD_CCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(size >= 0 && !strchr(name, ':') && (!build->f->full || !canonized || strcmp(build->f->full, canonized))) {
        n = strlen(name);
        build->central[build->idx].fn = (char*)malloc(n + 8); /* diff because of dir override */
        if(!build->central[build->idx].fn) goto skip2;
        s = name; d = build->central[build->idx].fn;
        if(!meta->base) {
            for(i = 0; i < OVERRIDELEN; i++) {
                if(meta->over[i]) { o = meta->over[i]; j = meta->ovrl[i]; }
                else { o = (char*)syspkg_ploaddirs[i]; j = syspkg_lenpldirs[i]; }
                if(!memcmp(s, o, j) && (!s[j] || s[j] == SEP[0])) {
                    for(; *o; o++, s++)
                        *d++ = (*o == SEP[0]) ? '/' : *o;
                    break;
                }
            }
            if(i == OVERRIDELEN) {
skip:           free(build->central[build->idx].fn);
                build->central[build->idx].fn = NULL;
skip2:          free(canonized);
                return;
            }
        }
        for(; *s; s++)
            *d++ = (*s == SEP[0]) ? '/' : *s;
        *d = 0;
        build->central[build->idx].fnlen = d - build->central[build->idx].fn;
        /* don't use st_mode directly here, the zip might be extracted on a different OS, use common UNIX values */
        build->central[build->idx].eattr = (emode[type] << 16);
        tm = gmtime((const time_t*)&mtime);
        if(tm) {
            build->central[build->idx].mtime = (((tm->tm_year - 80) & 0x7f) << 25) | ((tm->tm_mon + 1) << 21) |
                (tm->tm_mday << 16) | (tm->tm_hour << 11) | (tm->tm_min << 5) | (tm->tm_sec >> 1);
        }
        l->magic = ZIP_LOCAL_MAGIC;
        l->version = build->central[build->idx].version = 10;
        l->mtime = build->central[build->idx].mtime;
        l->flags = l->method = l->extlen = 0;
        l->crc32 = l->comp = l->orig = 0;
        build->f->ilen = (int)sizeof(zip_local_t);
        if(type == SYSPKG_WALK_TYPE_REG || type == SYSPKG_WALK_TYPE_LNK) {
            for(s = build->central[build->idx].fn, d = fn; *s && d + 1 < fn + MAXPATHSIZE; s++)
                *d++ = (*s == '/') ? SEP[0] : *s;
            *d = 0;
        } else {
            build->central[build->idx].fn[build->central[build->idx].fnlen++] = '/';
            build->central[build->idx].fn[build->central[build->idx].fnlen] = 0;
        }
        l->fnlen = build->central[build->idx].fnlen;
        memcpy(build->f->buf + build->f->ilen, build->central[build->idx].fn, l->fnlen);
        build->f->ilen += l->fnlen;
        build->central[build->idx].offs = build->comp;
        if(type == SYSPKG_WALK_TYPE_DIR) {
            build->central[build->idx].eattr |= 0x10;
            build->comp += build->f->ilen;
            mbedtls_sha256_update(&build->sha, (const unsigned char*)build->f->buf, build->f->ilen);
            syspkg_write(build->f);
        } else
        if(type == SYSPKG_WALK_TYPE_LNK) {
            build->central[build->idx].iattr = 1;
            l->comp = l->orig = (uint32_t)syspkg_readlink(full, (char*)build->f->buf + build->f->ilen, MAXPATHSIZE);
            l->crc32 = build->central[build->idx].crc32 = crc32(0, build->f->buf + build->f->ilen, l->comp);
            build->f->ilen += l->comp;
            build->comp += build->f->ilen;
            mbedtls_sha256_update(&build->sha, (const unsigned char*)build->f->buf, build->f->ilen);
            syspkg_write(build->f);
            syspkg_addfnmeta(meta, l->orig, fn, build->central[build->idx].fnlen);
        } else
        if(type == SYSPKG_WALK_TYPE_REG) {
            in = syspkg_open(full, 0);
            if(!in) goto skip;
            zcmp = ZSTD_createCCtx();
            if(!zcmp) { syspkg_close(in); goto skip; }
            ZSTD_CCtx_setParameter(zcmp, ZSTD_c_compressionLevel, 1);
            ZSTD_CCtx_setParameter(zcmp, ZSTD_c_nbWorkers, 4);
            build->central[build->idx].orig = size;
            is64 = 0; l->orig = size;
            if((uint64_t)size < FBUFSIZE - MAXPATHSIZE - 1 - sizeof(zip_local_t)) {
                /* the file fits in our remaining memory buffer */
                in->ilen = size;
                in->olen = 0;
                syspkg_read(in);
                l->crc32 = build->central[build->idx].crc32 = crc32(0, in->buf, in->ilen);
                b = build->f->buf + sizeof(zip_local_t) + build->central[build->idx].fnlen;
                zi.src = in->buf; zi.size = size; zi.pos = 0;
                zo.dst = b; zo.size = FBUFSIZE - MAXPATHSIZE - 1 - sizeof(zip_local_t); zo.pos = 0;
                remaining = ZSTD_compressStream2(zcmp, &zo , &zi, ZSTD_e_end);
                if(!remaining && zi.pos == (size_t)size) {
                    l->version = build->central[build->idx].version = 63;
                    l->method = build->central[build->idx].method = ZIP_DATA_ZSTD;
                } else {
                    memcpy(b, in->buf, size);
                    zo.pos = size;
                }
                l->comp = build->central[build->idx].comp = zo.pos;
                build->f->ilen = sizeof(zip_local_t) + build->central[build->idx].fnlen + zo.pos; build->f->olen = 0;
                build->comp += build->f->ilen;
                mbedtls_sha256_update(&build->sha, (const unsigned char*)build->f->buf, build->f->ilen);
                syspkg_write(build->f);
            } else {
                /* file is larger than our buffer */
                if(size >= 0xffffffff) {
                    /* file is larger than 4G, use ZIP64 */
                    l->comp = l->orig = -1;
                    l->extlen = 20;
                    z = (zip_ext64_t*)(build->f->buf + build->f->ilen);
                    build->f->ilen += 20;
                    z->magic = ZIP_EXTZIP64_MAGIC;
                    z->size = 16;
                    z->comp = 0;
                    z->orig = size;
                    is64 = 1;
                }
                l->version = build->central[build->idx].version = 63;
                l->method = build->central[build->idx].method = ZIP_DATA_ZSTD;
                /* write out incomplete local header */
                build->comp += build->f->ilen; build->f->olen = 0;
                syspkg_write(build->f);
                /* write compressed data */
                remaining = 0;
                while(1) {
                    in->ilen = FBUFSIZE;
                    in->olen = 0;
                    syspkg_read(in);
                    if(!in->olen) break;
                    build->central[build->idx].crc32 = crc32(build->central[build->idx].crc32, in->buf, in->olen);
                    zi.src = in->buf; zi.size = in->olen; zi.pos = 0;
                    zo.dst = build->f->buf; zo.size = FBUFSIZE; zo.pos = 0;
                    remaining = ZSTD_compressStream2(zcmp, &zo , &zi, in->olen < FBUFSIZE ? ZSTD_e_end : ZSTD_e_continue);
                    build->f->ilen = zo.pos; build->f->olen = 0;
                    syspkg_write(build->f);
                    build->central[build->idx].comp += zo.pos;
                    build->comp += zo.pos;
                } while(in->olen < FBUFSIZE ? (remaining != 0) : (zi.pos != zi.size));
                /* update local header and calculate sha */
                syspkg_seek(build->f, build->central[build->idx].offs + 14);
                memcpy(build->f->buf, &build->central[build->idx].crc32, 4);
                build->f->ilen = 4; build->f->olen = 0;
                syspkg_write(build->f);
                if(is64) {
                    syspkg_seek(build->f, build->central[build->idx].offs+sizeof(zip_local_t)+build->central[build->idx].fnlen+12);
                    memcpy(build->f->buf, &build->central[build->idx].comp, 8);
                    build->f->ilen = 8; build->f->olen = 0;
                    syspkg_write(build->f);
                } else {
                    syspkg_seek(build->f, build->central[build->idx].offs + 18);
                    memcpy(build->f->buf, &build->central[build->idx].comp, 4);
                    build->f->ilen = 4; build->f->olen = 0;
                    syspkg_write(build->f);
                }
                syspkg_seek(build->f, build->central[build->idx].offs);
                b = build->f->buf;
                build->f->buf = in->buf;
                do {
                    build->f->ilen = FBUFSIZE; build->f->olen = 0;
                    syspkg_read(build->f);
                    mbedtls_sha256_update(&build->sha, (const unsigned char*)in->buf, build->f->olen);
                } while(build->f->olen == FBUFSIZE);
                build->f->buf = b;
            }
            ZSTD_freeCCtx(zcmp);
            syspkg_close(in);
            syspkg_addfnmeta(meta, size, fn, build->central[build->idx].fnlen);
            build->pos += size;
        }
        if(build->progressbar) {
            per = build->pos * 100 / (build->orig ? build->orig : 1);
            if(per != old) {
                old = per;
                (*build->progressbar)(build->pl, build->numpl, build->pos, build->orig + 1, SYSPKG_MSG_GENPAYLOAD);
            }
        }
        build->idx++;
    }
    if(canonized) free(canonized);
}

/**
 * Add the Central Directory and the EOCD
 */
void syspkg_payload_close(syspkg_build_t *build)
{
    zip_central_t *c = (zip_central_t*)build->f->buf;
    zip_ext64_t *z;
    zip_eocd64_t *e = (zip_eocd64_t*)build->f->buf;
    zip_eocd32_t *f = (zip_eocd32_t*)build->f->buf;
    zip_eocdloc_t *l = (zip_eocdloc_t*)(build->f->buf + sizeof(zip_eocd64_t));
    uint64_t cdsize = 0;
    int i, need64 = (build->idx > 0xffff) || (build->comp > 0xffffffff);

    /* add central directory */
    for(i = 0; i < build->idx; i++) {
        c->magic = ZIP_CENTRAL_MAGIC;
        c->madeby = ZIP_DATA_MADEBY;
        c->version = build->central[i].version;
        c->mtime = build->central[i].mtime;
        c->flags = c->extlen = c->comlen = c->disk = 0;
        c->method = build->central[i].method;
        c->extlen = build->central[i].extlen;
        c->crc32 = build->central[i].crc32;
        c->iattr = build->central[i].iattr;
        c->eattr = build->central[i].eattr;
        c->fnlen = build->central[i].fnlen;
        memcpy(build->f->buf + sizeof(zip_central_t), build->central[i].fn, c->fnlen);
        build->f->ilen = sizeof(zip_central_t) + c->fnlen;
        if(build->central[i].comp >= 0xffffffff || build->central[i].orig >= 0xffffffff ||
            build->central[i].offs >= 0xffffffff) {
            need64 = 1;
            c->comp = c->orig = c->offs = -1;
            c->extlen = 28;
            z = (zip_ext64_t*)(build->f->buf + build->f->ilen);
            build->f->ilen += 28;
            z->magic = ZIP_EXTZIP64_MAGIC;
            z->size = 24;
            z->comp = build->central[i].comp;
            z->orig = build->central[i].orig;
            z->offs = build->central[i].offs;
        } else {
            c->comp = build->central[i].comp;
            c->orig = build->central[i].orig;
            c->offs = build->central[i].offs;
        }
        cdsize += build->f->ilen;
        mbedtls_sha256_update(&build->sha, (const unsigned char*)build->f->buf, build->f->ilen);
        syspkg_write(build->f);
        free(build->central[i].fn);
    }
    if(need64) {
        /* 64 bit version of end of central directory */
        build->f->ilen = sizeof(zip_eocd64_t) + sizeof(zip_eocdloc_t);
        e->magic = ZIP_EOCD64_MAGIC;
        e->eocdsize = sizeof(zip_eocd64_t) - 12;
        e->version = 45;
        e->madeby = ZIP_DATA_MADEBY;
        e->numdisk = e->strtdisk = 0;
        e->totdisk = 1;
        e->nument = build->idx;
        e->cdsize = cdsize;
        e->totoffs = build->comp;
        /* 64 bit version end of central directory locator */
        l->magic = ZIP_EOCDLOC_MAGIC;
        l->strteocd = 0;
        l->reloffs = build->comp + cdsize;
        l->totdisk = 1;
        mbedtls_sha256_update(&build->sha, (const unsigned char*)build->f->buf, build->f->ilen);
        syspkg_write(build->f);
        /* this is plain stupidity. We need to a write a useless eocd32 even if we have already written eocd64. */
    }
    /* 32 bit end of central directory */
    build->f->ilen = sizeof(zip_eocd32_t);
    f->magic = ZIP_EOCD32_MAGIC;
    f->numdisk = f->strtdisk = f->comlen = 0;
    f->totdisk = 1;
    f->nument = build->idx > 0xffff ? 0xffff : build->idx;
    f->cdsize = cdsize;
    f->totoffs = build->comp > 0xffffffff ? 0xffffffff : build->comp;
    build->comp += cdsize + (need64 ? sizeof(zip_eocd64_t) + sizeof(zip_eocdloc_t) : 0) + sizeof(zip_eocd32_t);
    mbedtls_sha256_update(&build->sha, (const unsigned char*)build->f->buf, build->f->ilen);
    syspkg_write(build->f);
}

/**
 * Return full destination path for a payload path
 */
void syspkg_payload_dest(syspkg_meta_t *meta, int zip, char *src, int len, char *dst, int *idx)
{
    int i, l, n;
    char *o;
    if(!meta || !meta->d.id || !src || !*src || len < 1 || !dst) return;
    *dst = 0; if(idx) *idx = OVERRIDELEN;
    if(meta->base) {
        n = strlen(BASEDIR);
        strcpy(dst, BASEDIR);
        if(len > MAXPATHSIZE - n) len = MAXPATHSIZE - n;
        memcpy(dst + n, src, len);
        dst[n + len] = 0;
        if(idx) *idx = -1;
    } else {
        for(i = 0; i < OVERRIDELEN; i++) {
            if(!zip) { o = (char*)syspkg_overrides[i]; l = 3; } else {
                if(meta->over[i]) { o = meta->over[i]; l = meta->ovrl[i]; }
                else { o = (char*)syspkg_ploaddirs[i]; l = syspkg_lenpldirs[i]; }
            }
            if(!memcmp(src, o, l) && (src[l] == '/' || src[l] == SEP[0])) break;
        }
        if(idx) *idx = i;
        if(i == OVERRIDELEN) return;
        n = snprintf(dst, MAXPATHSIZE, syspkg_resolves[i], meta->d.id);
        i = MAXPATHSIZE - n;
        len -= l + 1;
        if(len > i - 1) len = i - 1;
        memcpy(dst + n, src + l + 1, len);
        dst[n + len] = 0;
    }
    if(SEP[0] != '/')
        for(src = dst; *src; src++)
            if(*src == '/') *src = SEP[0];
}

/**
 * Extract and archive
 */
int syspkg_payload_extract(syspkg_ctx_t *ctx, syspkg_meta_t *meta, char *fn, int inst, uint64_t dld)
{
    unsigned char *c, *e, tmp[512+1024+MAXPATHSIZE], *buf;
    char str[MAXPATHSIZE];
    int i, ret = SUCCESS, r, idx, skip = 0, first = 1, type;
    uint64_t next, nend, pend, len, offs, lst;
    zip_local_t *loc = (zip_local_t*)&tmp;
    zip_central_t *cnt = (zip_central_t*)&tmp;
    zip_ext64_t *ext;
    syspkg_file_t *f, *g, *h;
    z_stream zstrm;
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;

    if(!ctx || !meta || !meta->d.id || !fn || !*fn) return EINVAL;
    f = syspkg_open(fn, 0);
    if(!f) return ENOENT;
    g = syspkg_open(fn, 0);
    if(!g) { syspkg_close(f); return ENOENT; }
    /* in zip all directory separators are '/' */
    if(SEP[0] != '/') {
        for(i = 0; i < OVERRIDELEN; i++) {
            if(meta->over[i]) {
                for(c = (unsigned char*)meta->over[i]; *c; c++)
                    if(*c == SEP[0]) *c = '/';
            }
        }
    }
    /* read in the file, locate zip local headers */
    next = 0; nend = sizeof(zip_local_t) + 2;
    memset(tmp, 0, sizeof(tmp));
    while(!syspkg_read(f)) {
        if(!next && *((uint32_t*)f->buf) != ZIP_LOCAL_MAGIC) { ret = EBADF; goto err; }
        pend = f->pos + f->olen; lst = -1;
        /* one does not simply unpack a zip file, because local headers do not contain eattr, only central directory does... */
        while(next != (uint64_t)-1 && next < pend) {
            if(lst == nend) break;
            lst = nend;
            if(next < f->pos) {
                memcpy(tmp + f->pos-next, f->buf, f->olen > sizeof(tmp) ? sizeof(tmp) : f->olen);
            } else {
                memcpy(tmp, f->buf + next-f->pos, (pend < nend ? pend : nend) - next);
            }
            if(pend <= nend) break;
            if(nend < pend) {
                if(nend - next == sizeof(zip_local_t) + 2) {
                    if(loc->magic == ZIP_LOCAL_MAGIC)
                        nend = next + sizeof(zip_local_t) + loc->fnlen + loc->extlen;
                    else if(cnt->magic == ZIP_CENTRAL_MAGIC)
                        nend = next + sizeof(zip_central_t) + cnt->fnlen + cnt->extlen;
                    if(pend <= nend) break;
                } else if(loc->magic == ZIP_LOCAL_MAGIC) {
                    len = loc->comp;
                    if(len == 0xffffffff) {
                        e = tmp + sizeof(zip_local_t) + loc->fnlen + loc->extlen;
                        if(e > tmp + sizeof(tmp)) e = tmp + sizeof(tmp);
                        for(c = tmp + sizeof(zip_local_t) + loc->fnlen; c < e; c += *((uint16_t*)(c + 2)) + 4)
                            if(*((uint16_t*)c) == ZIP_EXTZIP64_MAGIC) { len = ((zip_ext64_t *)c)->comp; break; }
                    }
                    next += sizeof(zip_local_t) + loc->fnlen + loc->extlen + len;
                    nend = next + sizeof(zip_local_t) + 2;
                    loc->magic = 0;
                } else if(loc->magic == ZIP_SIGNATURE_MAGIC) {
                    next += sizeof(zip_sign_t) + ((zip_sign_t*)tmp)->size;
                    nend = next + sizeof(zip_local_t) + 2;
                    loc->magic = 0;
                } else if(loc->magic == ZIP_EXTDATA_MAGIC) {
                    next += sizeof(zip_extdata_t) + ((zip_extdata_t*)tmp)->size;
                    nend = next + sizeof(zip_local_t) + 2;
                    loc->magic = 0;
                } else if(cnt->magic == ZIP_CENTRAL_MAGIC) {
                    nend = cnt->comp;
                    len = cnt->orig;
                    offs = cnt->offs;
                    if(nend == 0xffffffff || pend == 0xffffffff || offs == 0xffffffff) {
                        e = tmp + sizeof(zip_central_t) + cnt->fnlen + cnt->extlen;
                        if(e > tmp + sizeof(tmp)) e = tmp + sizeof(tmp);
                        for(c = tmp + sizeof(zip_local_t) + cnt->fnlen; c < e; c += *((uint16_t*)(c + 2)) + 4)
                            if(*((uint16_t*)c) == ZIP_EXTZIP64_MAGIC) {
                                ext = (zip_ext64_t *)c;
                                nend = ext->comp;
                                len = ext->orig;
                                if(ext->size > 16) offs = ext->offs;
                                break;
                            }
                    }
                    syspkg_payload_dest(meta, 1, (char*)tmp + sizeof(zip_central_t) + skip, cnt->fnlen, str, &idx);
                    /* for the first entry if it's a directory, set it as top level dir */
                    if(first) {
                        if(idx == OVERRIDELEN && tmp[sizeof(zip_central_t) + cnt->fnlen - 1] == '/') skip = cnt->fnlen;
                        first = 0;
                    }
                    if(*str) {
                        /* don't use S_IFMT here, the zip might be created on a different OS, use common UNIX values */
                        type = (cnt->eattr >> 16) & 0170000;
                        if(tmp[sizeof(zip_central_t) + cnt->fnlen - 1] == '/' || type == 0040000) {
                            syspkg_mkdir(str, 0775);
                        } else if(!type || type == 0100000 || type == 0120000) {
                            memset(&zstrm, 0, sizeof(z_stream));
                            if(inflateInit2(&zstrm, -MAX_WBITS) != Z_OK) { ret = ENOMEM; goto err; }
                            zcmp = ZSTD_createDCtx();
                            if(!zcmp) { inflateEnd(&zstrm); ret = ENOMEM; goto err; }
                            syspkg_seek(g, offs + sizeof(zip_local_t) + cnt->fnlen + cnt->extlen);
                            /* files that fit into FBUFSIZE or binaries up to MAXBCSIZE */
                            if((!idx && len < MAXBCSIZE) || len < FBUFSIZE) {
                                if(nend > FBUFSIZE - 1) {
                                    g->buf = (unsigned char*)realloc(g->buf, nend + 1);
                                    if(!g->buf) { ret = ENOMEM; inflateEnd(&zstrm); ZSTD_freeDCtx(zcmp); goto err; }
                                }
                                g->ilen = nend; g->olen = 0;
                                syspkg_read(g);
                                switch(cnt->method) {
                                    case ZIP_DATA_STORE:
                                        buf = g->buf;
                                        break;
                                    case ZIP_DATA_DEFLATE:
                                        buf = (unsigned char*)malloc(len);
                                        if(!buf) { ret = ENOMEM; inflateEnd(&zstrm); ZSTD_freeDCtx(zcmp); goto err; }
                                        zstrm.next_in = g->buf; zstrm.avail_in = nend;
                                        zstrm.next_out = buf; zstrm.avail_out = len;
                                        inflate(&zstrm, Z_FINISH);
                                        break;
                                    case ZIP_DATA_ZSTD:
                                        buf = (unsigned char*)malloc(len);
                                        if(!buf) { ret = ENOMEM; inflateEnd(&zstrm); ZSTD_freeDCtx(zcmp); goto err; }
                                        zi.src = g->buf; zi.size = nend; zi.pos = 0;
                                        zo.dst = buf; zo.size = len; zo.pos = 0;
                                        ZSTD_decompressStream(zcmp, &zo, &zi);
                                        break;
                                    default:
                                        ret = EBADF; inflateEnd(&zstrm); ZSTD_freeDCtx(zcmp); goto err;
                                }
                                if(type == 0120000) {
                                    syspkg_symlink((char*)buf, str);
                                } else {
                                    if(!idx && ctx->bytecode) (*ctx->bytecode)(&buf, (int*)&len);
                                    if(syspkg_writefileall(str, buf, len)) {
                                        ret = EPERM; inflateEnd(&zstrm); ZSTD_freeDCtx(zcmp); goto err;
                                    }
                                }
                                if(buf != g->buf) free(buf);
                            } else if(type != 0120000) {
                                /* it is a large file which does not fit into FBUFSIZE (only files supported in this mode) */
                                h = syspkg_open(str, 1);
                                if(!h) { ret = EPERM; inflateEnd(&zstrm); ZSTD_freeDCtx(zcmp); goto err; }
                                if(cnt->method == ZIP_DATA_STORE) free(h->buf);
                                g->pos = 0;
                                while(nend > g->pos) {
                                    switch(cnt->method) {
                                        case ZIP_DATA_STORE:
                                            g->ilen = nend - g->pos;
                                            if(g->ilen > FBUFSIZE) g->ilen = FBUFSIZE;
                                            g->olen = 0;
                                            syspkg_read(g);
                                            g->pos += g->olen;
                                            h->buf = g->buf;
                                            h->ilen = g->olen;
                                            h->olen = 0;
                                            syspkg_write(h);
                                            h->buf = NULL;
                                            break;
                                        case ZIP_DATA_DEFLATE:
                                            zstrm.next_out = h->buf; zstrm.avail_out = FBUFSIZE; zstrm.avail_in = 0; r = 0;
                                            do {
                                                if(!zstrm.avail_in) {
                                                    g->ilen = nend - g->pos;
                                                    if(g->ilen < 1) { r = Z_STREAM_END; break; }
                                                    if(g->ilen > FBUFSIZE) g->ilen = FBUFSIZE;
                                                    g->olen = 0;
                                                    if(!syspkg_read(g)) break;
                                                    zstrm.next_in = g->buf;
                                                    zstrm.avail_in = g->olen;
                                                    g->pos += g->olen;
                                                }
                                                r = inflate(&zstrm, Z_NO_FLUSH);
                                            } while(r == Z_OK && zstrm.avail_out > 0);
                                            if(r == Z_OK || r == Z_STREAM_END || !zstrm.avail_out) {
                                                h->olen = 0; h->ilen = FBUFSIZE - zstrm.avail_out;
                                                syspkg_write(h);
                                            }
                                            break;
                                        case ZIP_DATA_ZSTD:
                                            zo.dst = h->buf; zo.pos = 0; zo.size = FBUFSIZE; zi.pos = zi.size = FBUFSIZE; r = 0;
                                            do {
                                                if(zi.pos == zi.size) {
                                                    g->ilen = nend - g->pos;
                                                    if(g->ilen < 1) { r = 0; break; }
                                                    if(g->ilen > FBUFSIZE) g->ilen = FBUFSIZE;
                                                    g->olen = 0;
                                                    if(!syspkg_read(g)) break;
                                                    zi.src = g->buf;
                                                    zi.pos = 0;
                                                    zi.size = g->olen;
                                                    g->pos += g->olen;
                                                }
                                                r = (int)ZSTD_decompressStream(zcmp, &zo, &zi);
                                            } while(!ZSTD_isError(r) && zo.pos < zo.size);
                                            if(!ZSTD_isError(ret) || zo.pos == zo.size) {
                                                h->olen = 0; h->ilen = zo.pos;
                                                syspkg_write(h);
                                            }
                                            break;
                                        default:
                                            ret = EBADF; inflateEnd(&zstrm); ZSTD_freeDCtx(zcmp); goto err;
                                    }
                                }
                                syspkg_close(h);
                            }
                            inflateEnd(&zstrm);
                            ZSTD_freeDCtx(zcmp);
                        }
                    }
                    next += sizeof(zip_central_t) + cnt->fnlen + cnt->extlen;
                    nend = next + sizeof(zip_local_t) + 2;
                    cnt->magic = 0;
                } else if(cnt->magic == ZIP_EOCD32_MAGIC || cnt->magic == ZIP_EOCD64_MAGIC) {
                    next = (uint64_t)-1;
                } else {
                    ret = EBADF;
                    goto err;
                }
            }
        }
        if(ctx->progressbar) (*ctx->progressbar)(inst, ctx->numinstalls, dld, 2 * ctx->dlsize + 1, SYSPKG_MSG_UNPPKG);
        dld += f->olen;
        f->pos += f->olen;
        f->olen = 0;
    }
err:syspkg_close(f);
    syspkg_close(g);
    if(ctx->progressbar) (*ctx->progressbar)(inst, ctx->numinstalls, dld, 2 * ctx->dlsize + 1, SYSPKG_MSG_UNPPKG);
    return ret;
}
