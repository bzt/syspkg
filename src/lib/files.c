/*
 * syspkg/src/lib/files.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief OS dependent file and directory abstraction
 *
 */

#include "internal.h"
#ifdef __WIN32__
#include <windows.h>
#include <shlobj.h>
#include <ntsecapi.h>
/*extern int _fileno(FILE *f);*/
#else
#include <sys/statvfs.h>
#include <sys/stat.h>
#include <dirent.h>
#include <signal.h>
extern int fileno(FILE *f);
extern char *realpath (const char *__name, char *__resolved);
extern ssize_t readlink (const char *__path, char *__buf, size_t __len);
extern int symlink(const char *__path1, const char *__path2);
extern int kill(pid_t pid, int sig);
#endif

#ifdef MBEDTLS_NET_C
#define CHUNKSIZE 16384
#define REQUEST "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: Mozilla/5.0 (Unknown; rv:41.0) Gecko/41.0 Firefox/41.0\r\n" \
    "Accept: */*%s%s\r\n\r\n"
#endif

/**
 * Return 1 if user is admin
 * (this isn't really file related, but OS specific just like the other functions, so keep them together)
 */
int syspkg_isadmin()
{
#if DEBUG
    /* test */
    return 1;
#else
#ifdef __WIN32__
    LSA_OBJECT_ATTRIBUTES attr;
    LSA_HANDLE h;
    NTSTATUS ret;
    memset(&attr, 0, sizeof(LSA_OBJECT_ATTRIBUTES));
    ret = LsaOpenPolicy(NULL, &attr, POLICY_ALL_ACCESS, &h);
    if(ret == ERROR_SUCCESS) LsaClose(h);
    return ret == ERROR_SUCCESS;
#else
    return (getuid() == 0);
#endif
#endif
}

/**
 * Return the language code for the user's local in a malloc'd string
 * (this isn't really file related, but OS specific just like the other functions, so keep them together)
 */
char *syspkg_getlang()
{
#ifdef __MACOS__
    /* because this is Obj-C, compile files.c with
     * CFLAGS += -D__MACOS__ -x objective-c -fobjc-exceptions
     * LDFLAGS += -lobjc
     */
    NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
    return (char*)[[NSString stringWithFormat: @"%@", language] UTF8String];
#else
    /* if environment variable exists, use that */
    char *ret, *lng = getenv("LANG");
#ifdef __WIN32__
    int lid;
    if(!lng) {
        lid = GetUserDefaultLangID(); /* GetUserDefaultUILanguage(); */
        /* see https://docs.microsoft.com/en-us/windows/win32/intl/language-identifier-constants-and-strings */
        switch(lid & 0xFF) {
            case 0x01: lng = "ar"; break;   case 0x02: lng = "bg"; break;   case 0x03: lng = "ca"; break;
            case 0x04: lng = "zh"; break;   case 0x05: lng = "cs"; break;   case 0x06: lng = "da"; break;
            case 0x07: lng = "de"; break;   case 0x08: lng = "el"; break;   case 0x0A: lng = "es"; break;
            case 0x0B: lng = "fi"; break;   case 0x0C: lng = "fr"; break;   case 0x0D: lng = "he"; break;
            case 0x0E: lng = "hu"; break;   case 0x0F: lng = "is"; break;   case 0x10: lng = "it"; break;
            case 0x11: lng = "jp"; break;   case 0x12: lng = "ko"; break;   case 0x13: lng = "nl"; break;
            case 0x14: lng = "no"; break;   case 0x15: lng = "pl"; break;   case 0x16: lng = "pt"; break;
            case 0x17: lng = "rm"; break;   case 0x18: lng = "ro"; break;   case 0x19: lng = "ru"; break;
            case 0x1A: lng = "hr"; break;   case 0x1B: lng = "sk"; break;   case 0x1C: lng = "sq"; break;
            case 0x1D: lng = "sv"; break;   case 0x1E: lng = "th"; break;   case 0x1F: lng = "tr"; break;
            case 0x20: lng = "ur"; break;   case 0x21: lng = "id"; break;   case 0x22: lng = "uk"; break;
            case 0x23: lng = "be"; break;   case 0x24: lng = "sl"; break;   case 0x25: lng = "et"; break;
            case 0x26: lng = "lv"; break;   case 0x27: lng = "lt"; break;   case 0x29: lng = "fa"; break;
            case 0x2A: lng = "vi"; break;   case 0x2B: lng = "hy"; break;   case 0x2D: lng = "bq"; break;
            case 0x2F: lng = "mk"; break;   case 0x36: lng = "af"; break;   case 0x37: lng = "ka"; break;
            case 0x38: lng = "fo"; break;   case 0x39: lng = "hi"; break;   case 0x3A: lng = "mt"; break;
            case 0x3C: lng = "gd"; break;   case 0x3E: lng = "ms"; break;   case 0x3F: lng = "kk"; break;
            case 0x40: lng = "ky"; break;   case 0x45: lng = "bn"; break;   case 0x47: lng = "gu"; break;
            case 0x4D: lng = "as"; break;   case 0x4E: lng = "mr"; break;   case 0x4F: lng = "sa"; break;
            case 0x53: lng = "kh"; break;   case 0x54: lng = "lo"; break;   case 0x56: lng = "gl"; break;
            case 0x5E: lng = "am"; break;   case 0x62: lng = "fy"; break;   case 0x68: lng = "ha"; break;
            case 0x6D: lng = "ba"; break;   case 0x6E: lng = "lb"; break;   case 0x6F: lng = "kl"; break;
            case 0x7E: lng = "br"; break;   case 0x92: lng = "ku"; break;   case 0x09: default: lng = "en"; break;
        }
    }
#endif
    if(!(ret = malloc(6))) return NULL;
    memset(ret, 0, 6);
    if(lng) {
        ret[0] = lng[0];
        ret[1] = lng[1];
        if(lng[2] == '_' && (toupper(lng[0]) != lng[3] || toupper(lng[1]) != lng[4])) {
            ret[2] = lng[2];
            ret[3] = lng[3];
            ret[4] = lng[4];
        }
    }
    return ret;
#endif
}

/**
 * Return the user's home directory in a malloc'd string
 */
char *syspkg_gethome()
{
#ifdef __WIN32__
    char *tmp = NULL, *c;
    tmp = (char*)malloc(MAX_PATH);
    if(tmp) {
        if(!SHGetFolderPathA(HWND_DESKTOP, CSIDL_PROFILE, NULL, 0, tmp)) return tmp;
        if(!SHGetFolderPathA(HWND_DESKTOP, CSIDL_DESKTOPDIRECTORY, NULL, 0, tmp)) {
            c = strrchr(tmp, SEP[0]);
            if(c) *c = 0;
            return tmp;
        }
        free(tmp);
    }
    return NULL;
#else
    char *dir, *tmp = NULL;
    dir = tmp = getenv("HOME");
    if(!dir) {
        tmp = getenv("LOGNAME");
        if(!tmp) return NULL;
        dir = malloc(strlen(tmp) + 8);
        if(!dir) { free(tmp); return NULL; }
        sprintf(dir, SEP "home" SEP "%s", tmp);
        free(dir);
    }
    return tmp;
#endif
}

/**
 * Check or acquire exclusive access
 */
int syspkg_lock(int acquire)
{
    char fn[MAXPATHSIZE], tmp[32];
    uint64_t pid;
    FILE *f;
    strcpy(fn, PKGDIR);
    syspkg_mkdir(fn, 0755);
    sprintf(fn, "%s.lock", PKGDIR);
    f = fopen(fn, "r");
    if(f) {
        tmp[0] = 0;
        fgets(tmp, sizeof(tmp), f);
        fclose(f);
        pid = atol(tmp);
        if(pid
#ifndef __WIN32__
         && !kill(pid, SIGCONT)
#endif
        ) return EEXIST;
    }
    if(!acquire) return SUCCESS;
    f = fopen(fn, "w");
    if(f) {
        fprintf(f, "%ld",
#ifdef __WIN32__
            1UL
#else
            (uint64_t)getpid()
#endif
        );
        fclose(f);
        return SUCCESS;
    }
    return EPERM;
}

/**
 * Release exclusive access
 */
int syspkg_unlock()
{
    char fn[MAXPATHSIZE];
    FILE *f;
    int ret = SUCCESS;
    sprintf(fn, "%s.lock", PKGDIR);
    f = fopen(fn, "r");
    if(f) {
        fclose(f);
        ret = unlink(fn);
    }
    return !ret ? SUCCESS : EPERM;
}

/**
 * Get canonized file name
 */
char *syspkg_realpath(const char *path)
{
#ifdef __WIN32__
    char *buf = malloc(MAXPATHSIZE);
    if(buf) GetFullPathNameA(path, MAXPATHSIZE, buf, NULL);
    return buf;
#else
    return realpath(path, NULL);
#endif
}

/**
 * Read symlink's target
 */
int syspkg_readlink(const char *path, char *buf, size_t len)
{
#ifdef __WIN32__
    HANDLE h = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    int r = 0;
    if(h != INVALID_HANDLE_VALUE) {
        r = GetFinalPathNameByHandle(h, buf, len, FILE_NAME_NORMALIZED);
        CloseHandle(h);
    }
    return r;
#else
    return readlink(path, buf, len);
#endif
}

/**
 * Create symlink
 */
int syspkg_symlink(const char *target, const char *path)
{
#ifdef __WIN32__
    return CreateSymbolicLinkA(path, target, GetFileAttributesA(target) & FILE_ATTRIBUTE_DIRECTORY ?
        SYMBOLIC_LINK_FLAG_DIRECTORY : 0);
#else
    return symlink(target, path);
#endif
}

/**
 * Return file system info for a path
 */
int syspkg_statfs(char *path, uint64_t *fsid, int64_t *freeblks, int *blksiz)
{
    int ret = SUCCESS;
#ifndef __WIN32__
    struct statvfs vfs;
    char *c;
#else
    DWORD spc = 0, bps = 0, fc = 0, tc;
    char tmp[MAXPATHSIZE];
#endif
    if(!path || !*path || !fsid || !blksiz || !freeblks) return EINVAL;
    *fsid = *blksiz = *freeblks = 0;
#ifdef __WIN32__
    GetFullPathNameA(path, MAXPATHSIZE, tmp, NULL);
    if(tmp[1] == ':') {
        tmp[3] = 0; /* should only remain "C:\" or something */
        if(GetDiskFreeSpaceA(tmp, &spc, &bps, &fc, &tc)) {
            *fsid = (uint64_t)tmp[0];
            *blksiz = (int)(spc * bps);
            *freeblks = (int64_t)fc;
        }
    }
#else
    memset(&vfs, 0, sizeof(struct statvfs));
    do {
        errno = 0;
        ret = statvfs(path, &vfs);
        if(ret && errno == ENOENT) {
            c = strrchr(path, SEP[0]);
            if(c && c > path) { *c = 0; continue; }
            break;
        }
    } while(ret);
    if(!ret) {
        *fsid = vfs.f_fsid;
        *blksiz = vfs.f_bsize;
        *freeblks = vfs.f_bfree;
    }
#endif
    return ret;
}

/**
 * Recursively walk a directory
 */
static char full[MAXPATHSIZE+1];
static int skipbytes;
#ifndef __WIN32__
static struct stat st;
#endif
int syspkg_walkdir(char *directory, int parent, void *data, walk_cb pre, walk_cb suf)
{
    /* this is recursive, so do not allocate big arrays on its local stack */
#ifdef __WIN32__
    WIN32_FIND_DATA ffd;
    HANDLE h;
    ULONG r;
    int64_t fsiz;
#else
    DIR *dir;
    struct dirent *ent;
#endif
    int i, type;

    if(!parent) {
        i = strlen(directory);
        if(!i) return -1;
        strcpy(full, directory);
        if(full[i - 1] == SEP[0])
            i--;
        full[i] = 0;
        skipbytes = i + 1;
    } else
        i = strlen(full);
#ifdef __WIN32__
    h = FindFirstFileA(full, &ffd);
    if(h != INVALID_HANDLE_VALUE) {
        do {
            if(strcmp(ffd.cFileName, ".") && strcmp(ffd.cFileName, "..") && strcmp(ffd.cFileName, ".git")) {
                strncpy(full + i, SEP, MAXPATHSIZE - i - 1);
                strncpy(full + i + 1, ffd.cFileName, MAXPATHSIZE - i - 2);
                r = GetFileAttributesA(full);
                type = r & FILE_ATTRIBUTE_DIRECTORY ? SYSPKG_WALK_TYPE_DIR :
                    (r & FILE_ATTRIBUTE_REPARSE_POINT ? SYSPKG_WALK_TYPE_LNK : SYSPKG_WALK_TYPE_REG);
                fsiz = syspkg_filesize(full);
                if(pre) (*pre)(type, fsiz, 0, full, full + skipbytes, data);
                if(type == SYSPKG_WALK_TYPE_DIR)
                    syspkg_walkdir(NULL, parent+1, data, pre, suf);
                if(suf) (*suf)(type, fsiz, 0, full, full + skipbytes, data);
            }
        } while(FindNextFileA(h, &ffd) != 0);
        FindClose(h);
    }
#else
    if ((dir = opendir(full)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if(!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, "..") || !strcmp(ent->d_name, ".git")) continue;
            strncpy(full + i, SEP, MAXPATHSIZE - i - 1);
            strncpy(full + i + 1, ent->d_name, MAXPATHSIZE - i - 2);
            if(stat(full, &st)) continue;
            if(!S_ISDIR(st.st_mode) && !S_ISLNK(st.st_mode) && !S_ISREG(st.st_mode)) continue;
            type = S_ISDIR(st.st_mode) ? SYSPKG_WALK_TYPE_DIR :
                (S_ISLNK(st.st_mode) ? SYSPKG_WALK_TYPE_LNK : (S_ISREG(st.st_mode) ? SYSPKG_WALK_TYPE_REG : -1));
            if(type != -1) {
                if(pre) (*pre)(type, (int64_t)st.st_size, (uint64_t)st.st_mtime, full, full + skipbytes, data);
                if(type == SYSPKG_WALK_TYPE_DIR)
                    syspkg_walkdir(NULL, parent+1, data, pre, suf);
                if(suf) (*suf)(type, (int64_t)st.st_size, (uint64_t)st.st_mtime, full, full + skipbytes, data);
            }
            full[i] = 0;
        }
        closedir(dir);
    }
#endif
    return 0;
}

/**
 * Recursively create a directory
 */
#ifndef __WIN32__
#define mymkdir(d, p)   mkdir(d, p)
#else
#define mymkdir(d, p)   mkdir(d)
#endif
int syspkg_mkdir(char *dir, int perm)
{
    char *c;
#ifdef __WIN32__
    (void)perm;
#endif
    if(!dir || !*dir) return EINVAL;
    for(c = dir; *c; c++)
        if(c > dir && *c == SEP[0] && *(c+1)) { *c = 0; mymkdir(dir, perm); *c = SEP[0]; }
    mymkdir(dir, perm);
    return SUCCESS;
}

/**
 * Remove a file
 */
int syspkg_rm(char *fn)
{
    if(!fn || !*fn) return EINVAL;
    return remove(fn) ? EPERM : SUCCESS;
}

/**
 * Recursively remove a directory
 */
void _syspkg_rmrf(int type, int64_t size, uint64_t mtime, char *full, char *name, void *data)
{
    (void)size; (void)mtime; (void)name; (void)data;
    if(type == SYSPKG_WALK_TYPE_DIR)
        rmdir(full);
    else
        unlink(full);
}
int syspkg_rmrf(char *fn)
{
    if(!fn || !*fn) return EINVAL;
    syspkg_walkdir(fn, 0, NULL, NULL, _syspkg_rmrf);
    return syspkg_rm(fn);
}

/**
 * Returns 1 if path is a directory
 */
int syspkg_isdir(char *path)
{
#ifdef __WIN32__
    return GetFileAttributesA(path) & FILE_ATTRIBUTE_DIRECTORY ? 1 : 0;
#else
    return (path && *path && !stat(path, &st) && S_ISDIR(st.st_mode));
#endif
}

/**
 * Return -1 if path doesn't exists, otherwise the size
 */
int64_t syspkg_filesize(char *path)
{
#ifndef __WIN32__
    if(!path || !*path || stat(path, &st)) return -1;
    return (int64_t)st.st_size;
#else
    FILE *f;
    int64_t size;
    if(!path || !*path || !(f = fopen(path, "r"))) return -1;
    size = (int64_t)_filelengthi64(_fileno(f));
    fclose(f);
    return size;
#endif
}

/**
 * Renames a file
 */
int syspkg_rename(char *oldpath, char *newpath)
{
     return rename(oldpath, newpath);
}

/**
 * Replace variables in URLs
 */
int syspkg_url(char *mask, char *arch, char *ver, char *release, char *osver, char *url)
{
    char *s, *d, *e;
    if(!mask || !url) return EINVAL;
    for(s = mask, d = url; *s > ' ' && *s != '#'; ) {
        if(*s == '$') {
            s++;
            if(!memcmp(s, "ARCH", 4)) {
                s += 4;
                if(arch && *arch) {
                    for(e = arch; *e && *e != '=' && *e != ' ' && *e != '\n';)
                        *d++ = *e++;
                } else { strcpy(d, "any"); d += 3; }
            } else
            if(!memcmp(s, "VERSION", 7)) {
                s += 7; if(ver && *ver) { strcpy(d, ver); d += strlen(ver); }
            } else
            if(!memcmp(s, "RELEASE", 7)) {
                s += 7; if(release && *release) { strcpy(d, release); d += strlen(release); }
            } else
            if(!memcmp(s, "OSVER", 5)) {
                s += 5; if(osver && *osver) { strcpy(d, osver); d += strlen(osver); }
            }
        } else
            *d++ = *s++;
    }
    *d = 0;
    return d - url;
}

/**
 * Parse an URL into components
 */
int syspkg_parseurl(char *url, char **hostname, int *port, char **path)
{
    char *s, *d;
    size_t l;

    while(url && *url == ' ') url++;
    if(!url || memcmp(url, "https://", 8) || !hostname || !port || !path) return EINVAL;
    *port = 443;
    l = strlen(url);
    *hostname = realloc(*hostname, l);
    if(!*hostname) return ENOMEM;
    *path = realloc(*path, l);
    if(!*path) { free(*hostname); *hostname = NULL; return ENOMEM; }
    for(s = url + 8, d = *hostname; *s && *s != ':' && *s !='/'; s++)
        *d++ = *s;
    *d = 0;
    if(*s == ':') { *port = (int)syspkg_validsize(s + 1); if(*port > 65534) { *port = 65534; } while(*s && *s != '/') s++; }
    if(*s == '/') s++;
    for(d = *path; *s > ' ' && *s != '#'; )
        *d++ = *s++;
    *d = 0;
    return SUCCESS;
}

/**
 * Get a small file from the server or local file (typically used for certs, metainfo and URL lists)
 */
int syspkg_readfileall(char *url, unsigned char **buf, size_t *len)
{
#ifdef MBEDTLS_NET_C
    unsigned char *dst, *end = NULL;
    char *hostname = NULL, *path = NULL, *req = NULL, portstr[8], tmp[MAXPATHSIZE], *cookie = NULL, *s, *d;
    int port = 443, ret = SUCCESS, r, cnt, l, redir = 8;
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config conf;
    mbedtls_net_context server_fd;
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
#endif
    FILE *f;

    *buf = NULL; *len = 0;
    if(!url) return EINVAL;
    while(url && *url == ' ') url++;
    if(memcmp(url, "https://", 8)) {
        if(!memcmp(url, "file://", 7)) url += 7;
        if(SEP[0] != '/') {
            for(path = tmp; *url; path++, url++)
                *path = (*url == '/') ? SEP[0] : *url;
            *path = 0;
            url = tmp;
        }
        f = fopen(url,"r");
        if(f) {
            fseek(f, 0L, SEEK_END);
            *len = (size_t)ftell(f);
            fseek(f, 0L, SEEK_SET);
            *buf = (unsigned char*)malloc((*len) + 1);
            if(!*buf) { *len = 0; return ENOMEM; }
            fread(*buf, *len, 1, f);
            (*buf)[*len] = 0;
            fclose(f);
            return SUCCESS;
        }
        return ENOENT;
    }
#ifdef MBEDTLS_NET_C
    mbedtls_entropy_init( &entropy );
    mbedtls_ctr_drbg_init( &ctr_drbg );
    mbedtls_entropy_add_source(&entropy, mbedtls_hardware_poll, NULL, 32, MBEDTLS_ENTROPY_SOURCE_STRONG);
    mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, (const unsigned char*)"salt", 5);
    mbedtls_entropy_free( &entropy );

again:
    *buf = NULL; *len = 0;
    if(syspkg_parseurl(url, &hostname, &port, &path))
        return EINVAL;
    sprintf(portstr, "%d", port);

    mbedtls_net_init( &server_fd );
    mbedtls_ssl_init( &ssl );
    mbedtls_ssl_config_init( &conf );
    if(mbedtls_net_connect( &server_fd, hostname, portstr, MBEDTLS_NET_PROTO_TCP ) != 0 || mbedtls_ssl_config_defaults( &conf,
        MBEDTLS_SSL_IS_CLIENT, MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT) != 0)
        { ret = EACCES; goto end; }
    mbedtls_ssl_conf_authmode( &conf, MBEDTLS_SSL_VERIFY_OPTIONAL );
    mbedtls_ssl_conf_rng( &conf, mbedtls_ctr_drbg_random, &ctr_drbg );
    if(mbedtls_ssl_setup( &ssl, &conf ) != 0 || mbedtls_ssl_set_hostname( &ssl, hostname ) != 0)
        { ret = EACCES; goto end; }

    mbedtls_ssl_set_bio( &ssl, &server_fd, mbedtls_net_send, mbedtls_net_recv, NULL );

    for(cnt = 1000; ( r = mbedtls_ssl_handshake( &ssl ) ) != 0; cnt-- ) {
        if((r != MBEDTLS_ERR_SSL_WANT_READ && r != MBEDTLS_ERR_SSL_WANT_WRITE) || cnt < 1) { ret = EFAULT; goto end; }
    }

    req = malloc(strlen(hostname) + strlen(path) + (cookie && *cookie ? strlen(cookie) : 0) + 128);
    if(!req) { ret = ENOMEM; goto end; }
    l = sprintf(req, REQUEST, path, hostname, cookie && *cookie ? "\r\nCookie: " : "", cookie && *cookie ? cookie : "");

    for(cnt = 1000; ( r = mbedtls_ssl_write(&ssl, (const unsigned char*)req, l) ) <= 0; cnt-- ) {
        if((r != MBEDTLS_ERR_SSL_WANT_READ && r != MBEDTLS_ERR_SSL_WANT_WRITE) || cnt < 1) { free(req); ret = EIO; goto end; }
    }
    free(req);
    *buf = dst = malloc(CHUNKSIZE + 1);
    if(!*buf) { ret = ENOMEM; goto end; }

    end = NULL;
    do {
        *dst = 0;
        l = mbedtls_ssl_read( &ssl, dst, CHUNKSIZE );
        if(l == MBEDTLS_ERR_SSL_WANT_READ || l == MBEDTLS_ERR_SSL_WANT_WRITE) continue;
        if(!l || l == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) break;
        if(l < 0 ) { ret = EIO; goto end; }
        (*len) += l;
        dst -= (uintptr_t)(*buf);
        *buf = realloc(*buf, (*len) + CHUNKSIZE + 1);
        if(!*buf) { ret = ENOMEM; *len = 0; goto end; }
        dst += (uintptr_t)(*buf) + l;
        if(!end) {
            end = (unsigned char*)strstr((const char*)(*buf), "\r\n\r\n");
            if(end) {
                *end = 0;
                for(s = (char*)(*buf); s + 14 < (char*)end && *s; s++)
                    if(!memcmp(s, "\nSet-Cookie:", 12)) {
                        for(s += 12; *s == ' '; s++);
                        for(d = s; *d && *d != '\r' && *d != '\n' && *d != ';'; d++);
                        l = cookie ? strlen(cookie) : 0;
                        cookie = (char*)realloc(cookie, l + (d - s) + 3);
                        if(cookie) {
                            if(l) { memcpy(cookie + l, "; ", 2); l += 2; }
                            memcpy(cookie + l, s, d - s);
                            cookie[l + (d - s)] = 0;
                        }
                    }
                l = dst - (end + 4);
                dst = (*buf);
                while(dst < end && *dst && *dst != '\n' && *dst != ' ') dst++;
                if(!*dst || memcmp(dst, " 200", 4)) { end = NULL; break; }
                memcpy(*buf, end + 4, l);
                dst = (*buf) + l;
                *len = l;
            }
        }
    } while(1);

    mbedtls_ssl_close_notify( &ssl );

end:
    mbedtls_net_free( &server_fd );
    mbedtls_ssl_free( &ssl );
    mbedtls_ssl_config_free( &conf );
    mbedtls_ctr_drbg_free( &ctr_drbg );
    free(hostname);
    free(path);
    if(ret != SUCCESS) {
        if(*buf) free(*buf);
        *buf = NULL; *len = 0;
    } else
    if(!end && *buf) {
        /* handle http redirects */
        end = (unsigned char*)strstr((const char*)(*buf), "Location: ");
        if(end && --redir) {
            for(url = (char*)end + 10; *url == ' '; url++);
            for(end = (unsigned char*)url; *end && *end != '\r' && *end != '\n'; end++);
            *end = 0;
            goto again;
        }
        ret = ENOENT;
    }
    if(cookie) free(cookie);
    return ret;
#else
    return ENOENT;
#endif
}

/**
 * Write a small file to local file (typically used for certs, metainfo and URL lists)
 */
int syspkg_writefileall(char *fn, unsigned char *buf, size_t len)
{
    char tmp[MAXPATHSIZE], *d;
    FILE *f;
    if(!memcmp(fn, "file://", 7)) fn += 7;
    if(SEP[0] != '/') {
        for(d = tmp; *fn; d++, fn++)
            *d = (*fn == '/') ? SEP[0] : *fn;
        *d = 0;
        fn = tmp;
    }
    f = fopen(fn, "wb");
    if(!f) return EPERM;
    if(len > 0) fwrite(buf, len, 1, f);
    fclose(f);
    return SUCCESS;
}

/**
 * File abstraction for opening big files
 */
syspkg_file_t *syspkg_open(char *url, int wr)
{
    syspkg_file_t *f;
#ifdef MBEDTLS_NET_C
    unsigned char *dst, *end = NULL;
    char *hostname = NULL, *path = NULL, *req = NULL, portstr[8], *cookie = NULL, *s;
    int port = 443, r, cnt, l, redir = 8;
    mbedtls_entropy_context entropy;
#endif
    char tmp[MAXPATHSIZE], *d;
    if(!url) return NULL;
    while(url && *url == ' ') url++;
    f = (syspkg_file_t*)malloc(sizeof(syspkg_file_t));
    if(!f) return NULL;
    memset(f, 0, sizeof(syspkg_file_t));

    if(memcmp(url, "https://", 8)) {
        if(!memcmp(url, "file://", 7)) url += 7;
        if(SEP[0] != '/') {
            for(d = tmp; *url; d++, url++)
                *d = (*url == '/') ? SEP[0] : *url;
            *d = 0;
            url = tmp;
        }
        /* local file, read-write */
        f->f = fopen(url, wr ? "w+b" : "rb");
        if(!f->f) {
err2:       free(f);
            return NULL;
        }
        if(!(f->buf = (unsigned char*)malloc(FBUFSIZE + 1))) {
            fclose(f->f);
            goto err2;
        }
        f->ilen = FBUFSIZE;
#ifdef __WIN32__
        f->filesize = (uint64_t)_filelengthi64(_fileno(f->f));
#else
        if(!fstat(fileno(f->f), &st)) f->filesize = (uint64_t)st.st_size;
#endif
        f->full = syspkg_realpath(url);
    } else {
#ifdef MBEDTLS_NET_C
        /* remote file, read-only */
        mbedtls_entropy_init( &entropy );
        mbedtls_ctr_drbg_init( &f->ctr_drbg );
        mbedtls_entropy_add_source(&entropy, mbedtls_hardware_poll, NULL, 32, MBEDTLS_ENTROPY_SOURCE_STRONG);
        mbedtls_ctr_drbg_seed(&f->ctr_drbg, mbedtls_entropy_func, &entropy, (const unsigned char*)"salt", 5);
        mbedtls_entropy_free( &entropy );
again:  if(wr || syspkg_parseurl(url, &hostname, &port, &path)) goto err2;
        sprintf(portstr, "%d", port);
        mbedtls_net_init( &f->server_fd );
        mbedtls_ssl_init( &f->ssl );
        mbedtls_ssl_config_init( &f->conf );
        if(mbedtls_net_connect( &f->server_fd, hostname, portstr, MBEDTLS_NET_PROTO_TCP ) != 0 ||
            mbedtls_ssl_config_defaults( &f->conf, MBEDTLS_SSL_IS_CLIENT, MBEDTLS_SSL_TRANSPORT_STREAM,
            MBEDTLS_SSL_PRESET_DEFAULT) != 0) {
err:            mbedtls_net_free( &f->server_fd );
                mbedtls_ssl_free( &f->ssl );
                mbedtls_ssl_config_free( &f->conf );
                if(f->buf) free(f->buf);
                if(hostname) free(hostname);
                if(path) free(path);
                if(cookie) free(cookie);
                goto err2;
        }
        mbedtls_ssl_conf_authmode( &f->conf, MBEDTLS_SSL_VERIFY_OPTIONAL );
        mbedtls_ssl_conf_rng( &f->conf, mbedtls_ctr_drbg_random, &f->ctr_drbg );
        if(mbedtls_ssl_setup( &f->ssl, &f->conf ) != 0 || mbedtls_ssl_set_hostname( &f->ssl, hostname ) != 0) goto err;

        mbedtls_ssl_set_bio( &f->ssl, &f->server_fd, mbedtls_net_send, mbedtls_net_recv, NULL );

        for(cnt = 1000; ( r = mbedtls_ssl_handshake( &f->ssl ) ) != 0; cnt-- ) {
            if((r != MBEDTLS_ERR_SSL_WANT_READ && r != MBEDTLS_ERR_SSL_WANT_WRITE) || cnt < 1) goto err;
        }

        req = malloc(strlen(hostname) + strlen(path) + (cookie && *cookie ? strlen(cookie) : 0) + 128);
        if(!req) goto err;
        l = sprintf(req, REQUEST, path, hostname, cookie && *cookie ? "\r\nCookie: " : "", cookie && *cookie ? cookie : "");
        free(hostname); hostname = NULL;
        free(path); path = NULL;

        for(cnt = 1000; ( r = mbedtls_ssl_write(&f->ssl, (const unsigned char*)req, l) ) <= 0; cnt-- ) {
            if((r != MBEDTLS_ERR_SSL_WANT_READ && r != MBEDTLS_ERR_SSL_WANT_WRITE) || cnt < 1) { free(req); goto err; }
        }
        free(req);

        f->buf = dst = malloc(CHUNKSIZE + 1);
        if(!f->buf) goto err;

        end = NULL;
        do {
            *dst = 0;
            l = mbedtls_ssl_read( &f->ssl, dst, CHUNKSIZE );
            if(l == MBEDTLS_ERR_SSL_WANT_READ || l == MBEDTLS_ERR_SSL_WANT_WRITE) continue;
            if(!l || l == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) break;
            if(l < 0 ) goto err;
            (f->olen) += l;
            dst -= (uintptr_t)(f->buf);
            f->buf = realloc(f->buf, (f->olen) + CHUNKSIZE + 1);
            if(!f->buf) goto err;
            dst += (uintptr_t)(f->buf) + l;
            if(!end) {
                end = (unsigned char*)strstr((const char*)(f->buf), "\r\n\r\n");
                if(end) {
                    *end = 0;
                    for(s = (char*)(f->buf); s + 14 < (char*)end && *s; s++)
                        if(!memcmp(s, "\nSet-Cookie:", 12)) {
                            for(s += 12; *s == ' '; s++);
                            for(d = s; *d && *d != '\r' && *d != '\n' && *d != ';'; d++);
                            l = cookie ? strlen(cookie) : 0;
                            cookie = (char*)realloc(cookie, l + (d - s) + 3);
                            if(cookie) {
                                if(l) { memcpy(cookie + l, "; ", 2); l += 2; }
                                memcpy(cookie + l, s, d - s);
                                cookie[l + (d - s)] = 0;
                            }
                        }
                    l = dst - (end + 4);
                    if((dst = (unsigned char*)strstr((const char*)(f->buf), "Content-length: ")))
                        f->filesize = syspkg_validsize((char*)dst + 16);
                    dst = (f->buf);
                    while(dst < end && *dst && *dst != '\n' && *dst != ' ') dst++;
                    if(!*dst || memcmp(dst, " 200", 4)) { end = NULL; break; }
                    memcpy(f->buf, end + 4, l);
                    f->olen = l;
                    break;
                }
            }
        } while(1);
        if(!end && f->buf) {
            /* handle http redirects */
            end = (unsigned char*)strstr((const char*)(f->buf), "Location: ");
            if(end && --redir) {
                for(url = (char*)end + 10; *url == ' '; url++);
                for(end = (unsigned char*)url; *end && *end != '\r' && *end != '\n'; end++);
                *end = 0;
                f->olen = f->filesize = 0;
                mbedtls_ssl_close_notify( &f->ssl );
                mbedtls_net_free( &f->server_fd );
                mbedtls_ssl_free( &f->ssl );
                mbedtls_ssl_config_free( &f->conf );
                goto again;
            }
            goto err;
        }
        f->ilen = 65536;
        f->buf = (unsigned char*)realloc(f->buf, f->ilen + 1);
        if(!f->buf) goto err;
        if(cookie) free(cookie);
#else
        free(f);
        return NULL;
#endif
    }
    return f;
}

/**
 * File abstraction for seeking in big files
 */
int syspkg_seek(syspkg_file_t *f, int64_t offs)
{
#if !defined(__WIN32__) && !defined(MACOS)
    fpos_t pos = {0};
    pos.__pos = offs;
#else
    fpos_t pos = (fpos_t)offs;
#endif
    if(!f || !f->f) return EINVAL;
    return !fsetpos(f->f, &pos) ? SUCCESS : EIO;
}

/**
 * File abstraction for reading big files
 */
int syspkg_read(syspkg_file_t *f)
{
#ifdef MBEDTLS_NET_C
    unsigned char *dst;
    int l;
#endif
    if(!f || !f->buf || !f->ilen) return EINVAL;
    if(f->f) {
        if(f->olen >= FBUFSIZE) return ENOSPC;
        if(f->olen + f->ilen > FBUFSIZE) f->ilen = FBUFSIZE - f->olen;
        f->olen = fread(f->buf + f->olen, 1, f->ilen, f->f);
        if(!f->olen) return ENOSPC;
    } else {
#ifdef MBEDTLS_NET_C
        /* tricky, because we might have already read a bit of file data while reading in the headers in syspkg_open... */
        dst = f->buf + f->olen;
        f->olen = 0;
        do {
            *dst = 0;
            l = f->ilen - f->olen;
            if(!l) break;
            l = mbedtls_ssl_read( &f->ssl, dst, l < CHUNKSIZE ? l : CHUNKSIZE );
            if(l == MBEDTLS_ERR_SSL_WANT_READ || l == MBEDTLS_ERR_SSL_WANT_WRITE) continue;
            if(l < 0 || l == MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY) { f->ilen = 0; return SUCCESS; }
            if(l < 0 ) { f->ilen = 0; return EIO; }
            (f->olen) += l;
            dst += l;
        } while(1);
#else
        return EIO;
#endif
    }
    f->buf[f->olen] = 0;
    return SUCCESS;
}

/**
 * File abstraction for writing big files
 */
int syspkg_write(syspkg_file_t *f)
{
    if(!f || !f->f || !f->buf || !f->ilen) return EINVAL;
    f->olen = fwrite(f->buf, 1, f->ilen, f->f);
    return SUCCESS;
}

/**
 * File abstraction for closing big files
 */
int syspkg_close(syspkg_file_t *f)
{
    if(!f) return EINVAL;
    if(f->buf) free(f->buf);
    if(f->full) free(f->full);
    if(f->f) {
        fclose(f->f);
    } else {
#ifdef MBEDTLS_NET_C
        mbedtls_ssl_close_notify( &f->ssl );
        mbedtls_net_free( &f->server_fd );
        mbedtls_ssl_free( &f->ssl );
        mbedtls_ssl_config_free( &f->conf );
        mbedtls_ctr_drbg_free( &f->ctr_drbg );
#endif
    }
    memset(f, 0, sizeof(syspkg_file_t));
    free(f);
    return SUCCESS;
}
