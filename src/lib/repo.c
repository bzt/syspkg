/*
 * syspkg/src/lib/repo.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Repository related functions
 *
 */

#include "internal.h"

/**
 * Get package list of a repository
 */
syspkg_repo_t *syspkg_getrepo(char *url)
{
    char *pkgurls = NULL, *c, cn[MAXCNSIZE+32];
    syspkg_repo_t *ret = NULL;
    size_t len;
    int n = 0;
    mbedtls_x509_crt crt;

    if(!url || !*url) return NULL;
    mbedtls_x509_crt_init( &crt );
    memset(cn, 0, sizeof(cn));
    if(syspkg_readfileall(url, (unsigned char **)&pkgurls, &len) || !pkgurls) goto err;
    for(c = pkgurls; c < pkgurls + len && *c && memcmp(c, PEM_BEG_CRT, 27); c++)
        if(*c == '\n') n++;
    if(c > pkgurls && *(c - 1) != '\n') n++;
    if(c == pkgurls || !*c || memcmp(c, PEM_BEG_CRT, 27)) goto err;
    *(c - 1) = 0;
    if(mbedtls_x509_crt_parse(&crt, (unsigned char *)c, len + 1 - (c - pkgurls)) != 0 ||
        mbedtls_x509_dn_gets(cn, sizeof(cn), &crt.subject) < 3 || !cn[0] || !crt.ca_istrue ||
        !(crt.key_usage & MBEDTLS_X509_KU_KEY_CERT_SIGN)) goto err;
    mbedtls_x509_crt_free(&crt);
    ret = (syspkg_repo_t*)malloc(sizeof(syspkg_repo_t));
    if(!ret) goto err;
    memset(ret, 0, sizeof(syspkg_repo_t));
    ret->url = (char*)malloc(strlen(url) + 1);
    if(!ret->url) goto err;
    strcpy(ret->url, url);
    ret->cn = (char*)malloc(strlen(cn) + 1);
    if(!ret->cn) goto err;
    strcpy(ret->cn, cn);
    ret->num = n;
    return ret;
err:
    mbedtls_x509_crt_free( &crt );
    if(ret) {
        if(ret->url) free(ret->url);
        if(ret->cn) free(ret->cn);
        free(ret);
    }
    if(pkgurls) free(pkgurls);
    return NULL;
}

/**
 * Add an url to the repository list
 */
int syspkg_addrepo(syspkg_ctx_t *ctx, char *url)
{
    syspkg_repo_t *repo;
    int i;

    if(!ctx || !url || !*url || (memcmp(url, "https://", 8) && memcmp(url, "file:///", 8))) return EINVAL;
    for(i = 0; ctx->repourls && ctx->repourls[i]; i++)
        if(!strcmp(ctx->repourls[i], url)) return EEXIST;
    if(!syspkg_isadmin()) return EACCES;
    repo = syspkg_getrepo(url);
    if(!repo) return ENOENT;
    url = repo->url;
    free(repo->cn);
    if(repo->num < 1) { free(repo); return ENOENT; }
    free(repo);
    ctx->repourls = (char**)realloc(ctx->repourls, (i + 2) * sizeof(char*));
    if(!ctx->repourls) return ENOMEM;
    ctx->repourls[i] = url;
    ctx->repourls[i + 1] = NULL;
    return syspkg_savecfg(ctx);
}

/**
 * Remove a repository
 */
int syspkg_delrepo(syspkg_ctx_t *ctx, char *name)
{
    syspkg_repo_t *repo;
    int i = 0;

    if(!ctx || !name || !*name) return EINVAL;
    if(!syspkg_isadmin()) return EACCES;

    /* see if a repository url or canonical name given */
    for(i = 0; ctx->repourls && ctx->repourls[i]; i++) {
        if(name[0] == 'C' && name[1] == 'N' && name[2] == '=') {
            repo = syspkg_getrepo(ctx->repourls[i]);
            if(!repo) continue;
            if(repo->url) free(repo->url);
            if(repo->cn && !strcmp(repo->cn, name)) {
                free(repo->cn);
                free(repo);
                goto removerepo;
            }
            free(repo->cn);
            free(repo);
        } else if(!strcmp(ctx->repourls[i], name)) {
removerepo: free(ctx->repourls[i]);
            for( ; ctx->repourls[i]; i++)
                ctx->repourls[i] = ctx->repourls[i + 1];
            return syspkg_savecfg(ctx);
        }
    }
    return ENOENT;
}

/**
 * Get list of repositories
 */
int syspkg_listrepo(syspkg_ctx_t *ctx)
{
    syspkg_repo_t *repo;
    int i = 0;

    if(!ctx) return EINVAL;
    if(ctx->repos) {
        for(i = 0; ctx->repos[i].url; i++)
            if(ctx->repos[i].cn) free(ctx->repos[i].cn);
        free(ctx->repos);
    }
    for(i = 0; ctx->repourls && ctx->repourls[i]; i++);
    if(!i) return ENOENT;
    ctx->repos = (syspkg_repo_t*)malloc((i + 1) * sizeof(syspkg_repo_t));
    if(!ctx->repos) return ENOMEM;
    memset(ctx->repos, 0, (i + 1) * sizeof(syspkg_repo_t));
    for(i = 0; ctx->repourls && ctx->repourls[i]; i++) {
        repo = syspkg_getrepo(ctx->repourls[i]);
        if(repo) {
            memcpy(&ctx->repos[i], repo, sizeof(syspkg_repo_t));
            free(repo);
        } else {
            ctx->repos[i].url = (char*)malloc(strlen(ctx->repourls[i]) + 1);
            if(ctx->repos[i].url) strcpy(ctx->repos[i].url, ctx->repourls[i]);
        }
    }
    return SUCCESS;
}
