/* valid architectures */
#define VALIDARCHS  "x86_64", "aarch64", "riscv64"
#ifdef __x86_64__
# define MYARCH "x86_64"
#else
# ifdef __aarch64__
#  define MYARCH "aarch64"
# else
#  ifdef __riscv64__
#   define MYARCH "riscv64"
#  else
#   define MYARCH "any"
#  endif
# endif
#endif

/* valid categories */
#define VALIDCATS   "education", "games", "graphics", "internet", "office", "programming", "multimedia", "tools"

/* the directory separator */
#define SEP "/"

/* user local data, cert and keys are stored here */
#define USERDIR "%s/.config/syspkg/"

/* global configuration file */
#define PKGCFG "/opt/syspkg/syspkg.json"

/* global data files' directory */
#define PKGDIR "/opt/syspkg/var/"

/* where the installed files are stored */
#define BINDIR "/opt/%s/bin/"
#define INCDIR "/opt/%s/include/"
#define LIBDIR "/opt/%s/lib/"
#define ETCDIR "/opt/%s/etc/"
#define SRCDIR "/opt/%s/src/"
#define DOCDIR "/opt/%s/man/"
#define SHRDIR "/opt/%s/share/"
#define VARDIR "/opt/%s/var/"

/* only used for the base package */
#define BASEDIR "/"

/* directories in payloads (optional, can also be specified on per package basis in metajson) */
#define BINPDR "bin"
#define INCPDR "inc"
#define LIBPDR "lib"
#define ETCPDR "etc"
#define SRCPDR "src"
#define DOCPDR "man"
#define SHRPDR "shr"
#define VARPDR "var"
