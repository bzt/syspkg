/*
 * syspkg/src/lib/syspkg.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Main syspkg context handling functions
 *
 */

#include <time.h>
#include "internal.h"

const char *syspkg_validarchs[] = { "any", VALIDARCHS, NULL };
const char *syspkg_validcats[] = { VALIDCATS, NULL };
const char *syspkg_overrides[] = { "bin", "inc", "lib", "etc", "src", "man", "shr", "var", NULL };
const char *syspkg_ploaddirs[] = { BINPDR, INCPDR, LIBPDR, ETCPDR, SRCPDR, DOCPDR, SHRPDR, VARPDR, NULL };
const char *syspkg_resolves[] = { BINDIR, INCDIR, LIBDIR, ETCDIR, SRCDIR, DOCDIR, SHRDIR, VARDIR, NULL };
const char *syspkg_envnames[] = { "BINDIR", "INCDIR", "LIBDIR", "ETCDIR", "SRCDIR", "DOCDIR", "SHRDIR", "VARDIR", NULL };
const int syspkg_lenpldirs[] = { sizeof(BINPDR)-1, sizeof(INCPDR)-1, sizeof(LIBPDR)-1, sizeof(ETCPDR)-1, sizeof(SRCPDR)-1,
    sizeof(DOCPDR)-1, sizeof(SHRPDR)-1, sizeof(VARPDR)-1, 0 };

/***** required by mbedtls *****/
int mbedtls_hardware_poll( void *data, unsigned char *output, size_t len, size_t *olen )
{
    size_t i;
    unsigned char t = (unsigned char)time(NULL);
    (void) data;
    for( i = 0; i < len; ++i )
        output[i] = rand() ^ t;
    *olen = len;
    return( 0 );
}

/**
 * Allocate a new package manager context
 */
syspkg_ctx_t *syspkg_new(syspkg_progressbar_t progressbar, syspkg_conf_t conf)
{
    char *dir, *tmp = NULL, key[32];
    size_t l;
    int i, j, k;
    syspkg_ctx_t *ctx = malloc(sizeof(syspkg_ctx_t));
    if(!ctx) return NULL;
    memset(ctx, 0, sizeof(syspkg_ctx_t));
    srand(time(NULL));
    /* get user configuration directory */
    dir = syspkg_gethome();
    if(!dir) {
err:    free(ctx);
        return NULL;
    }
    ctx->cfgdir = realloc(NULL, strlen(USERDIR) + strlen(dir));
    if(!ctx->cfgdir) goto err;
    sprintf(ctx->cfgdir, USERDIR, dir);
    syspkg_mkdir(ctx->cfgdir, 0700);
    /* language code */
    ctx->lang = syspkg_getlang();
    /* read in the main configuration file */
    if(!syspkg_readfileall(PKGCFG, (unsigned char**)&tmp, &l)) {
        if(!ctx->lang)
            ctx->lang = syspkg_json(tmp, "lang", 5);
        ctx->osver = syspkg_json(tmp, "osver", 15);
        for(i = k = 0, dir = ""; dir; i++) {
            sprintf(key, "arch.%d", i);
            ctx->arch = (char**)realloc(ctx->arch, (k + 1) * sizeof(char*));
            if(!ctx->arch) break;
            ctx->arch[k] = NULL;
            dir = syspkg_json(tmp, key, MAXARCSIZE);
            if(dir) {
                for(j = 0; syspkg_validarchs[j]; j++)
                    if(!strcmp(syspkg_validarchs[j], dir))
                        ctx->arch[k++] = dir;
            }
        }
        for(i = 0, dir = ""; dir; i++) {
            sprintf(key, "license.%d", i);
            ctx->license = (char**)realloc(ctx->license, (i + 1) * sizeof(char*));
            if(!ctx->license) break;
            ctx->license[i] = dir = syspkg_json(tmp, key, MAXLICSIZE);
        }
        for(i = k = 0, dir = ""; dir; i++) {
            sprintf(key, "repos.%d", i);
            ctx->repourls = (char**)realloc(ctx->repourls, (k + 1) * sizeof(char*));
            if(!ctx->repourls) break;
            ctx->repourls[k] = dir = syspkg_json(tmp, key, MAXURLSIZE);
            if(!dir || !memcmp(dir, "https://", 8) || !memcmp(dir, "file:///", 8)) k++;
        }
        free(tmp);
    }
    if(!ctx->arch || !ctx->arch[0]) {
        ctx->arch = (char**)realloc(ctx->arch, 2 * sizeof(char*));
        if(ctx->arch) {
            ctx->arch[0] = (char*)malloc(strlen(MYARCH) + 1);
            if(ctx->arch[0]) strcpy(ctx->arch[0], MYARCH);
            ctx->arch[1] = NULL;
        }
    }
    ctx->progressbar = progressbar;
    ctx->conf = conf;
    return ctx;
}

/**
 * Free package manager context
 */
int syspkg_free(syspkg_ctx_t *ctx)
{
    int i;
    if(!ctx) return EINVAL;
    if(ctx->cfgdir) free(ctx->cfgdir);
    if(ctx->cert) free(ctx->cert);
    if(ctx->lang) free(ctx->lang);
    if(ctx->osver) free(ctx->osver);
    if(ctx->arch) {
        for(i = 0; ctx->arch[i]; i++)
            free(ctx->arch[i]);
        free(ctx->arch);
    }
    if(ctx->license) {
        for(i = 0; ctx->license[i]; i++)
            free(ctx->license[i]);
        free(ctx->license);
    }
    if(ctx->repourls) {
        for(i = 0; ctx->repourls[i]; i++)
            free(ctx->repourls[i]);
        free(ctx->repourls);
    }
    if(ctx->repos) {
        for(i = 0; ctx->repos[i].url; i++)
            if(ctx->repos[i].cn) free(ctx->repos[i].cn);
        free(ctx->repos);
    }
    if(ctx->packages) {
        for(i = 0; ctx->packages[i]; i++)
            syspkg_freemeta((syspkg_meta_t*)ctx->packages[i]);
        free(ctx->packages);
    }
    if(ctx->installs) free(ctx->installs);
    if(ctx->removes) free(ctx->removes);
    if(ctx->suggests) free(ctx->suggests);
    if(ctx->missing) free(ctx->missing);
    if(ctx->conflicts) free(ctx->conflicts);
    if(ctx->mnts) free(ctx->mnts);
    if(ctx->inst) {
        for(i = 0; i < ctx->numinst; i++)
            syspkg_freeinstalled(&ctx->inst[i]);
        free(ctx->inst);
    }
    memset(ctx, 0, sizeof(syspkg_ctx_t));
    free(ctx);
    return SUCCESS;
}

/**
 * Save configuration
 */
int syspkg_savecfg(syspkg_ctx_t *ctx)
{
    char *buf = NULL, *dst = NULL, fn[MAXPATHSIZE], *lang;
    size_t n = 0;
    int i;

    if(!ctx) return EINVAL;
    syspkg_sprintf(&buf, &dst, &n, "{\n");
    lang = syspkg_getlang();
    if(ctx->lang && strcmp(ctx->lang, lang))
        syspkg_sprintf(&buf, &dst, &n, "  \"lang\": \"%s\",\n", ctx->lang);
    free(lang);
    if(ctx->osver && *ctx->osver)
        syspkg_sprintf(&buf, &dst, &n, "  \"osver\": \"%s\",\n", ctx->osver);
    if(ctx->arch && ctx->arch[0]) {
        syspkg_sprintf(&buf, &dst, &n, "  \"arch\": [");
        for(i = 0; ctx->arch[i]; i++)
            syspkg_sprintf(&buf, &dst, &n, " \"%s\"%s", ctx->arch[i], ctx->arch[i + 1] ? "," : "");
        syspkg_sprintf(&buf, &dst, &n, " ],\n");
    }
    if(ctx->license && ctx->license[0]) {
        syspkg_sprintf(&buf, &dst, &n, "  \"license\": [");
        for(i = 0; ctx->license[i]; i++)
            syspkg_sprintf(&buf, &dst, &n, " \"%s\"%s\n", ctx->license[i], ctx->license[i + 1] ? "," : "");
        syspkg_sprintf(&buf, &dst, &n, " ],\n");
    }
    syspkg_sprintf(&buf, &dst, &n, "  \"repos\": [\n");
    for(i = 0; ctx->repourls && ctx->repourls[i]; i++)
        syspkg_sprintf(&buf, &dst, &n, "    \"%s\"%s\n", ctx->repourls[i], ctx->repourls[i + 1] ? "," : "");
    syspkg_sprintf(&buf, &dst, &n, "  ]\n}\n");
    if(buf) {
        strcpy(fn, PKGCFG);
        dst = strrchr(fn, SEP[0]);
        if(dst) { *dst = 0; syspkg_mkdir(fn, 0755); }
        i = syspkg_writefileall(PKGCFG, (unsigned char*)buf, strlen(buf));
        free(buf);
    } else
        i = ENOMEM;
    return i;
}
