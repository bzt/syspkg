/*
 * syspkg/inc/syspkg.h
 * https://gitlab.com/bztsrc/syspkg
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Public API declarations and prototypes
 *
 */

#ifndef _SYSPKG_H_
#define _SYSPKG_H_

#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#ifndef SUCCESS
#define SUCCESS 0
#endif

/* progress bar callback prototype and message types */
enum {
    SYSPKG_MSG_GENKEYS,     /* generating keys */
    SYSPKG_MSG_GENCERT,     /* generating certificates */
    SYSPKG_MSG_GENPAYLOAD,  /* generating payloads */
    SYSPKG_MSG_CHKPAYLOAD,  /* checking payload validity */
    SYSPKG_MSG_DLDPAYLOAD,  /* downloading payloads */
    SYSPKG_MSG_GETREPO,     /* downloading repo lists */
    SYSPKG_MSG_GETMETA,     /* downloading metajsons */
    SYSPKG_MSG_CLNMETA,     /* cleaning up package database */
    SYSPKG_MSG_GENHTML,     /* generating package catalog */
    SYSPKG_MSG_DELPKG,      /* removing packages */
    SYSPKG_MSG_UNPPKG,      /* unpacking packages */
    SYSPKG_MSG_CFGPKG       /* configuring packages */
};
typedef void (*syspkg_progressbar_t)(int step, int numstep, int64_t curr, int64_t total, int msg);

enum {
    SYSPKG_FORM_NONE,
    SYSPKG_FORM_STR,        /* string */
    SYSPKG_FORM_NUM,        /* decimal number */
    SYSPKG_FORM_CHK,        /* checkbox */
    SYSPKG_FORM_SEL         /* option list */
};
typedef struct {
    char *value;            /* value to be returned */
    char *name;             /* translated label */
    char *desc;             /* translated popup description */
    char type;              /* one of the SYSPKG_FORM_* defines above */
    union {
        char *str;
        struct {
            int min;
            int max;
            int def;
        } num;
        struct {
            char *enabled;
            char *disabled;
        } chk;
        struct {
            char **opts;
            int len;
        } sel;
    } spec;
} syspkg_input_t;

/* package configuration form callback */
typedef int (*syspkg_conf_t)(char *id, char *name, char *terms, int len, syspkg_input_t *input);

/* bytecode callback */
typedef void (*syspkg_bytecode_t)(unsigned char **buf, int *len);

/* mount point */
typedef struct {
    uint64_t fsid;
    int64_t freeblks;
    int blksiz;
} syspkg_mnts_t;

/* one package description */
typedef struct {
    char *id;
    unsigned int version;
} syspkg_dep_t;

typedef struct {
    char *id;                   /* package unix name */
    char *name;                 /* name in the current locale */
    char *desc;                 /* description in the current locale */
    char *category;             /* package's category, not translated */
    char *license;              /* license identifier */
    char *url;                  /* download url */
    char *homepage;             /* webpage */
    char *bugtracker;           /* issue page */
    syspkg_dep_t *depends;      /* mandatory dependencies */
    syspkg_dep_t *suggests;     /* optional dependencies */
    syspkg_dep_t *conflicts;    /* conflicting packages */
    char *maintainer;           /* maintainer's canonical name */
    char *release;              /* available version */
    char *irelease;             /* installed version or NULL */
    unsigned int version;       /* available version (maj << 16) | (feat << 8) | (fix) */
    unsigned int iversion;      /* installed version or 0 */
    unsigned int afirst, alast; /* first and last screenshot's attachment id or -1U */
    int err;                    /* recursion level when it was selected or negative error code */
} syspkg_package_t;

typedef struct {
    char *id;                   /* package unix name */
    char **depends;             /* dependencies */
    char **conflicts;           /* conflicts */
    char *path;                 /* a file installed by this package */
    char *url;                  /* metajson url */
    char *irelease;             /* installed version or NULL */
    unsigned int iversion;      /* installed version or 0 */
    unsigned int iref;          /* number of references to this package */
} syspkg_installed_t;

typedef struct {
    char *url;                  /* repo url */
    char *cn;                   /* canonical name */
    unsigned int num;           /* number of packages in this repo */
} syspkg_repo_t;

/* the main context */
typedef struct {
    syspkg_progressbar_t progressbar;/* progress bar callback */
    syspkg_conf_t conf;         /* configure package form callback */
    syspkg_bytecode_t bytecode; /* bytecode converter callback */
    char *cfgdir;               /* user local configuration directory */
    char *cert;                 /* returned certificate */
    char *lang;                 /* detected language */
    char *osver;                /* optional OS version filter */
    char **arch;                /* supported architectures */
    char **license;             /* supported licenses */
    char **repourls;            /* list of repository urls */
    syspkg_repo_t *repos;       /* returned list of repositories */
    syspkg_installed_t *inst;   /* list of installed packages */
    syspkg_package_t **packages;/* returned list of packages */
    syspkg_package_t **installs;/* packages to be installed */
    syspkg_package_t **removes; /* packages to be removed */
    syspkg_dep_t **suggests;    /* returned list of suggested packages */
    syspkg_dep_t **missing;     /* returned list of unmet dependencies */
    char **conflicts;           /* returned list of conflicting package pairs */
    syspkg_mnts_t *mnts;        /* mount points, to count free space */
    int numinst;                /* number of installed packages */
    int numpackages;            /* number of returned packages */
    int numinstalls;            /* number of packages to be installed */
    int numremoves;             /* number of packages to be removed */
    int numsuggests;            /* number of suggested packages */
    int nummissing;             /* number of missing packages */
    int numconflicts;           /* number of conflicting packages */
    int numfiles;               /* number of files in the database */
    int nummnts;                /* number of mount points */
    size_t dlsize, total;       /* total bytes to download and install (or remove) */
    size_t comp;                /* largest compressed file */
} syspkg_ctx_t;

/*** Public API ***/
/* pkg.c */
syspkg_ctx_t *syspkg_new(syspkg_progressbar_t progressbar, syspkg_conf_t conf);
int syspkg_free(syspkg_ctx_t *ctx);

/* cert.c */
int syspkg_cert(syspkg_ctx_t *ctx, char *name, char *countrycode, int isrepo);
int syspkg_sign(syspkg_ctx_t *ctx, char *crtfile);
int syspkg_revoke(syspkg_ctx_t *ctx, char *crtfile);
int syspkg_trust(syspkg_ctx_t *ctx, char *crtfile);
int syspkg_untrust(syspkg_ctx_t *ctx, char *cn);

/* meta.c */
int syspkg_build(syspkg_ctx_t *ctx, char *jsonfile, int numpl, char **plspec);
int syspkg_check(syspkg_ctx_t *ctx, char *url);
int syspkg_search(syspkg_ctx_t *ctx, int installed, char *search, char *depends, int *ids);
int syspkg_which(syspkg_ctx_t *ctx, char *search);

/* repo.c */
int syspkg_addrepo(syspkg_ctx_t *ctx, char *url);
int syspkg_delrepo(syspkg_ctx_t *ctx, char *name);
int syspkg_listrepo(syspkg_ctx_t *ctx);

/* package.c */
int syspkg_update(syspkg_ctx_t *ctx);
int syspkg_upgrade(syspkg_ctx_t *ctx);
int syspkg_remove(syspkg_ctx_t *ctx, char **name, int nodeps);
int syspkg_install(syspkg_ctx_t *ctx, char **name, int nodeps);
int syspkg_reconf(syspkg_ctx_t *ctx, char **name);
int syspkg_commit(syspkg_ctx_t *ctx);

/* attach.c */
unsigned int *syspkg_loadattachment(unsigned int aidx, int size);

#endif /* _SYSPKG_H_ */
