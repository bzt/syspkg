#!/bin/sh

../bin/syspkg cert "GB" "John Dev <john@xmail>"
cp ~/.config/syspkg/cert ./certdev
../bin/syspkg cert --repo "HU" "Awesome Repo"
cp ~/.config/syspkg/cert ./certrepo
../bin/syspkg cert --sign ./certdev ./certdevsig
cp certdevsig ~/.config/syspkg/cert

openssl x509 -text -in certrepo
openssl x509 -text -in ~/.config/syspkg/cert
