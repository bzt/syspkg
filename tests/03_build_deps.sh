#!/bin/sh

# testing with interdependent packages

./clean.sh
./01_certs.sh
cp certdevsig ~/.config/syspkg/cert
dir=`pwd`
printf "" >repo.txt

for i in `seq 1 12`
do
    mkdir zip zip/bin zip/lib zip/inc
    printf "#!/bin/sh\nlorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temport" >zip/bin/test$i
    for j in `seq 1 128`
    do
        echo "$i $j lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temport" >zip/lib/libtest$i.$j.so
        echo "$i $j lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temport" >zip/inc/test$i.$j.h
    done
    cat example$i.json | sed "s/\@URL/${dir//\//\\\/}/" > meta$i.json
    if [ "$i" != "12" ]; then
        ../bin/syspkg build meta$i.json x86_64=zip
    else
        ../bin/syspkg build meta$i.json
    fi
    rm -rf zip
    echo file://$dir/meta$i.json >>repo.txt
done
../bin/syspkg check meta1.json
../bin/syspkg check test1-0.0.1-x86_64.zip
sha256sum test1-0.0.1-x86_64.zip
cp certrepo ~/.config/syspkg/cert
../bin/syspkg build repo.txt index.html
