#!/bin/sh

rm -rf /opt/syspkg/var 2>/dev/null
dir=`pwd`
cat >/opt/syspkg/syspkg.json <<EOF
{
  "arch": [ "x86_64" ],
  "repos": [
    "file://$dir/repo.txt"
  ]
}
EOF
time ../bin/syspkg update
