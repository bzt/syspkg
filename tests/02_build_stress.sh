#!/bin/sh

# stress testing with many packages

./clean.sh
./01_certs.sh
cp certdevsig ~/.config/syspkg/cert
printf "" >repo.txt
cats=("education" "games" "graphics" "internet" "office" "programming" "tools")
dir=`pwd`
#num=32768
num=1024

for i in `seq 1 $num`
do
    mkdir zip zip/bin zip/lib zip/inc
    cat >meta$i.json <<EOF
{
  "id": "test$i",
  "description": [
    { "en_US", "Name$i", "Description $i" }
  ],
  "version": "1.0.1",
  "release": "1.0-beta-rc",
  "license": "MIT",
  "category": "${cats[$[$i%7]]}",
  "url": "https://github.com/myuser/test$i/archive/\$VERSION-\$ARCH.zip",
  "eula": "https://github.com/myuser/test$i/raw/master/LICENSE",
  "homepage": "https://myuser.github.io/test$i",
  "bugtracker": "https://github.com/myuser/test$i/issues",
  "screenshots": [
    "file://$dir/screen.png"
  ]
}
EOF
    printf "#!/bin/sh\nlorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temport" >zip/bin/test$i
    for j in `seq 1 128`
    do
        echo "$i $j lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temport" >zip/lib/libtest$i.$j.so
        echo "$i $j lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod temport" >zip/inc/test$i.$j.h
    done
    echo "Building $i / $num"
    ../bin/syspkg build meta$i.json x86_64=zip
    rm -rf zip
    echo file://$dir/meta$i.json >>repo.txt
done
../bin/syspkg check meta1.json
../bin/syspkg check test1-1.0.1-x86_64.zip
sha256sum test1-1.0.1-x86_64.zip
cp certrepo ~/.config/syspkg/cert
../bin/syspkg build repo.txt index.html
